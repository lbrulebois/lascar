/* On intègre la référence de la carte dans une variable
   pour faciliter l'utilisation par la suite de composants
   fournis par LASCARjs (paramètres dans l'URL, layer GeoJSON,
   layer OverPass, ...). */
var
    // Carte de gauche, que l'on positionne sur la première
    // moitié de l'écran, il s'agît là d'une carte "par défaut".
    leftMap = new Lascar.cl.Map("#l-left-map", {
        style: { width: "50%", float: "left" }
    }),
    // Carte de droite, que l'on positionne sur la seconde
    // moitié de l'écran. On en profite pour supprimer les
    // layers par défaut et intégrer ceux de Google.
    rightMap = new Lascar.cl.Map("#l-right-map", {
        style: { width: "50%", float: "right" },
        controls: { scale: { generate: false } },
        layers: {
            "osm_basic": undefined,
            "osm_fr": undefined,
            "satellite": new Lascar.cl.layer.LayerDesc({
                code: "satellite",
                title: "Vue satellite (Google)",
                url: "http://{s}.google.com/vt/lyrs=s&x={x}&y={y}&z={z}",
                attribution: '© Contributeurs d\'<a href="http://openstreetmap.org">OpenStreetMap</a> - Hébergeur : <a href="http://google.com/">Google</a>',
                zoom: {
                    min: 0,
                    max: 20
                },
                subdomains: ["mt0", "mt1", "mt2", "mt3"]
            }),
            "terrain": new Lascar.cl.layer.LayerDesc({
                code: "terrain",
                title: "Vue terrain (Google)",
                url: "http://{s}.google.com/vt/lyrs=p&x={x}&y={y}&z={z}",
                attribution: '© Contributeurs d\'<a href="http://openstreetmap.org">OpenStreetMap</a> - Hébergeur : <a href="http://google.com/">Google</a>',
                zoom: {
                    min: 0,
                    max: 20
                },
                subdomains: ["mt0", "mt1", "mt2", "mt3"]
            }),
            "hybride": new Lascar.cl.layer.LayerDesc({
                code: "hybride",
                title: "Vue hybride (Google)",
                url: "http://{s}.google.com/vt/lyrs=s,h&x={x}&y={y}&z={z}",
                attribution: '© Contributeurs d\'<a href="http://openstreetmap.org">OpenStreetMap</a> - Hébergeur : <a href="http://google.com/">Google</a>',
                zoom: {
                    min: 0,
                    max: 20
                },
                subdomains: ["mt0", "mt1", "mt2", "mt3"]
            }),
            "streets": new Lascar.cl.layer.LayerDesc({
                code: "streets",
                title: "Routes (Google)",
                url: "http://{s}.google.com/vt/lyrs=m&x={x}&y={y}&z={z}",
                attribution: '© Contributeurs d\'<a href="http://openstreetmap.org">OpenStreetMap</a> - Hébergeur : <a href="http://google.com/">Google</a>',
                zoom: {
                    min: 0,
                    max: 20
                },
                subdomains: ["mt0", "mt1", "mt2", "mt3"]
            })
        },
        default_layer: "satellite"
    }),
    // On génère un composant dont l'objectif et de synchronizer
    // les cartes entre elles de manière à conserver le même niveau
    // de zoom et le même point central tout en offrant des vues
    // différentes (grâce aux layers).
    synchronizer = new ((function () {
        function Synchronizer() {
            this.maps = new Array();
        };

        Synchronizer.prototype.addTo = function (m) {
            // Si le paramètre est un objet de type 'Map'
            // on la référence !
            if (m instanceof Lascar.cl.Map && $.inArray(m, this.maps) == -1) {
                // On ajoute la carte comme observatrice 
                // et le composant comme observateur de 
                // la carte ...
                this.maps.push(m);
                m.observers.push(this);
                // On ajoute une variable pour déterminer si 
                // le mouvement est programmé ou du à une 
                // demande de l'utilisateur ...
                m.isProgramatic = false;
                // On en profite provoquer une notification
                // lors d'un changement (zoom ou layer) sur
                // la carte ...
                m._leaflet.map.on("moveend", function (event) {
                    if (!m.isProgramatic) {
                        var config = m.getConfig();
                        // On notifier le/les composants observateurs ...
                        m.changed = true;
                        m.notify(config);
                    } else {
                        // S'il s'agît d'un mouvement "programmé", 
                        // on le désactive une fois le mouvement terminé !
                        m.isProgramatic = false;
                    }
                });
            }
            // S'il s'agît d'un tableau, pour chaque 
            // carte qu'il contient on la référence !
            else if (m instanceof Array) {
                var _this = this;
                $.each(m, function (i, _m) {
                    _this.addTo(_m);
                });
            }

            // On retourne enfin le composant
            return this;
        };

        Synchronizer.prototype.update = function (e, o) {
            // S'il s'agît d'une des cartes actuellement 
            // gérée par le composant, on répercute son 
            // changement de position sur les autres cartes.
            if (e instanceof Lascar.cl.Map && $.inArray(e, this.maps) >= 0) {
                // Pour les consignes à appliquer, on récupère 
                // celle de la notification et on supprime la 
                // notion de layer pour éviter les conflits ;-)
                var params = o,
                    _this = this;
                delete params.layer;
                $.each(this.maps.diff([e]), function (i, map) {
                    map.isProgramatic = true;
                    map.apply(params);
                });

                return true;
            }
        };

        return Synchronizer;
    })());

synchronizer.addTo(leftMap);
synchronizer.addTo(rightMap);
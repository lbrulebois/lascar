/* On intègre la référence de la carte dans une variable
   pour faciliter l'utilisation par la suite de composants
   fournis par LASCARjs (paramètres dans l'URL, layer GeoJSON,
   layer OverPass, ...). */
var map = new Lascar.cl.Map();
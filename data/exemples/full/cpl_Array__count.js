// EXEMPLE 1 : 
// Comptage de l'ensemble des notes.
var grades = ["A","B","A+","A-","D","C-","A","B+","A","A-"];
grades.count(); 
// >> { "A": 3, "B": 1, "A+": 1, "A-": 2, "D": 1, "C-": 1, "B+": 1 }

// EXEMPLE 2 :
// Comptage des notes par grade majeur.
var Grade = function(s) {
    var matchMajor = s.match(/([a-fA-F])/),
        matchMinor = s.match(/(\+|-)/)
    this.major = matchMajor != null ? matchMajor[1] : undefined;
    this.minor = matchMinor != null ? matchMinor[1] : undefined;
};
var grades = [
    new Grade("A"), new Grade("B"), new Grade("A+"), 
    new Grade("A-"), new Grade("D"), new Grade("C-"), 
    new Grade("A"), new Grade("B+"), new Grade("A"), 
    new Grade("A-")
];
grades.count(function(o) { return o.major; }); 
// >> { "A": 6, "B": 2, "D": 1, "C": 1 }

// EXEMPLE 3
// Compter le nombre d'éléments égaux à une valeur.
grades.count(function(o) { return o.major === "A"; }); 
// >> { true: 6, false: 4 }
// EXEMPLE : 
// Liste des élèves d'une classe passant un examen.
var students = ["Antoine", "Marie", "Léo", "Clara", "Maxime", "Julie"];
// On souhaite connaîte les élèves qui n'ont pas encore
// rendu leur copie ...
students.diff(["Antoine", "Léo", "Clara"]); 
// >> [ "Marie", "Maxime", "Julie" ]
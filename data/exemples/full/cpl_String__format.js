"Hello {0}, it's {1} o'clock and I found a {2} value at line {1}".format("World", 12, null);
// >> "Hello World, it's 12 o'clock and I found a null value at line 12"
"I think it's undefined ... {0}".format()
// >> "I think it's undefined ... {0}"

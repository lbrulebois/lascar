/* On intègre la référence de la carte dans une variable
   pour faciliter l'utilisation par la suite de composants
   fournis par LASCARjs (paramètres dans l'URL, layer GeoJSON,
   layer OverPass, ...). */
var 
    map = new Lascar.cl.Map(),
    // Gestionnaire du marqueur identifié par la clé "poi"
    markerMan = new Lascar.cl.component.MarkerManager("poi");

// On associe la carte au gestionnaire du marqueur "poi"
markerMan.addTo(map);

// Pour pouvoir générer les marqueurs en utilisant potentiellement
// l'icône par défaut de Leaflet, il faut au préalable corriger
// le chemin des ressources Leaflet (car elles ne sont pas localisées
// au même endroit qu'initialement prévu dans Leaflet).
Lascar.util.correctLp(/leaflet/);
// On intègre enfin le marqueur "manuellement" pour le moment !
markerMan.build(new Lascar.cl.component.UrlDataMarker({
    x: 2.6994958,
    y: 48.4020962,
    marker: "castle"
}));
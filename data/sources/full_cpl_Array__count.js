// On ajoute la méthode 'count()' permettant de
// compter le nombre d'éléments correspondant
// au critère fourni dans un tableau.
if (typeof Array.prototype.count != "function") {
    /* METHODE "count()"
    permettant de compter le nombre d'élements en fonction
    du tri a effectuer.
    @param [0] : la fonction permettant de filtrer sur le 
    type de donnée recherchée.
    @return le nombre de données correspondantes en fonction
    du critère de tri. */
    Array.prototype.count = function (classifier) {
        return this.reduce(function (counter, item) {
            var p = (classifier || String)(item);
            counter[p] = counter.hasOwnProperty(p) ? counter[p] + 1 : 1;
            return counter;
        }, {});
    };
}
// On ajoute la méthode 'format()' permettant
// de générer une chaîne de caractères et la 
// complétant à l'aide de paramètres.
if (typeof String.prototype.format != "function") {
    /* METHODE "format()"
    permettant de générer une chaîne de caractères
    en replaçant l'indice par l'argument correspondant.
    @param [0..n] : Les arguments à utiliser pour 
    le remplacement.
    @return : La chaîne de caractères. */
    String.prototype.format = function () {
        var _args = arguments;
        return this.replace(/{(\d+)}/g, function (match, value) {
            return typeof _args[value] != "undefined" ? _args[value] : match;
        });
    };
}
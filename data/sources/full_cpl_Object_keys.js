// On ajoute la méthode 'keys()' permettant de 
// lister l'ensemble des propriétés d'un objet.
if (typeof Object.keys != "function") {
    Object.keys = function (o) {
        // Si le paramètre n'est pas un objet, 
        // on retourne une erreur ! 
        if (typeof o != "object" || o == null) {
            throw new Error("Object.keys called on non-object");
        }

        var keys = new Array();
        // Pour chaque élément dans l'objet on 
        // intègre dans le tampon uniquement les
        // propriétés portées par l'objet.
        for (var key in o) {
            if (o.hasOwnProperty(key)) {
                keys.push(key);
            }
        }
        return keys;
    };
}
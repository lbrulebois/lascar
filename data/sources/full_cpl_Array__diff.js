// On ajoute la méthode 'diff()' permettant de
// récupérer la différence entre deux tableaux.
if (typeof Array.prototype.diff != "function") {
    Array.prototype.diff = function (table) {
        return this.filter(function (i) {
            return $.inArray(i, table) < 0;
        });
    };
}
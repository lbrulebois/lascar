<ol class="breadcrumb">
    <li><a href="#p=accueil">Documentation</a></li>
    <li class="active">Utilisation rapide</li>
</ol>

# Utilisation rapide 

## Prérequis

Pour utiliser rapidement LASCARjs, il vous faudra vous prémunir au préalable des librairies suivantes :
* **jQuery** (>= 3.1.0)
* **Leaflet** (>= 0.7.7)

Les versions indiquées entre partenthèses sont celles utilisées dans le cadre des démonstrateurs sur la page d'accueil,
mais rien ne vous empêche d'utiliser des versions supérieures ou inférieures. Sachez que nous n'avons pas encore testé 
ces autres versions et ne pouvons pas garantir de support.  

Il vous faudra aussi disposer d'une page web dans laquelle intégrer la carte. Dans le cadre de cet exemple, nous vous proposons de générer une carte en mode <q>plein écran</q>.

## Rendu

<div class="row">
<div class="col-sm-12">
    <iframe src="/data/exemples/simple/index.html" width="100%" height="300">
        <p>Votre navigateur ne supporte pas les iframes.</p>
    </iframe>
</div>  
</div>

<a target="_blank" href="/data/exemples/simple/index.html" class="btn btn-sm btn-primary">
    <span class="glyphicon glyphicon-eye-open"></span> Consulter le rendu
</a>

## Sources

Ci-dessous, vous trouverez le code source permettant d'obtenir le rendu ci-dessus. 
Les sources ci-dessous se découpent en 3 parties nécessaires pour générer le résultat final :
* Le code **HTML** : permettant de fournir la structure de la page.
* Le code **CSS** : permettant de personnaliser le contenu de la page.
* Le code **JavaScript** : permettant de générer la carte.

<div id="exemple_simple">
</div>
<script type="text/javascript">
    $(function() {
        var exemple_simple = (new LascarCode({
			default: "html",
			codes: {
				"html": {
					type: "language-markup",
					name: "HTML",
					src: "data/exemples/simple/index.html"
				},
                "css": {
					type: "language-css",
					name: "CSS",
					src: "data/exemples/simple/style.css"
				},
                "js": {
					type: "language-javascript",
					name: "JavaScript",
					src: "data/exemples/simple/script.js"
				}
			}
		})).build();
        exemple_simple.appendTo($("#exemple_simple"));
    });
</script>


## Ce qu'il faut retenir ...

### ... pour générer rapidement une carte avec LASCARjs

* Il vous faut pour commencer avoir intégré les librairies nécessaires : jQuery, Leaflet (JS + CSS) et LASCARjs.
* Ensuite, il vous faut un conteneur (par exemple `<div>`) ayant pour identifiant : `lf-map` (l'identifiant par défaut d'une carte générée par LASCARjs).
* Enfin, il vous faut intégrer le code JavaScript suivant : `new Lascar.cl.Map();` pour provoquer la génération de la carte.

Libre à vous de personnaliser l'affichage de cette carte au travers d'une feuille de style CSS.
Pensez toutefois à donner une "hauteur" à votre carte pour qu'elle puisse s'afficher (cf. propriété `height` en CSS).

### ... pour aller plus loin

N'hésitez pas à consulter la documentation avancée vous permettant d'exploiter l'intégralité du potentiel de LASCARjs et de ses composants, en cliquant sur le bouton ci-dessous :  
<a class="btn btn-primary" href="#p=full/index">Tout savoir sur LASCARjs</a>

<script type="text/javascript">
$(function() { Prism.fileHighlight(); });
</script>
<ol class="breadcrumb">
    <li><a href="#p=accueil">Documentation</a></li>
    <li class="active">Notes de versions</li>
</ol>

# Notes de versions 

## 0.2.1
Version corrective mineure dont les objectifs sont :
* Apporter des améliorations au classes [<span class="glyphicon glyphicon-link"></span> `Lascar.cl.component.Requester`](#p=full/cl/component/Requester), [<span class="glyphicon glyphicon-link"></span> `Lascar.cl.component.MarkerManager`](#p=full/cl/component/MarkerManager), [<span class="glyphicon glyphicon-link"></span> `Lascar.cl.component.Notifier`](#p=full/cl/component/Notifier), [<span class="glyphicon glyphicon-link"></span> `Lascar.cl.component.Url`](#p=full/cl/component/Url), 
[<span class="glyphicon glyphicon-link"></span> `Lascar.cl.layer.LayerDesc`](#p=full/cl/layer/LayerDesc) et [<span class="glyphicon glyphicon-link"></span> `Lascar.cl.layer.OverPass`](#p=full/cl/layer/OverPass).
* Optimiser le code de la librairie de base en supprimant des variables inutilisées et supprimant une ligne de code inatteignable.
* Optimiser les méthodes <a href="#p=full/util/check" class="unavailable">Lascar.util.check</a> et <a href="#p=full/util/dropUndefined" class="unavailable">Lascar.util.dropUndefined</a>.
* Ajout de la méthode complémentaire [<span class="glyphicon glyphicon-link"></span> `Array.prototype.diff()`](#p=full/cpl/Array_diff).

<a class="btn btn-primary btn-sm btn-block" href="#p=notes/0.2.1"> + d'informations / <code>0.2.1</code></a>
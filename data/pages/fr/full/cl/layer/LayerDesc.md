<ol class="breadcrumb">
    <li><a href="#p=accueil">Documentation</a></li>
    <li><a href="#p=full/index">Documentation détaillée</a></li>
    <li class="active">LayerDesc</li>
</ol>

`Lascar.cl.layer`
<h1 class="simple">LayerDesc <small>classe</small></h1>
La classe `LayerDesc` a pour vocation de permettre d'encapsuler, dans un objet, les informations qui ont pour objectif de configurer un fond de carte pour une ou plusieurs cartes. Ainsi, il est possible de vérifier l'intégrité des informations fournies avant de les traiter grace au système de validation intégré dans LASCARjs.
En effet cette classe possède une description permettant de contrôler que les informations requises soient présentes (cf. `Lascar.dc.D_LayerDesc`).

---
<h2 class="simple">Paramètres</h2>
<h4 class="simple">code</h4>
<blockquote class="documentation">
Code d'identification du layer qui doit être unique car utilisé pour accéder rapidement au fond de carte concerné.
<small>Type : <code>string</code>, Valeur par défaut : <code>null</code></small>
</blockquote>

<h4 class="simple">title</h4>
<blockquote class="documentation">
Libellé permettant aux utilisateurs d'identifier le fond de carte. Ce libellé est utilisé dans le composant de sélection des <q>layers</q> de la carte Leaflet.
<small>Type : <code>string</code>, Valeur par défaut : <code>null</code></small>
</blockquote>

<h4 class="simple">url</h4>
<blockquote class="documentation">
Template d'URL (ou adresse URL) permettant d'accéder aux tuiles et générer le fond de carte. Cette URL peut avoir la forme suivante :
<pre class="usage">
http://{s}.somedomain.com/blabla/{z}/{x}/{y}{r}.png
http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png
</pre>
<small>Type : <code>string</code>, Valeur par défaut : <code>null</code></small>
</blockquote>

<h4 class="simple">attribution</h4>
<blockquote class="documentation">
Chaîne de caractères permettant de spécifier les crédits associés au fond de carte utilisé. N'hésitez pas à consulter les conditions d'utilisations du fond de carte que vous souhaitez utiliser pour remplir cette propriété correctement.
<small>Type : <code>string</code>, Valeur par défaut : <code>null</code></small>
</blockquote>

<h4 class="simple">zoom.min</h4>
<blockquote class="documentation">
Le niveau de zoom minimum supporté par le fournisseur de tuiles.
<small>Type : <code>number</code>, Valeur par défaut : <code>null</code></small>
</blockquote>

<h4 class="simple">zoom.max</h4>
<blockquote class="documentation">
Le niveau de zoom maximum supporté par le fournisseur de tuiles.
<small>Type : <code>number</code>, Valeur par défaut : <code>null</code></small>
</blockquote>

<h2 class="simple">Méthodes</h2>

### Constructeur
<pre class="usage">
LayerDesc() : new
LayerDesc(data) : new
</pre>

Constructeur permettant de générer une nouvelle instance d'une configuration d'un fond de carte.
Ce constructeur peut prendre un potentiel un paramètre `data` permettant d'initialiser les paramètres de l'objet (s'il est conforme).
S'il n'est pas défini ou ne correspond par au type indiqué, les paramètres de l'objet sont initialisés à <code>null</code>.

<h4 class="simple">data</h4>
<blockquote class="documentation">
Objet permettant d'initialiser les paramètres de l'objet à partir des valeurs qu'il contient.
Le constructeur s'intéresse tout particulièrement aux paramètres du même nom. Il intègre depuis la 
version <code>0.2.1</code> les autres paramètres directement dans l'objet en vue d'une exploitation
future. L'exemple le plus concret est l'utilisation du paramètre complémentaire <code>subdomains</code>.
<small>Type : <code>object</code>, non null</small>
</blockquote>

### validate()
<small>Plus d'informations : [<span class="glyphicon glyphicon-link"></span> Desc/validate()](#p=full/util/Desc).</small>

<pre class="usage">
validate() throws Error
</pre>

Méthode permettant de valider l'objet afin de s'assurer qu'il contienne les paramètres attendus. Dans le cas contraire, elle renvoie une exception.

### toLeaflet()
<pre class="usage">
toLeaflet() throws Error : L.tileLayer
</pre>

Méthode permettant de générer une instance `L.tileLayer` (servant à afficher un fond de carte sur une carte Leaflet) à partir des informations configurées dans l'objet. Si les prérequis ne sont pas présents, l'appel à cette méthode retourne l'erreur issue d'un appel à la méthode `validate()`.

Si les crédits ne sont pas renseignés, ils seront attribués par défaut à OpenStreetMap avec pour valeur : <q><em>&copy; contributeurs d'<a href="http://openstreetmap.org">OpenStreetMap</a></em></q>. 
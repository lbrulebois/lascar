<ol class="breadcrumb">
    <li><a href="#p=accueil">Documentation</a></li>
    <li><a href="#p=full/index">Documentation détaillée</a></li>
    <li class="active">GeoJSON</li>
</ol>

`Lascar.cl.layer`
<h1 class="simple">GeoJSON <small>classe</small></h1>
**hérite** de [<span class="glyphicon glyphicon-link"></span> Lascar.cl.layer.UrlBased](#p=full/cl/layer/UrlBased)

<div class="alert alert-info alert-dismissable">
    <span class="glyphicon glyphicon-info-sign"></span> Cette classe héritant d'une autre, la documentation sur cette page sera succincte de manière à n'évoquer que les ajouts, changements ou suppressions. Pour les paramètres et méthodes hérités, merci de consulter la documentation de la classe mère.
</div>


---
<h2 class="simple">Paramètre</h2>

*Aucun paramètre supplémentaire apporté par cette classe n'est à décrire.*

<h2 class="simple">Méthodes</h2>
### Constructeur
<pre class="usage">
GeoJSON() : new
</pre>

Constructeur permettant de générer une nouvelle instance de layer exploitant des ressources distantes issues d'une source GeoJSON.

### parsing
Les méthodes ci-dessous sont des méthodes complémentaires permettant de parser les différents objets fournis par la ressource GeoJSON en un objet Leaflet accessible sur la carte.

<h3 class="simple">parsing["coordinates"]()</h3>
<pre class="usage">
parsing["coordinates"](data: object) : L.circleMarker
</pre>

Méthode permettant de parser les coordonnées dans afin d'inverser les valeurs pour qu'elles soient exploitables par la librairie Leaflet.

<h4 class="simple">data</h4>
<blockquote class="documentation">
Les coordonnées ou un ensemble de coordonnées à inverser.
<small>Type : <code>number[]</code> ou <code>number[][]</code>, non null.</small>
</blockquote>

<h3 class="simple">parsing["Point"]()</h3>
<pre class="usage">
parsing["Point"](data: object) : L.circleMarker
</pre>

Méthode permettant de générer l'objet Leaflet associé à une information de type "Point" issue d'une réponse au format GeoJSON.

<h4 class="simple">data</h4>
<blockquote class="documentation">
Les informations reçues au format GeoJSON au sujet d'un objet de type <code>Point</code>.
<small>Type : <code>object</code>, non null.</small>
</blockquote>

<h3 class="simple">parsing["LineString"]()</h3>
<pre class="usage">
parsing["LineString"](data: object) : L.polyline
</pre>

Méthode permettant de générer l'objet Leaflet associé à une information de type "LineString" issue d'une réponse au format GeoJSON.

<h4 class="simple">data</h4>
<blockquote class="documentation">
Les informations reçues au format GeoJSON au sujet d'un objet de type <code>LineString</code>.
<small>Type : <code>object</code>, non null.</small>
</blockquote>

<h3 class="simple">parsing["Polygon"]()</h3>
<pre class="usage">
parsing["Polygon"](data: object) : L.polygon
</pre>

Méthode permettant de générer l'objet Leaflet associé à une information de type "Polygon" issue d'une réponse au format GeoJSON.

<h4 class="simple">data</h4>
<blockquote class="documentation">
Les informations reçues au format GeoJSON au sujet d'un objet de type <code>Polygon</code>.
<small>Type : <code>object</code>, non null.</small>
</blockquote>

<h3 class="simple">parsing["Multi"]()</h3>
<pre class="usage">
parsing["Multi"](type: string, data: object) : L.FeatureGroup
</pre>

Méthode générique permettant de générer un ensemble dit "Multi" à partir d'un type dit "classique" : `Point`, `LineString`, `Polygon` et des informations associées.

<h4 class="simple">type</h4>
<blockquote class="documentation">
La description du type parmis les valeurs suivantes : <code>"Point"</code>, <code>"LineString"</code>, <code>"Polygon"</code>.
<small>Type : <code>string</code>, non null.</small>
</blockquote>

<h4 class="simple">data</h4>
<blockquote class="documentation">
Les informations reçues au format GeoJSON au sujet d'un objet de type <code>Point</code>, <code>LineString</code>, <code>Polygon</code>.
<small>Type : <code>object</code>, non null.</small>
</blockquote>

<h3 class="simple">parsing["MultiPoint"]()</h3>
<pre class="usage">
parsing["MultiPoint"](data: object) : L.FeatureGroup
</pre>

Méthode permettant de générer l'objet Leaflet associé à une information de type "MultiPoint" issue d'une réponse au format GeoJSON. Cette méthode exploite les propriétés de la méthode `parsing["Multi"]()`.

<h4 class="simple">data</h4>
<blockquote class="documentation">
Les informations reçues au format GeoJSON au sujet d'un objet de type <code>MultiPoint</code>.
<small>Type : <code>object</code>, non null.</small>
</blockquote>

<h3 class="simple">parsing["MultiLineString"]()</h3>
<pre class="usage">
parsing["MultiLineString"](data: object) : L.FeatureGroup
</pre>

Méthode permettant de générer l'objet Leaflet associé à une information de type "MultiLineString" issue d'une réponse au format GeoJSON. Cette méthode exploite les propriétés de la méthode `parsing["Multi"]()`.

<h4 class="simple">data</h4>
<blockquote class="documentation">
Les informations reçues au format GeoJSON au sujet d'un objet de type <code>MultiLineString</code>.
<small>Type : <code>object</code>, non null.</small>
</blockquote>

<h3 class="simple">parsing["MultiPolygon"]()</h3>
<pre class="usage">
parsing["MultiPolygon"](data: object) : L.FeatureGroup
</pre>

Méthode permettant de générer l'objet Leaflet associé à une information de type "MultiPolygon" issue d'une réponse au format GeoJSON. Cette méthode exploite les propriétés de la méthode `parsing["Multi"]()`.

<h4 class="simple">data</h4>
<blockquote class="documentation">
Les informations reçues au format GeoJSON au sujet d'un objet de type <code>MultiPolygon</code>.
<small>Type : <code>object</code>, non null.</small>
</blockquote>

<h3 class="simple">parsing["GeometryCollection"]()</h3>
<pre class="usage">
parsing["GeometryCollection"](data: object) : L.FeatureGroup
</pre>

Méthode permettant de générer l'objet Leaflet associé à une information de type "GeometryCollection" issue d'une réponse au format GeoJSON.

<h4 class="simple">data</h4>
<blockquote class="documentation">
Les informations reçues au format GeoJSON au sujet d'un objet de type <code>GeometryCollection</code>.
<small>Type : <code>object</code>, non null.</small>
</blockquote>

<h3 class="simple">parsing["Feature"]()</h3>
<pre class="usage">
parsing["Feature"](data: object) : L.FeatureGroup
</pre>

Méthode permettant de générer l'objet Leaflet associé à une information de type "Feature" issue d'une réponse au format GeoJSON.

<h4 class="simple">data</h4>
<blockquote class="documentation">
Les informations reçues au format GeoJSON au sujet d'un objet de type <code>Feature</code>.
<small>Type : <code>object</code>, non null.</small>
</blockquote>

<h3 class="simple">parsing["FeatureCollection"]()</h3>
<pre class="usage">
parsing["FeatureCollection"](data: object) : L.FeatureGroup
</pre>

Méthode permettant de générer l'objet Leaflet associé à une information de type "FeatureCollection" issue d'une réponse au format GeoJSON.

<h4 class="simple">data</h4>
<blockquote class="documentation">
Les informations reçues au format GeoJSON au sujet d'un objet de type <code>FeatureCollection</code>.
<small>Type : <code>object</code>, non null.</small>
</blockquote>
<ol class="breadcrumb">
    <li><a href="#p=accueil">Documentation</a></li>
    <li><a href="#p=full/index">Documentation détaillée</a></li>
    <li class="active">OverPass</li>
</ol>

`Lascar.cl.layer`
<h1 class="simple">OverPass <small>classe</small></h1>
**hérite** de [<span class="glyphicon glyphicon-link"></span> Lascar.cl.layer.UrlBased](#p=full/cl/layer/UrlBased)

<div class="alert alert-info alert-dismissable">
    <span class="glyphicon glyphicon-info-sign"></span> Cette classe héritant d'une autre, la documentation sur cette page sera succincte de manière à n'évoquer que les ajouts, changements ou suppressions. Pour les paramètres et méthodes hérités, merci de consulter la documentation de la classe mère.
</div>


---
<h2 class="simple">Paramètre</h2>

*Aucun paramètre supplémentaire apporté par cette classe n'est à décrire.*

<h2 class="simple">Méthodes</h2>
### Constructeur
<pre class="usage">
OverPass() : new
</pre>

Constructeur permettant de générer une nouvelle instance de layer exploitant des ressources distantes issues d'une source OverPass.

### parsing
Les méthodes ci-dessous sont des méthodes complémentaires permettant de parser les différents objets fournis par la ressource OverPass en un objet Leaflet accessible sur la carte.

<h3 class="simple">parsing["node"]()</h3>
<pre class="usage">
parsing["node"](data: object) : L.circleMarker
</pre>

Méthode permettant de générer l'objet Leaflet associé à une information de type "node" issue d'une réponse de l'OverPass API.

<h4 class="simple">data</h4>
<blockquote class="documentation">
Les informations reçues par l'OverPass API au format JSON au sujet d'un objet de type <code>node</code>.
<small>Type : <code>object</code>, non null.</small>
</blockquote>

<h3 class="simple">parsing["way"]()</h3>
<pre class="usage">
parsing["way"](data: object) : L.polygon|L.polyline
</pre>

Méthode permettant de générer l'objet Leaflet associé à une information de type "way" issue d'une réponse de l'OverPass API.

<h4 class="simple">data</h4>
<blockquote class="documentation">
Les informations reçues par l'OverPass API au format JSON au sujet d'un objet de type <code>way</code>.
<small>Type : <code>object</code>, non null.</small>
</blockquote>

<h3 class="simple">parsing["relation"]()</h3>
<pre class="usage">
parsing["relation"](data: object) : L.FeatureGroup
</pre>

Méthode permettant de générer l'objet Leaflet associé à une information de type "relation" issue d'une réponse de l'OverPass API.

<h4 class="simple">data</h4>
<blockquote class="documentation">
Les informations reçues par l'OverPass API au format JSON au sujet d'un objet de type <code>relation</code>.
<small>Type : <code>object</code>, non null.</small>
</blockquote>
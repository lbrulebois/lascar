<ol class="breadcrumb">
    <li><a href="#p=accueil">Documentation</a></li>
    <li><a href="#p=full/index">Documentation détaillée</a></li>
    <li class="active">UrlBased</li>
</ol>

`Lascar.cl.layer`
<h1 class="simple">UrlBased <small>classe</small></h1>

La classe `UrlBased` a pour vocation d'uniformiser le comportement des layers consommant des ressources externes : web services, fichiers plats, ... Cette classe propose le fonctionnement suivant : 
* Génération et envoie de la requête.
* Si la réponse est "positive", appel des fonctions de parsage. En cas d'erreur, affichage d'une notification sur la carte à l'aide du composant   
[<span class="glyphicon glyphicon-link"></span> `Lascar.cl.component.Notifier`](#p=full/cl/component/Notifier) généré automatiquement sur la carte lors de son référencement auprès de ce layer.

---
<h2 class="simple">Paramètre</h2>
<h4 class="simple">req</h4>
<blockquote class="documentation">
Gestionnaire d'envoi de requêtes pour le layer basé sur des ressources distantes.
<small>Type : <a href="#p=full/cl/component/Requester"><span class="glyphicon glyphicon-link"></span> <code>Lascar.cl.component.Requester</code></a>, non null</small>
</blockquote>

<h4 class="simple">layer</h4>
<blockquote class="documentation">
Le layer généré suite au dernier appel du web service permettant une intégration rapide sur une nouvelle carte.
<small>Type : <a href="http://leafletjs.com/reference-1.0.3.html#featuregroup"><span class="glyphicon glyphicon-link"></span> <code>L.FeatureGroup</code></a>, null</small>
</blockquote>

<h4 class="simple">maps</h4>
<blockquote class="documentation">
Liste des cartes associées à ce layer. Elles sont référencées ainsi :
<pre class="usage">
&lt;id_carte&gt;: {
    map: &lt;Lascar.cl.Map&gt;,
    layer: &lt;null|L.FeatureGroup&gt;
}
</pre>
<small>Type : <code>object</code>, non null</small>
</blockquote>

<h4 class="simple">parseFuncs</h4>
<blockquote class="documentation">
Liste des fonctions de parsage appelées pour générer le layer à intégrer sur chacune des cartes associées à l'objet actuel.
<small>Type : <code>function[]</code></a>, non null</small>
</blockquote>

<h4 class="simple">options</h4>
<blockquote class="documentation">
Options permettant de configurer le layer et sa génération (plus d'informations, cf. <code>default</code>).
<small>Type : <code>object</code></a>, non null</small>
</blockquote>

<h4 class="simple">default</h4>
<blockquote class="documentation">
La configuration par défaut de toutes les instances de layer exploitant des ressources distantes. Les valeurs sont les suivantes et sont explicités ci-après :
<ul>
<!-- <li><code></code>, valeur par défaut : <code></code></li> -->
<li><code>style</code>, valeur par défaut : 
    <ul>
    <li><code>point</code>, valeur par défaut : 
        <ul>
        <li><code>opacity</code>, valeur par défaut : <code>1.0</code></li>
        <li><code>weight</code>, valeur par défaut : <code>2</code></li>
        <li><code>color</code>, valeur par défaut : <code>"#0088CE"</code></li>
        <li><code>radius</code>, valeur par défaut : <code>5</code></li>
        </ul>
    </li>
    <li><code>ligne</code>, valeur par défaut : 
        <ul>
        <li><code>opacity</code>, valeur par défaut : <code>1.0</code></li>
        <li><code>weight</code>, valeur par défaut : <code>2</code></li>
        <li><code>color</code>, valeur par défaut : <code>"#0088CE"</code></li>
        </ul>
    </li>
    <li><code>forme</code>, valeur par défaut : 
        <ul>
        <li><code>opacity</code>, valeur par défaut : <code>1.0</code></li>
        <li><code>weight</code>, valeur par défaut : <code>2</code></li>
        <li><code>color</code>, valeur par défaut : <code>"#0088CE"</code></li>
        </ul>
    </li>
    </ul>
</li>
</ul>
<small>Type : <code>object</code>, non null</small>
</blockquote>

<h2 class="simple">Méthodes</h2>
### Constructeur
<pre class="usage">
UrlBased() : new
UrlBased(req_params) : new
UrlBased(req_params, options) : new
</pre>

Constructeur permettant de générer une nouvelle instance de layer exploitant des ressources distantes.

<h4 class="simple">req_params</h4>
<blockquote class="documentation">
Les paramètres de configuration à intégrer dans le requêteur.
<small>Type : <code>object</code>, Valeur par défaut : <code>undefined</code>.</small>
</blockquote>

<h4 class="simple">options</h4>
<blockquote class="documentation">
Options permettant de configurer le layer et sa génération (plus d'informations, cf. <code>default</code>).
<small>Type : <code>object</code>, non null - par défaut : <code>UrlBased/default</code>.</small>
</blockquote>

### update()
<pre class="usage">
update(observable: Observable[, data: object])
</pre>

Cette méthode consiste à récupérer l'ensemble des notifications des objets observés par l'objet portant cette méthode.

<h4 class="simple">observable</h4>
<blockquote class="documentation">
L'objet à l'origine de la notification. Permet d'adapter le comportement en fonction de l'émetteur du message.
<small>Type : <a href="#p=full/util/Observable"><span class="glyphicon glyphicon-link"></span> <code>Lascar.util.Observable</code></a>, non null</small>
</blockquote>

<h4 class="simple">data</h4>
<blockquote class="documentation">
L'information accompagnant la notification dans l'optique de préciser la nature du changement.
<small>Type : <code>tous</code>, Valeur par défaut : <code>undefined</code></small>
</blockquote>

### parse()
<pre class="usage">
parse(data: object) throws Error
</pre>

Cette méthode consiste a retourner une erreur si la réponse est négative ou à provoquer la génération du layer à partir des informations fournies.
Une fois le layer généré, il est répercuté sur l'ensemble des cartes associées à ce composant.

<h4 class="simple">data</h4>
<blockquote class="documentation">
Les données reçues par la ressource externe dans l'optique de générer le potentiel layer.
<small>Type : <code>object</code>, non null</small>
</blockquote>

### addTo()
<pre class="usage">
addTo(carte) : this
</pre>

Méthode permettant d'associer la/les cartes au composant et d'ajouter une copie du layer à la/es cartes fournies en paramètre.

<h4 class="simple">carte</h4>
<blockquote class="documentation">
La ou les cartes à associer au comosant et pour la/lesquelles il faut appliquer une copie du layer parsé.
<small>Type : <code>Array</code> ou <a href="#p=full/cl/Map"><span class="glyphicon glyphicon-link"></span> <code>Lascar.cl.Map</code></a>, non null</small>
</blockquote>

### apply() 
<pre class="usage">
apply()
</pre>

Méthode permettant de provoquer l'appel du requêteur et ainsi la potentielle génération du layer si la réponse le permet (plus d'informations cf. la méthode `parse()`).

### setProperties() 
<pre class="usage">
setProperties(leaflet, properties: object)
</pre>

Méthode permettant de générer la popup associée à un objet Leaflet et d'y intégrer les propriétés fournies en paramètre. 

<h4 class="simple">leaflet</h4>
<blockquote class="documentation">
L'objet Leaflet sur lequel appliquer la popup d'informations.
<small>Type : <code>object</code>, non null</small>
</blockquote>

<h4 class="simple">properties</h4>
<blockquote class="documentation">
Les propriétés à intégrer dans la dialogue.
<small>Type : <code>object</code>, non null</small>
</blockquote>
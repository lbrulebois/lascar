<ol class="breadcrumb">
    <li><a href="#p=accueil">Documentation</a></li>
    <li><a href="#p=full/index">Documentation détaillée</a></li>
    <li class="active">Requester</li>
</ol>

`Lascar.cl.component`
<h1 class="simple">Requester <small>classe</small></h1>
**hérite** de [<span class="glyphicon glyphicon-link"></span> Lascar.util.Observable](#p=full/util/Observable)

<div class="alert alert-info alert-dismissable">
    <span class="glyphicon glyphicon-info-sign"></span> Cette classe héritant d'une autre, la documentation sur cette page sera succincte de manière à n'évoquer que les ajouts, changements ou suppressions. Pour les paramètres et méthodes hérités, merci de consulter la documentation de la classe mère.
</div>

La classe `Requester` a pour vocation de permettre de générer un composant en charge d'envoyer des requêtes à partir des informations configurées lors de sa génération et de la demande d'envoi de la requête. Cette classe exploite la méthode `$.ajax()` fournie par jQuery : [<span class="glyphicon glyphicon-link"></span> plus d'informations](http://api.jquery.com/jquery.ajax/).

Cette classe possède une description permettant de contrôler que les informations requises soient présentes (cf. `Lascar.dc.D_Requester`) : cette définition requiert que les paramètres `url` et `type` soient présents.

---
<h2 class="simple">Paramètre</h2>
<h4 class="simple">default</h4>
<blockquote class="documentation">
La configuration par défaut de toutes les instances de requêteur. 
Les valeurs sont les suivantes et sont explicités ci-après :
<ul>
<li><code>async</code>, valeur par défaut : <code>true</code></li>
<li><code>cache</code>, valeur par défaut : <code>false</code></li>
<li><code>crossDomain</code>, valeur par défaut : <code>false</code></li>
<li><code>contentType</code>, valeur par défaut : <code>"application/json"</code></li>
<li><code>dataType</code>, valeur par défaut : <code>"json"</code></li>
<li><code>headers</code>, valeur par défaut : <code>{}</code></li>
</ul>
<small>Type : <code>objet</code>, non null</small>
</blockquote>

<h4 class="simple">url</h4>
<blockquote class="documentation">
L'adresse URL à contacter.
<small>Type : <code>string</code>, non null.</small>
</blockquote>

<h4 class="simple">type</h4>
<blockquote class="documentation">
Le type d'appel à réaliser : <code>GET</code>, <code>POST</code>, <code>PUT</code>, <code>DELETE</code>, ...
<small>Type : <code>string</code>, non null.</small>
</blockquote>

<h4 class="simple">async</h4>
<blockquote class="documentation">
L'appel à réaliser doit-il être asynchrone ou non.
<small>Type : <code>boolean</code>, Valeur par défaut : <code>true</code></small>
</blockquote>

<h4 class="simple">cache</h4>
<blockquote class="documentation">
Faut-il mettre-en-cache les informations reçues.
<small>Type : <code>boolean</code>, Valeur par défaut : <code>false</code></small>
</blockquote>

<h4 class="simple">crossDomain</h4>
<blockquote class="documentation">
L'appel doit-il s'effectuer sur un domain différent que celui depuis lequel est effectué l'appel. Pensez à vous assurer que le service apellé permet le <a href="https://en.wikipedia.org/wiki/Cross-origin_resource_sharing"><span class="glyphicon glyphicon-link"></span> CORS</a>.
<small>Type : <code>boolean</code>, Valeur par défaut : <code>false</code></small>
</blockquote>

<h4 class="simple">contentType</h4>
<blockquote class="documentation">
Le MIME du type de contenu envoyé lors de l'appel.
<small>Type : <code>string</code>, Valeur par défaut : <code>"application/json"</code></small>
</blockquote>

<h4 class="simple">dataType</h4>
<blockquote class="documentation">
Le type de données attendu pour la réponse suite à l'appel.
<small>Type : <code>string</code>, Valeur par défaut : <code>"json"</code></small>
</blockquote>

<h4 class="simple">headers</h4>
<blockquote class="documentation">
Les potentielles en-têtes à intégrer en supplément avant l'appel.
<small>Type : <code>object</code>, Valeur par défaut : <code>{}</code></small>
</blockquote>

<h2 class="simple">Méthodes</h2>
### Constructeur
<pre class="usage">
Requester() : new
Requester(options) : new
</pre>

Constructeur permettant de générer une nouvelle instance d'un requêteur. Ce constructeur peut prendre un potentiel paramètre `options` permettant de le configurer. Ces éléments seront intégrés dans la configuration par défaut. Pour information, les valeurs du paramètre `options` qui sont définies à `undefined` ont pour vocation de supprimer les valeurs de la configuration actuelle.

<h4 class="simple">options</h4>
<blockquote class="documentation">
Les paramètres de configuration à intégrer dans le requêteur.
<small>Type : <code>object</code>, Valeur par défaut : <code>undefined</code></small>
</blockquote>

### send()
<pre class="usage">
send(observateur [, options]) throws Error : undefined | object
</pre>

Méthode permettant de provoquer l'envoi d'une nouvelle requête à partir des informations configurées dans le requêteur. Cette métode prend en compte deux paramètres :
* le premier `observateur` permet d'ajouter (s'il ne fait pas déjà parti des observateur) l'objet comme observateur du requêteur.
* le deuxième `options` permet d'ajouter, modifier ou supprimer des paramètres de configuration du requêteur.

Si le paramètre `async` du requêteur a pour valeur :
* `true` : la méthode ne retourne rien, les observateurs seront notifiés de la réponse.
* `false` : la méthode retournera l'information notifiée à l'ensemble des observateurs.

Si les paramètre requis : `url` et `type` ne sont pas présents, une erreur est retournée lors de la validation des prérequis.

<h4 class="simple">observateur</h4>
<blockquote class="documentation">
L'observateur à ajouter s'il ne fait pas déjà parti des observateurs du requêteur.  
Une piste d'amélioration serait de pouvoir intégrer un ou plusieurs observateurs à la fois.
<small>Type : <code>object</code>, null</small>
</blockquote>

<h4 class="simple">options</h4>
<blockquote class="documentation">
Les potentiels paramètres de configuration à (ré-)intégrer dans le requêteur.
<small>Type : <code>object</code>, Valeur par défaut : <code>undefined</code></small>
</blockquote>
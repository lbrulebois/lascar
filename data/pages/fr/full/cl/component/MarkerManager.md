<ol class="breadcrumb">
    <li><a href="#p=accueil">Documentation</a></li>
    <li><a href="#p=full/index">Documentation détaillée</a></li>
    <li class="active">MarkerManager</li>
</ol>

`Lascar.cl.component`
<h1 class="simple">MarkerManager <small>classe</small></h1>

La classe `MarkerManager` a pour vocation de permettre la génération d'un composant d'intégration de marqueurs sur la/les cartes associées. Il exploite aisni les informations de type [<span class="glyphicon glyphicon-link"></span> `Lascar.cl.component.UrlDataMarker`](#p=full/cl/component/UrlDataMarker) fournies par le gestionnaire d'URL ([<span class="glyphicon glyphicon-link"></span> `Lascar.cl.component.Url`](#p=full/cl/component/Url)).

---
<h2 class="simple">Paramètres</h2>

<h4 class="simple">markers</h4>
<blockquote class="documentation">
Liste des marqueurs pouvant être exploités par le composant. Les marqueurs doivent être identifiés par une clé unique et être des instances d'options de <code>L.marker</code> (plus d'informations : <a href="http://leafletjs.com/reference.html#marker-options"><span class="glyphicon glyphicon-link"></span> L.marker/options</a>).
<small>Type : <code>object</code>, Valeur par défaut : <code>{}</code></small>
</blockquote>

<h4 class="simple">layer</h4>
<blockquote class="documentation">
Layer potentiellement généré suite à une notification du gestionnaire d'URL.
<small>Type : <a href="http://leafletjs.com/reference.html#featuregroup"><span class="glyphicon glyphicon-link"></span> <code>L.FeatureGroup</code></a>, Valeur par défaut : <code>null</code></small>
</blockquote>

<h4 class="simple">maps</h4>
<blockquote class="documentation">
Liste des cartes associées au composant et pour lequels il faut associer le layer généré (avec le marqueur). Chaque carte doit être référencée ainsi dans ce paramètre :
<pre class="usage">
"id_carte" : { map: &lt;Lascar.cl.Map&gt, layer: null }
</pre>

Tel que la valeur d'<code>id_carte</code> soit celle du paramètre <code>id</code> de l'objet <a href="#p=full/cl/Map"><span class="glyphicon glyphicon-link"></span> <code>Lascar.cl.Map</code></a> (associé par la suite dans le paramètre <code>map</code>).
<small>Type : <code>object</code>, Valeur par défaut : <code>{}</code></small>
</blockquote>

<h4 class="simple">key</h4>
<blockquote class="documentation">
Clé d'identification du paramètre à traiter dans l'URL afin d'extraire les potentielles informations liées au marqueur à (re-)générer.
<small>Type : <code>string</code>, Valeur par défaut : <code>"marker"</code></small>
</blockquote>

<h2 class="simple">Méthodes</h2>
### Constructeur
<pre class="usage">
MarkerManager() : new
MarkerManager(cle) : new
</pre>

Constructeur permettant de générer une nouvelle instance de gestionnaire de marqueurs pouvant être disposés sur une ou plusieurs cartes à la fois. Ce constructeur accèpte un paramètre `cle` permettant de définir le nom du paramètre à exploiter dans l'URL.

<h4 class="simple">cle</h4>
<blockquote class="documentation">
Le libellé du paramètre dans l'URL permettant d'accéder aux potentielles informations à exploiter pour générer le marqueur sur la/les cartes.
<small>Type : <code>string</code>, Valeur par défaut : <code>"marker"</code></small>
</blockquote>

### link()
<pre class="usage">
link(urlManager) throws Error
</pre>

Méthode permettant d'associer le composant actuel au gestionnaire de paramètres dans l'URL. Cela permet ainsi de mettre-en-place le/les marqueurs sur la/les cartes associés au gestionnaire de marqueurs à partir des éléments de configurations fournis.

<h4 class="simple">urlManager</h4>
<blockquote class="documentation">
Le gestionnaire des paramètres dans l'URL à partir duquel obtenir les potentielles informations du marqueur à générer sur la/les cartes.
<small>Type : <a href="#p=full/cl/component/Url"><span class="glyphicon glyphicon-link"></span> <code>Lascar.cl.component.Url</code></a>, non null</small>
</blockquote>

### update()
<pre class="usage">
update(observable: Observable[, data: object])
</pre>

Cette méthode consiste à récupérer l'ensemble des notifications des objets observés par l'objet portant cette méthode.

<h4 class="simple">observable</h4>
<blockquote class="documentation">
L'objet à l'origine de la notification. Permet d'adapter le comportement en fonction de l'émetteur du message.
<small>Type : <a href="#p=full/util/Observable"><span class="glyphicon glyphicon-link"></span> <code>Lascar.util.Observable</code></a>, non null</small>
</blockquote>

<h4 class="simple">data</h4>
<blockquote class="documentation">
L'information accompagnant la notification dans l'optique de préciser la nature du changement.
<small>Type : <code>tous</code>, Valeur par défaut : <code>undefined</code></small>
</blockquote>
</div>

### clear()
<pre class="usage">
clear() : boolean
</pre>

Méthode permettant de réinitialiser les layers du marqueur généré sur la/les cartes associées à ce composant. Cette méthode retourne  `false` lorsque le layer généré n'existe pas, `true` si le layer a correctement été réinitialisé.

### build()
<pre class="usage">
build(donnees) throws Error
</pre>

Méthode permettant de générer ou regénérer le layer du marqueur pour chacune des cartes, à partir des données fournies.

<h4 class="simple">donnees</h4>
<blockquote class="documentation">
Les informations retournées par le gestionnaire de paramètres dans l'URL en ce qui concerne le marqueur à configurer.
<small>Type : <a href="#p=full/cl/component/UrlDataMarker"><span class="glyphicon glyphicon-link"></span> <code>Lascar.cl.component.UrlDataMarker</code></a>, non null</small>
</blockquote>

### addTo()
<pre class="usage">
addTo(carte) : this
</pre>

Méthode permettant d'associer la/les cartes au composant et d'ajouter une copie du layer à la/es cartes fournies en paramètre.

<h4 class="simple">carte</h4>
<blockquote class="documentation">
La ou les cartes à associer au comosant et pour la/lesquelles il faut appliquer une copie du layer parsé.
<small>Type : <code>Array</code> ou <a href="#p=full/cl/Map"><span class="glyphicon glyphicon-link"></span> <code>Lascar.cl.Map</code></a>, non null</small>
</blockquote>
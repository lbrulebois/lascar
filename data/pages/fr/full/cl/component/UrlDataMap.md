<ol class="breadcrumb">
    <li><a href="#p=accueil">Documentation</a></li>
    <li><a href="#p=full/index">Documentation détaillée</a></li>
    <li class="active">UrlDataMap</li>
</ol>

`Lascar.cl.component`
<h1 class="simple">UrlDataMap <small>classe</small></h1>
La classe `UrlDataMap` a pour vocation de permettre d'encapsuler, dans un objet, les informations issues de l'URL qui ont pour objectif de configurer une carte. Ainsi, il est possible de vérifier l'intégrité des informations fournies avant de les traiter grace au système de validation intégré dans LASCARjs.
En effet cette classe possède une description permettant de contrôler que les informations requises soient présentes (cf. `Lascar.dc.D_UrlDataMap`).

---
<h2 class="simple">Paramètres</h2>
<h4 class="simple">x</h4>
<blockquote class="documentation">
Longitude du point central de la carte ou à affecter à la carte.
<small>Type : <code>number</code>, Valeur par défaut : <code>null</code></small>
</blockquote>

<h4 class="simple">y</h4>
<blockquote class="documentation">
Latitude du point central de la carte ou à affecter à la carte.
<small>Type : <code>number</code>, Valeur par défaut : <code>null</code></small>
</blockquote>  

<h4 class="simple">zoom</h4>
<blockquote class="documentation">
Niveau de zoom de la carte ou à affecter à la carte.
<small>Type : <code>number</code>, Valeur par défaut : <code>null</code></small>
</blockquote> 

<h4 class="simple">layer</h4>
<blockquote class="documentation">
Clé du fond de carte utilisé par la carte ou à affecter à celle-ci.
<small>Type : <code>string</code>, Valeur par défaut : <code>null</code></small>
</blockquote>

<h2 class="simple">Méthodes</h2>

### Constructeur
<pre class="usage">
UrlDataMap() : new
UrlDataMap(data) : new
</pre>

Constructeur permettant de générer une nouvelle instance d'une configuration d'une carte ou à destination d'une carte.
Ce constructeur peut prendre un potentiel un paramètre `data` permettant d'initialiser les paramètres de l'objet (s'il est conforme).
S'il n'est pas défini ou ne correspond par au type indiqué, les paramètres de l'objet sont initialisés à <code>null</code>.

<h4 class="simple">data</h4>
<blockquote class="documentation">
Objet permettant d'initialiser les paramètres de l'objet à partir des valeurs qu'il contient.
Le constructeur s'intéresse tout particulièrement aux paramètres du même nom.
<small>Type : <code>object</code>, non null</small>
</blockquote>

### validate()
<small>Plus d'informations : [<span class="glyphicon glyphicon-link"></span> Desc/validate()](#p=full/util/Desc).</small>

<pre class="usage">
validate() throws Error
</pre>

Méthode permettant de valider l'objet afin de s'assurer qu'il contienne les paramètres attendus. Dans le cas contraire, elle renvoie une exception.
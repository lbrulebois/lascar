<ol class="breadcrumb">
    <li><a href="#p=accueil">Documentation</a></li>
    <li><a href="#p=full/index">Documentation détaillée</a></li>
    <li class="active">Url</li>
</ol>

`Lascar.cl.component`
<h1 class="simple">Url <small>classe</small></h1>
**hérite** de [<span class="glyphicon glyphicon-link"></span> Lascar.util.Observable](#p=full/util/Observable)

<div class="alert alert-info alert-dismissable">
    <span class="glyphicon glyphicon-info-sign"></span> Cette classe héritant d'une autre, la documentation sur cette page sera succincte de manière à n'évoquer que les ajouts, changements ou suppressions. Pour les paramètres et méthodes hérités, merci de consulter la documentation de la classe mère.
</div>

La classe `Url` a pour vocation de permettre la génération d'un composant dont l'objectif est de récupérer les paramètres dans l'URL pour les notifier à ses observateurs et d'intégrer les changements des observateurs dans l'URL. Les paramètres récupérés et réintégrés sont issus des classes [<span class="glyphicon glyphicon-link"></span> `Lascar.cl.component.UrlDataMap`](#p=full/cl/component/UrlDataMap) et [<span class="glyphicon glyphicon-link"></span> `Lascar.cl.component.UrlDataMarker`](#p=full/cl/component/UrlDataMarker).

---
<h2 class="simple">Paramètre</h2>

*Aucun paramètre supplémentaire apporté par cette classe n'est à décrire.*

<h2 class="simple">Méthodes</h2>
### Constructeur
<pre class="usage">
Url() : new
</pre>

Constructeur permettant de générer une nouvelle instance de gestionnaire de paramètres dans l'URL pouvant être associé à une ou plusieurs cartes à la fois.

### addTo()
<pre class="usage">
addTo(carte) : this
</pre>

Méthode permettant d'associer la/les cartes au composant et d'ajouter ainsi une fonction lors des événements `moveend` et `baselayerchange` pour que l'instance <a href="#p=full/cl/Map"><span class="glyphicon glyphicon-link"></span> <code>Lascar.cl.Map</code></a> le notifie des changements opérés et soit notifiée lors de changements dans l'URL.

<h4 class="simple">carte</h4>
<blockquote class="documentation">
La ou les cartes à associer au comosant et pour la/lesquelles il faut intégrer leurs configurations dans l'URL et transmettre les changements pour la/les mettre-à-jour.
<small>Type : <code>Array</code> ou <a href="#p=full/cl/Map"><span class="glyphicon glyphicon-link"></span> <code>Lascar.cl.Map</code></a>, non null</small>
</blockquote>

### update()
<pre class="usage">
update(observable: Observable[, data: object])
</pre>

Cette méthode consiste à récupérer l'ensemble des notifications des objets observés par l'objet portant cette méthode.

<h4 class="simple">observable</h4>
<blockquote class="documentation">
L'objet à l'origine de la notification. Permet d'adapter le comportement en fonction de l'émetteur du message.
<small>Type : <a href="#p=full/util/Observable"><span class="glyphicon glyphicon-link"></span> <code>Lascar.util.Observable</code></a>, non null</small>
</blockquote>

<h4 class="simple">data</h4>
<blockquote class="documentation">
L'information accompagnant la notification dans l'optique de préciser la nature du changement.
<small>Type : <code>tous</code>, Valeur par défaut : <code>undefined</code></small>
</blockquote>
</div>

### apply()
<pre class="usage">
apply()
</pre>

Méthode permettant d'appliquer les paramètres de l'URL en notifiant les cartes observatrices (et les potentiels autres composants) des potentielles configurations récupérées. Ainsi les observateurs notifiés peuvent appliquer les informations fournies.

### unserialize()
<pre class="usage">
unserialize() : object
</pre>

Méthode permettant d'extraire les paramètres intégrés dans l'URL à partir du symbole `#` afin de les parser sous forme de [<span class="glyphicon glyphicon-link"></span> `Lascar.cl.component.UrlDataMap`](#p=full/cl/component/UrlDataMap) ou [<span class="glyphicon glyphicon-link"></span> `Lascar.cl.component.UrlDataMarker`](#p=full/cl/component/UrlDataMarker) et de notifier l'ensemble du parsage aux différents observateurs. 

La réponse est formattée ainsi :
<pre class="usage">
{ "clé paramètre": Lascar.cl.component.UrlDataMap | Lascar.cl.component.UrlDataMarker [, ...] }
</pre>

### serialize()
<pre class="usage">
serialize()
serialize(complements)
</pre>

Méthode permettant d'intégrer les paramètres de la/les cartes observatrices (et des potentiels autres composants) dans l'URL pour faciliter entre autre, le partage de la vue (afin que la/les cartes soient initialisées telles qu'elles sont décrites dans l'URL). 

Cette méthode permet l'utilisation d'un paramètre `complements` permettant d'intégrer des corrections aux paramètres actuellement en place dans l'URL.

<h4 class="simple">complements</h4>
<blockquote class="documentation">
La/les configurations de cartes à corriger dans l'URL. Il s'agît d'un objet qui doit être structuré ainsi :
<pre class="usage">
{ "clé carte": Lascar.cl.component.UrlDataMap [, ...] }
</pre>
<small>Type : <code>object</code>, Valeur par défaut : <code>undefined</code></small>
</blockquote>
</div>
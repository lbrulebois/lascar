<ol class="breadcrumb">
    <li><a href="#p=accueil">Documentation</a></li>
    <li><a href="#p=full/index">Documentation détaillée</a></li>
    <li class="active">UrlDataMarker</li>
</ol>

`Lascar.cl.component`
<h1 class="simple">UrlDataMarker <small>classe</small></h1>
La classe `UrlDataMarker` a pour vocation de permettre d'encapsuler, dans un objet, les informations issues de l'URL qui ont pour objectif de configurer un marqueur sur une ou plusieurs cartes. Ainsi, il est possible de vérifier l'intégrité des informations fournies avant de les traiter grace au système de validation intégré dans LASCARjs.
En effet cette classe possède une description permettant de contrôler que les informations requises soient présentes (cf. `Lascar.dc.D_UrlDataMarker`).

---
<h2 class="simple">Paramètres</h2>
<h4 class="simple">x</h4>
<blockquote class="documentation">
Longitude du point central de la carte ou à affecter à la carte.
<small>Type : <code>number</code>, Valeur par défaut : <code>null</code></small>
</blockquote>

<h4 class="simple">y</h4>
<blockquote class="documentation">
Latitude du point central de la carte ou à affecter à la carte.
<small>Type : <code>number</code>, Valeur par défaut : <code>null</code></small>
</blockquote>  

<h4 class="simple">marker</h4>
<blockquote class="documentation">
Clé du marqueur utilisé par la/les cartes ou à affecter à celles-ci.
<small>Type : <code>string</code>, Valeur par défaut : <code>null</code></small>
</blockquote>

<h2 class="simple">Méthodes</h2>

### Constructeur
<pre class="usage">
UrlDataMarker() : new
UrlDataMarker(data) : new
</pre>

Constructeur permettant de générer une nouvelle instance d'une configuration d'un marqueur ou à destination d'une ou plusieurs cartes.
Ce constructeur peut prendre un potentiel un paramètre `data` permettant d'initialiser les paramètres de l'objet (s'il est conforme).
S'il n'est pas défini ou ne correspond par au type indiqué, les paramètres de l'objet sont initialisés à <code>null</code>.

<h4 class="simple">data</h4>
<blockquote class="documentation">
Objet permettant d'initialiser les paramètres de l'objet à partir des valeurs qu'il contient.
Le constructeur s'intéresse tout particulièrement aux paramètres du même nom.
<small>Type : <code>object</code>, non null</small>
</blockquote>

### validate()
<small>Plus d'informations : [<span class="glyphicon glyphicon-link"></span> Desc/validate()](#p=full/util/Desc).</small>

<pre class="usage">
validate() throws Error
</pre>

Méthode permettant de valider l'objet afin de s'assurer qu'il contienne les paramètres attendus. Dans le cas contraire, elle renvoie une exception.
<ol class="breadcrumb">
    <li><a href="#p=accueil">Documentation</a></li>
    <li><a href="#p=full/index">Documentation détaillée</a></li>
    <li class="active">Notifier</li>
</ol>

`Lascar.cl.component`
<h1 class="simple">Notifier <small>classe</small></h1>
**hérite** de [<span class="glyphicon glyphicon-link"></span> L.Control](http://leafletjs.com/reference.html#control) / <span class="bootstrap" title="Nécessite Bootstrap.">B</span>
<div class="alert alert-info alert-dismissable">
    <span class="glyphicon glyphicon-info-sign"></span> Cette classe héritant d'une autre, la documentation sur cette page sera succincte de manière à n'évoquer que les ajouts, changements ou suppressions. Pour les paramètres et méthodes hérités, merci de consulter la documentation de la classe mère.
</div>

La classe `Notifier` a pour vocation de permettre de générer un composant  de gestion des notifications pour une carte Leaflet. Ce composant est utilisé par exemple pour gérer l'affichage du statut de chargement des layers GeoJSON ou OverPass. Libre à vous de l'utiliser pour n'importe quel besoin de notification auprès de l'utilisateur.  
Ce composant utilise les icônes fournies par [<span class="glyphicon glyphicon-link"></span> Bootstrap](http://getbootstrap.com/components/#glyphicons), n'hésitez pas à le modifier pour utiliser d'autres icônes ou faire sans.

---
<h2 class="simple">Paramètre</h2>
<h4 class="simple">position</h4>
<blockquote class="documentation">
Position du composant Leaflet par rapport à sa carte d'affectation.
Plus d'informations sur les valeurs de positions utilisables : <a href="http://leafletjs.com/reference.html#control-%27topleft%27"><span class="glyphicon glyphicon-link"></span> cliquez-ici</a>.
<small>Type : <code>string</code>, Valeur par défaut : <code>topleft</code></small>
</blockquote>

<h2 class="simple">Méthodes</h2>
### Constructeur
<small>Plus d'informations : [<span class="glyphicon glyphicon-link"></span> L.Control/new()](http://leafletjs.com/reference.html#control-l.control).</small>

<pre class="usage">
Notifier() : new
Notifier(options) : new
</pre>

Constructeur permettant de générer une nouvelle instance du composant en charge de l'affichage des notifications sur une carte Leaflet. 
Ce constructeur peut prendre un potentiel un paramètre `options` permettant d'impacter les valeurs par défaut du composant.

<h4 class="simple">options</h4>
<blockquote class="documentation">
Objet permettant d'impacter les valeurs par défaut du composant et ainsi les corriger. Les valeurs possibles sont indiquées <a href="http://leafletjs.com/reference.html#control-position"><span class="glyphicon glyphicon-link"></span> ici</a>.
<small>Type : <code>object</code>, non null</small>
</blockquote>

### onAdd()
<pre class="usage">
onAdd(map) : HTMLElement
</pre>
Méthode appelée suite à l'intégration du composant dans une carte via la méthode [<span class="glyphicon glyphicon-link"></span> L.control/addTo()](http://leafletjs.com/reference.html#control-addto) afin de générer le conteneur à associer à la carte Leaflet.

<h4 class="simple">map</h4>
<blockquote class="documentation">
La carte Leaflet à laquelle il faut attacher le composant.
<small>Type : <code>L.map</code>, non null - <a href="http://leafletjs.com/reference.html#map-class"><span class="glyphicon glyphicon-link"></span> plus d'informations</a></small>
</blockquote>

### show()
<pre class="usage">
show(icone, message) : this
</pre>
Méthode permettant d'afficher le conteneur du composant de notifications avec par défaut, uniquement l'icône de chargement. Cette méthode peut être appelée avec les paramètres `icone` et `message` afin de préciser la nature de la notification.

<h4 class="simple">icone</h4>
<blockquote class="documentation">
Le code <q>réduit</q> de l'icône Bootstrap à utiliser (il s'agît de la section représentée par une étoile ici : <code>glyphicon-*</code>).
<small>Type : <code>string</code>, Valeur par défaut : <code>"refresh"</code></small>
</blockquote>

<h4 class="simple">message</h4>
<blockquote class="documentation">
Le potentiel message à associer à la notification. S'il est indéfini, le précédent message sera supprimé et ne sera pas remplacé.
<small>Type : <code>string</code>, Valeur par défaut : <code>undefined</code></small>
</blockquote>

### hide()
<pre class="usage">
hide() : this
</pre>
Méthode permettant de masquer le conteneur du composant de notificatons.
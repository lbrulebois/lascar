<ol class="breadcrumb">
    <li><a href="#p=accueil">Documentation</a></li>
    <li><a href="#p=full/index">Documentation détaillée</a></li>
    <li class="active">Map</li>
</ol>

`Lascar.cl`
<h1 class="simple">Map <small>classe</small></h1>
**hérite** de [<span class="glyphicon glyphicon-link"></span> Lascar.util.Observable](#p=full/util/Observable)

<div class="alert alert-info alert-dismissable">
    <span class="glyphicon glyphicon-info-sign"></span> Cette classe héritant d'une autre, la documentation sur cette page sera succincte de manière à n'évoquer que les ajouts, changements ou suppressions. Pour les paramètres et méthodes hérités, merci de consulter la documentation de la classe mère.
</div>

La classe `Map` a pour vocation de permettre la génération d'une carte Leaflet soit à partir d'une configuration par défaut, soit à partir de paramètres personnalisés. Ainsi il est possible de générer rapidement une carte et profiter des nombreuses autres fonctionnalités apportées par LASCARjs.

---
<h2 class="simple">Paramètre</h2>
<h4 class="simple">id</h4>
<blockquote class="documentation">
Sélecteur jQuery permettant d'accéder au conteneur de la carte (obligatoirement un <q>id</q>).
<small>Type : <code>string</code>, non null</small>
</blockquote>

<h4 class="simple">options</h4>
<blockquote class="documentation">
Options permettant de configurer la carte et sa génération (plus d'informations, cf. <code>default</code>).
<small>Type : <code>object</code>, non null</small>
</blockquote>

<h4 class="simple">default</h4>
<blockquote class="documentation">
La configuration par défaut de toutes les instances de carte. Les valeurs sont les suivantes et sont explicités ci-après :
<ul>
<!-- <li><code></code>, valeur par défaut : <code></code></li> -->
<li><code>id</code>, valeur par défaut : <code>"#lf-map"</code></li>
<li><code>options</code>, valeur par défaut : 
    <ul>
    <li><code>style</code>, valeur par défaut : 
        <ul>
        <li><code>width</code>, valeur par défaut : <code>"100%"</code></li>
        <li><code>height</code>, valeur par défaut : <code>"100%"</code></li>
        </ul>
    </li>
    <li><code>center</code>, valeur par défaut : <code>[46.53972, 2.43028]</code></li>
    <li><code>zoom</code>, valeur par défaut : <code>6</code></li>
    <li><code>controls</code>, valeur par défaut :
        <ul>
        <li><code>zoom</code>, valeur par défaut : 
            <ul>
            <li><code>generate</code>, valeur par défaut : <code>true</code></li>
            <li><code>position</code>, valeur par défaut : <code>"topright"</code></li>
            </ul>
        </li>
        <li><code>scale</code>, valeur par défaut : 
            <ul>
            <li><code>generate</code>, valeur par défaut : <code>true</code></li>
            <li><code>position</code>, valeur par défaut : <code>"bottomright"</code></li>
            </ul>
        </li>
        </ul>
    </li>
    <li><code>default_layer</code>, valeur par défaut : <code>"osm_basic"</code></li>
    <li><code>layers</code>, valeur par défaut :
        <ul>
        <li><code>osm_basic</code>, valeur par défaut : l'instance <a href="#p=full/cl/layer/LayerDesc"><span class="glyphicon glyphicon-link"></span> <code>Lascar.cl.layer.LayerDesc</code></a> configurée pour utiliser les tuiles d'OpenStreetMap.</li> 
        <li><code>osm_fr</code>, valeur par défaut : l'instance <a href="#p=full/cl/layer/LayerDesc"><span class="glyphicon glyphicon-link"></span> <code>Lascar.cl.layer.LayerDesc</code></a> configurée pour utiliser les tuiles d'OpenStreetMap France.</li>  
        </ul>
    </li>
    </ul>
</li>
</ul>
<small>Type : <code>object</code>, non null</small>
</blockquote>

<h2 class="simple">Méthodes</h2>
### Constructeur
<pre class="usage">
Map() : new
Map(id) : new
Map(options) : new
Map(id, options) : new
</pre>

Constructeur permettant de générer une nouvelle instance de `Map` et la configurer pour ainsi permettre la généreration de la carte Leaflet. Ce constructeur applique les options par défaut et permet au développeur de configurer la carte en fonction de ses besoins à l'aide des paramètres `id` (pour l'identifiant du conteneur de la carte) et `options` pour les éléments de configuration de la carte.

<h4 class="simple">id</h4>
<blockquote class="documentation">
Sélecteur jQuery permettant d'accéder au conteneur de la carte (obligatoirement un <q>id</q>).
<small>Type : <code>string</code>, non null - par défaut : <code>"#lf-map"</code>.</small>
</blockquote>

<h4 class="simple">options</h4>
<blockquote class="documentation">
Options permettant de configurer la carte et sa génération (plus d'informations, cf. <code>default</code>).
<small>Type : <code>object</code>, non null - par défaut : <code>Map/default</code>.</small>
</blockquote>

### build()
<pre class="usage">
build() throws Error
</pre>

Méthode permettant de générer la carte Leaflet à partir des éléments de configuration associés à l'instance. Les éléments Leaflet générés sont disposés dans le paramètre `_leaflet` de l'instance :
* `map` (`L.map`) : la carte Leaflet générée.
* `layers` (`Array`) : la liste des layers parsés.
* `ctrlLayers` (`L.control.layers`) : le gestionnaire de layers.
* ...

Une erreur peut être retournée si la configuration ne permet pas la génération de la carte Leaflet, c'est-à-dire :
* Lorsque l'identifiant `id` est invalide ou ne permet pas d'accéder à un objet DOM.
* Lorsque les options sont inexistantes ou nulles.
* Lorsque qu'aucun layer n'est configuré.
* Lorsque le layer par défaut n'est pas défini dans les layers utilisables.

### getConfig()
<pre class="usage">
getConfig() : Lascar.cl.component.UrlDataMap
</pre>

Méthode permettant de récupérer la configuration de l'affichage de la carte : coordonnées du point central, niveau de zoom et layer actuel afin d'être exploité potentiellement par le composant de gestion des paramètres dans l'URL par exemple.

### getActiveBaseLayer()
<pre class="usage">
getActiveBaseLayer() : object
</pre>

Méthode permettant de retourner les informations sur le layer de base actuellement utilisé sur la carte.   
Les informations retournées sont les suivantes :
<pre class="usage">
{
    layer: object,
    name: string,
    overlay: undefined | boolean
}
</pre>

Tel que :
* `layer` : soit la référence sur le layer Leaflet actuellement utilisé.
* `name` : soit le nom du layer actuellement utilisé.
* `overlay` : normalement a pour valeur `undefined` indiquant qu'il n'est pas un layer complémentaire.

### update()
<pre class="usage">
update(observable: Observable[, data: object])
</pre>

Cette méthode consiste à récupérer l'ensemble des notifications des objets observés par l'objet portant cette méthode.

<h4 class="simple">observable</h4>
<blockquote class="documentation">
L'objet à l'origine de la notification. Permet d'adapter le comportement en fonction de l'émetteur du message.
<small>Type : <a href="#p=full/util/Observable"><span class="glyphicon glyphicon-link"></span> <code>Lascar.util.Observable</code></a>, non null</small>
</blockquote>

<h4 class="simple">data</h4>
<blockquote class="documentation">
L'information accompagnant la notification dans l'optique de préciser la nature du changement.
<small>Type : <code>tous</code>, Valeur par défaut : <code>undefined</code></small>
</blockquote>
</div>

### apply()
<pre class="usage">
apply() : this
apply(null) : this
apply({layer: string}) : this
apply({x: number, y: number, zoom: number}) : this
apply({layer: string, x: number, y: number, zoom: number}) : this
</pre>

Méthode permettant de configurer l'affichage de la carte en fonction des informations fournies :
* pas de paramètre ou le paramètre vaut `null` : repositionnement du centre de la carte, du niveau de zoom et du layer aux valeurs configurées.
* un paramètre de type `object`, repositionnement (en fonction des paramètres présents) aux valeurs indiquées ou celle configurées à la génération de l'instance.
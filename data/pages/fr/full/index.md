<ol class="breadcrumb">
    <li><a href="#p=accueil">Documentation</a></li>
    <li class="active">Documentation détaillée</li>
</ol>

# Documentation détaillée 

Vous trouverez ici l'intégralité des fonctionnalités apportées par la librairie LASCARjs dans sa dernère version : **0.2.1**.  
Comme indiqué dans la section [Utilisation rapide](#p=simple/index), cette librairie se base sur les librairies ci-dessous :
* **jQuery** (>= 3.1.0)
* **Leaflet** (>= 0.7.7)
* **Bootstrap** (>= 3.3.7) : pour certains modules uniquement, qui seront marqués du signe suivant : <span class="bootstrap" title="Nécessite Bootstrap.">B</span>. 

Les versions indiquées entre partenthèses sont celles utilisées dans le cadre des démonstrateurs sur la page d'accueil,
mais rien ne vous empêche d'utiliser des versions supérieures ou inférieures. Sachez que nous n'avons pas encore testé 
ces autres versions et ne pouvons pas garantir de support.  

## Classes

<div class="row">

<!-- Classes "Lascar.cl" -->
<div class="col-sm-4">
<h4 class="simple"><code>Lascar.cl</code></h4>
<p>Sont référencées ici l'ensemble des classes dîtes de <q>base</q>.</p>

<ul>
<li><a href="#p=full/cl/Map">Map</a></li>
</ul>
</div>

<!-- Classes "Lascar.cl.layer" -->
<div class="col-sm-4">
<h4 class="simple"><code>Lascar.cl.layer</code></h4>
<p>Sont référencées ici l'ensemble des classes associées aux <q>layers</q> et à la gestion
des éléments affichés dans/sur la carte.</p>

<ul>
<li><a href="#p=full/cl/layer/LayerDesc">LayerDesc</a></li>
<li><a href="#p=full/cl/layer/UrlBased">UrlBased</a></li>
<li><a href="#p=full/cl/layer/GeoJSON">GeoJSON</a></li>
<li><a href="#p=full/cl/layer/OverPass">OverPass</a></li>
</ul>
</div>

<!-- Classes "Lascar.cl.component" -->
<div class="col-sm-4">
<h4 class="simple"><code>Lascar.cl.component</code></h4>
<p>Sont référencées ici l'ensemble des classes associées aux <q>composants</q> apportés
par la librairie.</p>

<ul>
<li><a href="#p=full/cl/component/UrlDataMap">UrlDataMap</a></li>
<li><a href="#p=full/cl/component/UrlDataMarker">UrlDataMarker</a></li>
<li><a href="#p=full/cl/component/Url">Url</a></li>
<li><a href="#p=full/cl/component/MarkerManager">MarkerManager</a></li>
<li><a href="#p=full/cl/component/Requester">Requester</a></li>
<li><a href="#p=full/cl/component/Notifier">Notifier</a> <span class="bootstrap" title="Nécessite Bootstrap.">B</span></li>
</ul>
</div>

</div>

## Utilitaires

<div class="row">

<!-- COMPLEMENTS sur les classes existantes -->
<div class="col-sm-4">
<h4 class="simple">Compléments</h4>
<p>Sont référencées ici l'ensemble des méthodes intégrées à des classes de base ainsi qu'un composant de gestion des changements dans l'URL de la page.</p>

<ul>
<li><a href="#p=full/cpl/String_format">String.prototype.format</a></li>
<li><a href="#p=full/cpl/Object_keys">Object.keys</a></li>
<li><a href="#p=full/cpl/Array_count">Array.prototype.count</a></li>
<li><a href="#p=full/cpl/Array_diff">Array.prototype.diff</a></li>
<li><a href="#p=full/cpl/HashChange">window.HashChange</a></li>
</ul>
</div>

<!-- COMPLEMENTS de classes "Lascar.util" -->
<div class="col-sm-4">
<h4 class="simple">Classes <code>Lascar.util</code></h4>
<p>Sont référencées ici l'ensemble des classes dîtes <q>utiles</q>, permettant le bon fonctionnement de la librairie.</p>

<ul>
<li><a href="#p=full/util/DescItem">DescItem</a></li>
<li><a href="#p=full/util/Desc">Desc</a></li>
<li><a href="#p=full/util/Observable">Observable</a></li>
</ul>
</div>

<!-- METHODES "Lascar.util" -->
<div class="col-sm-4">
<h4 class="simple">Méthodes <code>Lascar.util</code></h4>
<p>Sont référencées ici l'ensemble des méthodes dîtes <q>utiles</q>, permettant le bon fonctionnement de la librairie et des cartes générées.</p>

<ul>
<li><a href="#p=full/util/check" class="unavailable">check</a></li>
<li><a href="#p=full/util/correctLp" class="unavailable">correctLp</a></li>
<li><a href="#p=full/util/dropUndefined" class="unavailable">dropUndefined</a></li>
<li><a href="#p=full/util/applyLayerOnMap" class="unavailable">applyLayerOnMap</a></li>
<li><a href="#p=full/util/unfold" class="unavailable">unfold</a></li>
<li><a href="#p=full/util/getUrlParams" class="unavailable">getUrlParams</a></li>
</ul>
</div>

</div>
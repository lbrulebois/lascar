<ol class="breadcrumb">
    <li><a href="#p=accueil">Documentation</a></li>
    <li><a href="#p=full/index">Documentation détaillée</a></li>
    <li class="active">String/format()</li>
</ol>

`String.prototype`
<h1 class="simple">format() <small>fonction</small></h1>
**associée** à la classe [<span class="glyphicon glyphicon-link"></span> String](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/String)

La fonction `format()`a pour vocation de faciliter la génération de chaînes de caractères dont certaines sections sont variables le tout de manière lisible (sans utiliser de `str.concat()` ou de `str + ""`). Sa définition est fortement inspirée de celle en C# [<span class="glyphicon glyphicon-link"></span> String/format()](https://msdn.microsoft.com/fr-fr/library/system.string.format%28v=vs.110%29.aspx).

---

<h2 class="simple">Description</h2>
<pre class="usage">
<em>str</em>.format([param1, ... , paramN]) : string
</pre>

Cette méthode à pour vocation de remplacer (quand cela est possible et en fonction des paramètres fournis) les chaînes `{x}`, tel que `x` soit un entier naturel positif ou nul, par le paramètre positionné à l'indice indiqué par `x`. 

<h4 class="simple">param1, ... , paramN</h4>
<blockquote class="documentation">
Le ou les paramètres de remplacement à intégrer dans la chaîne de caractères.
<small>Type : <code>tous</code></small>
</blockquote>

<h2 class="simple">Source</h2>

<div id="source_string__format">
</div>
<script type="text/javascript">
    $(function() {
        var source_string__format = (new LascarCode({
			default: "js",
			codes: {
                "js": {
					type: "language-javascript",
					name: "JavaScript",
					src: "data/sources/full_cpl_String__format.js"
				}
			}
		})).build();
        source_string__format.appendTo($("#source_string__format"));
    });
</script>

Cette fonction peut être améliorée, effectivement, actuellement, quand le paramètre requis a pour valeur `undefined`, aucun remplacement n'est effectué. Il peut être intéressant, en fonction des besoins, à ce que le paramètre requis soit intégré comme une chaîne vide.

Il est aussi possible de l'améliorer en prenant en compte le fait que le premier argument (et le seul) pourraît être un tableau.

<h2 class="simple">Exemples</h2>
<div id="exemple_string__format">
</div>
<script type="text/javascript">
    $(function() {
        var exemple_string__format = (new LascarCode({
			default: "js",
			codes: {
                "js": {
					type: "language-javascript",
					name: "JavaScript",
					src: "data/exemples/full/cpl_String__format.js"
				}
			}
		})).build();
        exemple_string__format.appendTo($("#exemple_string__format"));
    });
</script>


<script type="text/javascript">
$(function() { Prism.fileHighlight(); });
</script>

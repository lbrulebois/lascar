<ol class="breadcrumb">
    <li><a href="#p=accueil">Documentation</a></li>
    <li><a href="#p=full/index">Documentation détaillée</a></li>
    <li class="active">Array/count()</li>
</ol>

`Array.prototype`
<h1 class="simple">count() <small>fonction</small></h1>
**associée** à la classe [<span class="glyphicon glyphicon-link"></span> Array](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/Array)

<div class="alert alert-warning" role="alert">
<p><i class="fa fa-flask" aria-hidden="true" />
Cette méthode est en phase d'expérimentation et utilise une fonction qui n'est pas compatible avec les <q>anciens navigateurs</q>. Pour plus d'informations, merci de consulter la documentation MDN <a href="https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/Array/reduce"><span class="glyphicon glyphicon-link"></span> Array/reduce()</a>.</p>
</div>

La fonction `count()` a pour vocation de faciliter le dénombrement dans un tableau. Sans critère de discimination, elle permet de recenser le nombre d'instances identiques, avec un critère, elle l'applique sur chacun des éléments pour mieux les regrouper selon le filtrage mis à disposition.

---

<h2 class="simple">Description</h2>
<pre class="usage">
<em>arr</em>.count([discriminant]) : object
</pre>

<h4 class="simple">discriminant</h4>
<blockquote class="documentation">
La fonction permettant de retourner un critère discriminant à appliquer sur le tableau.
<small>Type : <code>function</code>, Valeur par défaut : <code>undefined</code></small>
</blockquote>

<h2 class="simple">Source</h2>

<div id="source_array__count">
</div>
<script type="text/javascript">
    $(function() {
        var source_array__count = (new LascarCode({
			default: "js",
			codes: {
                "js": {
					type: "language-javascript",
					name: "JavaScript",
					src: "data/sources/full_cpl_Array__count.js"
				}
			}
		})).build();
        source_array__count.appendTo($("#source_array__count"));
    });
</script>

<h2 class="simple">Exemples</h2>
<div id="exemple_array__count">
</div>
<script type="text/javascript">
    $(function() {
        var exemple_array__count = (new LascarCode({
			default: "js",
			codes: {
                "js": {
					type: "language-javascript",
					name: "JavaScript",
					src: "data/exemples/full/cpl_Array__count.js"
				}
			}
		})).build();
        exemple_array__count.appendTo($("#exemple_array__count"));
    });
</script>


<script type="text/javascript">
$(function() { Prism.fileHighlight(); });
</script>
<ol class="breadcrumb">
    <li><a href="#p=accueil">Documentation</a></li>
    <li><a href="#p=full/index">Documentation détaillée</a></li>
    <li class="active">Object.keys()</li>
</ol>

`Object`
<h1 class="simple">keys() <small>fonction</small></h1>
**associée** à la classe [<span class="glyphicon glyphicon-link"></span> Object](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/Object)

La fonction `keys()` a pour vocation de permettre de lister les propriétés d'un objet.
Celle fournie dans LASCARjs a pour objectif de combler le manque de cette fonction dans 
les navigateurs anciens. Elle n'est pas complète, mais répond aux besoins nécessaires 
dans le cadre de l'utilisation de LASCARjs. Pour plus d'informations, [<span class="glyphicon glyphicon-link"></span> consultez l'article du MDN](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/Object/keys).

---

<h2 class="simple">Description</h2>
<pre class="usage">
Object.keys(objet) : Array
</pre>

<h4 class="simple">objet</h4>
<blockquote class="documentation">
L'objet pour lequel on souhaite obtenir les noms des paramètres.
<small>Type : <code>object</code>, non null</small>
</blockquote>

<h2 class="simple">Source</h2>

<div id="source_object_keys">
</div>
<script type="text/javascript">
    $(function() {
        var source_object_keys = (new LascarCode({
			default: "js",
			codes: {
                "js": {
					type: "language-javascript",
					name: "JavaScript",
					src: "data/sources/full_cpl_Object_keys.js"
				}
			}
		})).build();
        source_object_keys.appendTo($("#source_object_keys"));
    });
</script>

<h2 class="simple">Exemples</h2>
<div id="exemple_object_keys">
</div>
<script type="text/javascript">
    $(function() {
        var exemple_object_keys = (new LascarCode({
			default: "js",
			codes: {
                "js": {
					type: "language-javascript",
					name: "JavaScript",
					src: "data/exemples/full/cpl_Object_keys.js"
				}
			}
		})).build();
        exemple_object_keys.appendTo($("#exemple_object_keys"));
    });
</script>


<script type="text/javascript">
$(function() { Prism.fileHighlight(); });
</script>
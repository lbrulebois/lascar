<ol class="breadcrumb">
    <li><a href="#p=accueil">Documentation</a></li>
    <li><a href="#p=full/index">Documentation détaillée</a></li>
    <li class="active">Array/diff()</li>
</ol>

`Array.prototype`
<h1 class="simple">diff() <small>fonction</small></h1>
**associée** à la classe [<span class="glyphicon glyphicon-link"></span> Array](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/Array)

La fonction `diff()` a pour vocation de faciliter la détermination de la différence entre un tableau source et un tableau cible et ainsi obtenir le tableau du delta.

<div class="alert alert-warning" role="alert">
<p><i class="fa fa-flask" aria-hidden="true" />
Cette méthode est en phase d'expérimentation et peut être améliorée entre autre au niveau de la vérification de son unique paramètre (s'assurer qu'il s'agisse bien d'un tableau par exemple). De plus celle-ci utilise une fonction qui n'est pas compatible avec les <q>anciens navigateurs</q>. Pour plus d'informations, merci de consulter la documentation MDN <a href="https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/Array/filter"><span class="glyphicon glyphicon-link"></span> Array/filter()</a>.</p>
</div>

---

<h2 class="simple">Description</h2>
<pre class="usage">
<em>arr</em>.diff(<em>arr2</em>) : Array
</pre>

<h4 class="simple">arr2</h4>
<blockquote class="documentation">
Le tableau à comparer avec le tableau actuel (à l'origine de l'appel de la méthode <code>diff()</code>).
<small>Type : <code>Array</code>, non null</small>
</blockquote>

<h2 class="simple">Source</h2>

<div id="source_array__diff">
</div>
<script type="text/javascript">
    $(function() {
        var source_array__diff = (new LascarCode({
			default: "js",
			codes: {
                "js": {
					type: "language-javascript",
					name: "JavaScript",
					src: "data/sources/full_cpl_Array__diff.js"
				}
			}
		})).build();
        source_array__diff.appendTo($("#source_array__diff"));
    });
</script>

<h2 class="simple">Exemples</h2>
<div id="exemple_array__diff">
</div>
<script type="text/javascript">
    $(function() {
        var exemple_array__diff = (new LascarCode({
			default: "js",
			codes: {
                "js": {
					type: "language-javascript",
					name: "JavaScript",
					src: "data/exemples/full/cpl_Array__diff.js"
				}
			}
		})).build();
        exemple_array__diff.appendTo($("#exemple_array__diff"));
    });
</script>


<script type="text/javascript">
$(function() { Prism.fileHighlight(); });
</script>
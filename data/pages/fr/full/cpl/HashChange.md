<ol class="breadcrumb">
    <li><a href="#p=accueil">Documentation</a></li>
    <li><a href="#p=full/index">Documentation détaillée</a></li>
    <li class="active">HashChange</li>
</ol>

`window`
<h1 class="simple">HashChange <small>composant</small></h1>

Le composant `HashChange` a pour vocation de faciliter la détection de l'événement `onhashchange` (lié aux changements de la section de l'URL à partir du symbole `#` inclus), d'appliquer les fonctions référencées lors de cet événement et de permettre le changement de l'URL sans provoquer l'événement observé. Ce composant est un singleton de la classe du même nom et peut être amélioré en fonction de vos besoins.

---

<h2 class="simple">Paramètre</h2>
<h4 class="simple">funcs</h4>
<blockquote class="documentation">
La liste des fonctions à appliquer lors de la détection de l'événement <code>onhashchange</code>.
Aucune vérification sur le type n'est actuellement effectuée (cela peut être une source d'amélioration), pensez à bien intégrer des fonctions dans cette liste.
<small>Type : <code>Array</code>, Valeur par défaut : <code>[]</code></small>
</blockquote>

<h2 class="simple">Méthode</h2>
### goto()
<pre class="usage">
goto(url) : void
</pre>

Méthode permettant de changer d'URL tout en évitant de provoquer les fonctions liées à l'événement `onhashchange`.

<h4 class="simple">url</h4>
<blockquote class="documentation">
L'adresse URL vers laquelle rediriger l'utilisateur. S'il ne s'agît pas d'une chaîne de caractères, le paramètre sera utilisé en tant que tel dans l'URL.
<small>Type : <code>string</code></small>
</blockquote>
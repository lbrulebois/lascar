<ol class="breadcrumb">
    <li><a href="#p=accueil">Documentation</a></li>
    <li><a href="#p=full/index">Documentation détaillée</a></li>
    <li class="active">Desc</li>
</ol>

`Lascar.util`
<h1 class="simple">Desc <small>classe</small></h1>
La classe `Desc` a pour vocation de permettre la génération de descriptions de classes dans l'optique de valider la structure des instances générées en apportant les outils nécessaires. Cette classe peut aussi être utilisée directement pour vérifier les informations fournies par l'utilisateur.

---
<h2 class="simple">Paramètre</h2>
<h4 class="simple">items</h4>
<blockquote class="documentation">
Liste des descriptions de paramètres permettant de valider la structure de l'objet. Les éléments renseignés dans ce tableau doivent être du type <a href="#p=full/util/DescItem"><span class="glyphicon glyphicon-link"></span> DescItem</a>.
<small>Type : <code>Array</code>, Valeur par défaut : <code>[]</code></small>
</blockquote>

<h2 class="simple">Méthode</h2>

### Constructeur
<pre class="usage">
Desc([constructeur]) : new
</pre>

Constructeur permettant de générer une nouvelle instance de description d'un type d'objet afin de valider les structure des instances. Cette méthode permet aussi de lier la description directement à une classe au travers du paramètre `constructeur`. Ainsi la classe disposera de :

* Un nouveau paramètre `description` référençant la description générée.
* Une nouvelle méthode `validate` permettant de s'assurer de la cohérence de données. Cette méthode est basée sur l'appel à la méthode [<span class="glyphicon glyphicon-link"></span> Lascar.util.check()](#p=full/util/check).

<h4 class="simple">constructeur</h4>
<blockquote class="documentation">
Le constructeur à associer à la description pour faciliter la validation structurelle des instances générées.
<small>Type : <code>function</code>, Valeur par défaut : <code>undefined</code></small>
</blockquote>
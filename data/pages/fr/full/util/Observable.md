<ol class="breadcrumb">
    <li><a href="#p=accueil">Documentation</a></li>
    <li><a href="#p=full/index">Documentation détaillée</a></li>
    <li class="active">Observable</li>
</ol>

`Lascar.util`
<h1 class="simple">Observable <small>classe</small></h1>
La classe `Observable` a pour vocation de permettre de générer des objets en capacité de notifier des observateurs sur des changements survenus. Utilisée en tant que telle, cette classe a peu d'utilité, elle a principalement vocation a être héritée afin d'implémenter le pattern <q>Observer/Observable</q> ([<span class="glyphicon glyphicon-link"></span> plus d'informations](https://fr.wikipedia.org/wiki/Observateur_%28patron_de_conception%29)).  

---
<h2 class="simple">Paramètres</h2>
<h4 class="simple">changed</h4>
<blockquote class="documentation">
Indicateur de changement de statut de l'objet observable. Cet indicateur permet de savoir s'il faut notifier ou non les observateurs.
<small>Type : <code>boolean</code>, Valeur par défaut : <code>false</code></small>
</blockquote>

<h4 class="simple">observers</h4>
<blockquote class="documentation">
Liste des objets observant l'object actuel. Pour que ces objets reçoivent les notifications, ils doivent implémenter la méthode suivante :
<pre class="usage">
update(observable: Observable[, data: object])
</pre>

<small>Type : <code>Array</code>, Valeur par défaut : <code>[]</code></small>
</blockquote>

<h2 class="simple">Méthodes</h2>

### Constructeur
<pre class="usage">
Observable() : new
</pre>

Constructeur permettant de générer une nouvelle instance d'un objet observé, qui est en capacité de notifier ses observateurs des changements survenus. Cette méthode permet d'initialiser les paramètres à leurs valeurs par défaut.

### notify()
<pre class="usage">
notify() : boolean
notify(data) : boolean
</pre>

Méthode permettant de notifier les observateurs d'un changement. Cette notification peut être accompagnée d'une information complémentaire représentée par le paramètre `data` qui n'est pas obligatoire.  

Pour que la notification soit envoyée, il faut que le paramètre `changed` de l'objet soit préalablement passé à la valeur `true`. Dans le cas contraire, la méthode renvoie `false` sans envoyer de notification.

Si le pramètre `changed` de l'objet a pour valeur `true`, la liste des observateurs est parcourue afin d'appeler les méthodes `update()` associées. Si l'observateur n'est pas un objet, qu'il est null ou qu'il ne dispose pas d'une méthode possédant la signature ci-après, celui-ci est ignoré.  

<div class="panel">

<div class="panel-body">
<pre class="usage">
update(observable: Observable[, data: object])
</pre>

Cette méthode consiste à récupérer l'ensemble des notifications des objets observés par l'objet portant cette méthode.

<h4 class="simple">observable</h4>
<blockquote class="documentation">
L'objet à l'origine de la notification. Permet d'adapter le comportement en fonction de l'émetteur du message.
<small>Type : <a href="#p=full/util/Observable"><span class="glyphicon glyphicon-link"></span> <code>Lascar.util.Observable</code></a>, non null</small>
</blockquote>

<h4 class="simple">data</h4>
<blockquote class="documentation">
L'information accompagnant la notification dans l'optique de préciser la nature du changement.
<small>Type : <code>tous</code>, Valeur par défaut : <code>undefined</code></small>
</blockquote>
</div>

<div class="panel-footer">
<strong>Signature de la méthode</strong> <code>update()</code>
</div>

</div>

Une fois l'ensemble des observateur notifié du changement, le paramètre `changed` est passé à la valeur `false` et la méthode retourne `true`.

<h4 class="simple">data</h4>
<blockquote class="documentation">
L'information accompagnant la notification à destination du/des observateurs dans l'optique de préciser la nature du changement.
<small>Type : <code>tous</code>, Valeur par défaut : <code>undefined</code></small>
</blockquote>
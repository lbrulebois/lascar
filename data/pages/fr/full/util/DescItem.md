<ol class="breadcrumb">
    <li><a href="#p=accueil">Documentation</a></li>
    <li><a href="#p=full/index">Documentation détaillée</a></li>
    <li class="active">DescItem</li>
</ol>

`Lascar.util`
<h1 class="simple">DescItem <small>classe</small></h1>
La classe `DescItem`a pour vocation de permettre de décrire la structure attendue d'un paramètre d'un objet. Cette classe doit être utilisée dans le cadre de la validation d'un objet à l'aide de la classe [<span class="glyphicon glyphicon-link"></span> Desc](#p=full/util/Desc).

---
<h2 class="simple">Paramètres</h2>
<h4 class="simple">label</h4>
<blockquote class="documentation">
Libellé du paramètre à retrouver dans l'objet.
<small>Type : <code>string</code>, non null</small>
</blockquote>

<h4 class="simple">type</h4>
<blockquote class="documentation">
Description du type attendu pour le paramètre. Il peut s'agîr d'une chaîne de caractères pour les types de base (<code>object</code>, <code>string</code>, <code>number</code>, <code>function</code>, <code>undefined</code>) ou un constructeur d'une classe.
<small>Type : <code>string / function</code>, non null</small>
</blockquote>

<h2 class="simple">Méthode</h2>

### Constructeur
<pre class="usage">
DescItem(label, type) : new
</pre>

<div class="alert alert-warning" role="alert">
<p><span class="glyphicon glyphicon-fire"></span>
Lors de la génération de descriptions, prenez garde à bien remplir les champs requis. Aucune vérification n'est actuellement effectuée : cela peut être certainement amélioré.</p>
</div>

Constructeur permettant de générer une nouvelle instance de description d'un paramètre dans le cadre de la validation d'un objet. 

<h4 class="simple">label</h4>
<blockquote class="documentation">
Le libellé du paramètre.
<small>Type : <code>string</code>, non null</small>
</blockquote>

<h4 class="simple">type</h4>
<blockquote class="documentation">
La description du type attendu pour le paramètre.
<small>Type : <code>string / function</code>, non null</small>
</blockquote>
<ol class="breadcrumb">
    <li><a href="#p=accueil">Documentation</a></li>
    <li class="active">Tutoriels</li>
</ol>

# Tutoriels
Venez découvrir nos tutoriels en-ligne dont l'objectif est de vous accompagner de manière ludique à l'utilisation de la librairie LASCARjs. Nos tutoriels sont répartis en différentes sections en fonction des besoins et des usages. Bonne découverte !

## Pour bien démarrer

<!-- Niveau : Facile -->
<div class="row">

<!-- Tuto. n°1 : ... -->
<div class="col-sm-4">
<div class="thumbnail">
<div class="caption">
<div class="pull-right"><span class="label label-success">Facile</span></div>
<h3>Utilisation rapide</h3>
<p>Découvrez les prérequis, l'installation et la mise en place rapide d'une carte Leaflet à l'aide de LASCARjs.</p>
<p class="text-center"><a href="#p=simple/index" class="btn btn-success">Accéder au tutoriel</a></p>
</div>
</div>
</div>

</div>

## Prise en main
<!-- Niveau : Moyen -->
<div class="row">

<!-- Tuto. n°1 : ... -->
<div class="col-sm-4">
<div class="thumbnail">
<div class="caption">
<div class="pull-right">
<span class="label label-warning">Moyen</span>
</div>
<h3>Générer une carte personnalisée</h3>
<p>Après avoir généré une carte en une ligne de code, voyons ensemble comment la personnaliser toujours aussi simplement.</p>
<p class="text-center"><a href="#p=tutos/medium/custommap" class="btn btn-warning">Accéder au tutoriel</a></p>
</div>
</div>
</div>

<!-- Tuto. n°2 : ... -->
<div class="col-sm-4">
<div class="thumbnail">
<div class="caption">
<div class="pull-right">
<span class="label label-warning">Moyen</span>
</div>
<h3>Intégrer des marqueurs sur une carte</h3>
<p>Toujours dans cette optique de personnalisation, voyons ensemble comment intégrer des marqueurs sur vos cartes.</p>
<p class="text-center"><a class="btn btn-warning" href="#p=tutos/medium/markersmap">Accéder au tutoriel</a></p>
</div>
</div>
</div>

<!-- Tuto. n°3 : ... -->
<div class="col-sm-4">
<div class="thumbnail">
<div class="caption">
<div class="pull-right">
<span class="label label-warning">Moyen</span>
</div>
<h3>Partager une carte au travers de l'URL</h3>
<p>Nous avons vu comment créer des cartes personnalisées, voyons dorénavant comment les partager au travers d'informations dans l'URL.</p>
<p class="text-center"><a class="btn btn-warning" href="#p=tutos/medium/sharemap">Accéder au tutoriel</a></p>
</div>
</div>
</div>

</div>

<div class="row">

<!-- Tuto. n°4 : ... -->
<div class="col-sm-4">
<div class="thumbnail">
<div class="caption">
<div class="pull-right">
<span class="label label-info"><i class="fa fa-bullhorn" aria-hidden="true"></i> A venir</span>
<span class="label label-warning">Moyen</span>
</div>
<h3>Exploiter une source OverPass dans un layer</h3>
<p>...</p>
<p class="text-center"><a class="btn btn-warning" disabled="disabled">Accéder au tutoriel</a></p>
</div>
</div>
</div>

<!-- Tuto. n°5 : ... -->
<div class="col-sm-4">
<div class="thumbnail">
<div class="caption">
<div class="pull-right">
<span class="label label-info"><i class="fa fa-bullhorn" aria-hidden="true"></i> A venir</span>
<span class="label label-warning">Moyen</span>
</div>
<h3>Exploiter une source GeoJSON dans un layer</h3>
<p>...</p>
<p class="text-center"><a class="btn btn-warning" disabled="disabled">Accéder au tutoriel</a></p>
</div>
</div>
</div>

</div>

## Pour aller plus loin

<div class="row">

<!-- Tuto. n°1 : ... -->
<div class="col-sm-4">
<div class="thumbnail">
<div class="caption">
<div class="pull-right">
<span class="label label-info"><i class="fa fa-bullhorn" aria-hidden="true"></i> A venir</span>
<span class="label label-danger">Difficile</span>
</div>
<h3>Personnaliser le gestionnaire de paramètres dans l'URL</h3>
<p>...</p>
<p class="text-center"><a class="btn btn-danger" disabled="disabled">Accéder au tutoriel</a></p>
</div>
</div>
</div>

</div>

<!--
<pre><code class="language-css line-numbers">p { color: red; }</code></pre>
<script type="text/javascript">
$(function() { Prism.highlightAll(); });
</script>
-->
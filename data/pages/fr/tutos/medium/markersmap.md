<ol class="breadcrumb">
    <li><a href="#p=accueil">Documentation</a></li>
    <li><a href="#p=tutos/index">Tutoriels</a></li>
    <li class="active">Intégrer des marqueurs sur une carte</li>
</ol>

# Intégrer des marqueurs sur une carte

## Prérequis

### Les classiques
Pour utiliser rapidement LASCARjs, il vous faudra vous prémunir au préalable des librairies suivantes :
* **jQuery** (>= 3.1.0)
* **Leaflet** (>= 0.7.7)

Les versions indiquées entre partenthèses sont celles utilisées dans le cadre des démonstrateurs sur la page d'accueil,
mais rien ne vous empêche d'utiliser des versions supérieures ou inférieures. Sachez que nous n'avons pas encore testé 
ces autres versions et ne pouvons pas garantir de support.  

### L'objectif

En repartant de la carte en mode <q>plein écran</q> du tutoriel [<span class="glyphicon glyphicon-link"></span> Utilisation rapide](#p=simple/index), nous allons :
* Intégrer des marqueurs sur une carte à l'aide des composants fournis.

### Sources de base

<div id="exemple_simple">
</div>
<script type="text/javascript">
    $(function() {
        var exemple_simple = (new LascarCode({
			default: "html",
			codes: {
				"html": {
					type: "language-markup",
					name: "HTML",
					src: "data/exemples/simple/index.html"
				},
                "css": {
					type: "language-css",
					name: "CSS",
					src: "data/exemples/simple/style.css"
				},
                "js": {
					type: "language-javascript",
					name: "JavaScript",
					src: "data/exemples/simple/script.js"
				}
			}
		})).build();
        exemple_simple.appendTo($("#exemple_simple"));
    });
</script>

## Etape 1 : Intégrer un marqueur manuellement

Pour cette première étape, on se propose d'intégrer rapidement et manuellement un marqueur. L'idée étant de manipuler le composant   
[<span class="glyphicon glyphicon-link"></span> `Lascar.cl.component.MarkerManager`](#p=full/cl/component/MarkerManager) afin de mieux appréhender son fonctionnement. Celui-ci permet de gérer un seul et unique marqueur, pouvant être partagé sur une ou plusieurs cartes. Les informations permettant sa génération peuvent être programmées ou en provenance de l'URL (ce que nous verrons dans le prochain tutoriel).

### 1.1 - Création du gestionnaire de marqueur
La création d'un gestionnaire de marqueur est simple, il s'agît d'utiliser le constructeur avec la potentielle clé permettant de paramétrer le marqueur depuis l'URL (ce que nous verrons plus tard). Ci-dessous, voici comment j'ai configuré mon gestionnaire de marqueur :

<pre><code class="language-js line-numbers">    // ...
    // Gestionnaire du marqueur identifié par la clé "poi"
    markerMan = new Lascar.cl.component.MarkerManager("poi");

// On associe la carte au gestionnaire du marqueur "poi"
markerMan.addTo(map);</code></pre>

### 1.2 - Génération "manuelle" du marqueur
Pour générer le marqueur, le gestionnaire de marqueur a besoin d'une instance de [<span class="glyphicon glyphicon-link"></span> `Lascar.cl.component.UrlDataMarker`](#p=full/cl/component/UrlDataMarker) valide. Cette instance dispose de l'ensemble des informations nécessaires à la génération du marqueur. Lorsque le code du pictogramme à utiliser n'est pas disponible, le gestionnaire génère le marqueur avec le pictogramme par défaut de Leaflet.

<div class="alert alert-warning">
    <h4 class="title">Attention !</h4>
    <p style="margin-bottom: 10px;">Il est possible que ayez besoin de corriger le chemin d'accès aux images fournies si vous modifiez la structure par défaut de la librairie Leaflet. Dans ce cas, LASCARjs vous propose une méthode (<a href="#p=full/util/correctLp" class="unavailable">Lascar.cl.correctLp</a>) permettant de corriger ce chemin de deux manières :
    </p>
    <ul style="margin-bottom: 10px;">
    <li>Soit en utilisant un regex permettant d'identifier le chemin de la ressource adjacente au répertoire <code>images</code> contenant les images de Leaflet.</li>
    <li>Soit en définissant directement le chemin vers les images de Leaflet.</li>
    </ul>
    <p style="margin-bottom: 10px;">Dans notre situation et vis-à-vis de l'architecture actuelle des démonstrateurs, j'ai corrigé ainsi le chemin vers les images de Leaflet :
    </p>
    <pre><code class="language-js line-numbers">// Pour pouvoir générer les marqueurs en utilisant potentiellement
// l'icône par défaut de Leaflet, il faut au préalable corriger
// le chemin des ressources Leaflet (car elles ne sont pas localisées
// au même endroit qu'initialement prévu dans Leaflet).
Lascar.util.correctLp(/leaflet/);</code></pre>
</div>

Ci-dessous, voici comment j'ai généré un exemple de marqueur :
<pre><code class="language-js line-numbers">// On intègre enfin le marqueur "manuellement" pour le moment !
markerMan.build(new Lascar.cl.component.UrlDataMarker({
    x: 2.6994958,
    y: 48.4020962,
    marker: "castle"
}));</code></pre>

## Etape 2 : Personnaliser les marqueurs

Nous venons de voir que la génération de marqueurs n'était pas difficile, mais celui que nous venons de générer est simple. La question qui se pose maintenant est : <q>comment améliorer les visuels des pictogrammes ?</q>. C'est ce que nous allons voir dans cette section !

### 2.1 - Prérequis

Dans le cadre de cet exemple, j'utilise la notion de <q>POI</q> (Point Of Interest - Point d'intérêt) permettant de signaler la présence d'un lieu pouvant intéresser certaines personnes. Pour signifier ces lieux de manière ergonomique sur ma carte, il me faut posséder les différents marqueurs à utiliser.

Ci-dessous, voici quelques pictogrammes pouvant être utilisés dans le cadre de cet exemple : 
<div class="row">
<div class="col-sm-4">
<div class="thumbnail">
<div class="caption">
<h3>Château</h3>
<p>Pictogramme :</p>
<p class="text-center"><img src="" alt="" /></p>
</div>
</div>
</div>

<div class="col-sm-4">
<div class="thumbnail">
<div class="caption">
<h3>Musée</h3>
<p>Pictogramme :</p>
<p class="text-center"><img src="" alt="" /></p>
</div>
</div>
</div>

<div class="col-sm-4">
<div class="thumbnail">
<div class="caption">
<h3>Point de vue</h3>
<p>Pictogramme :</p>
<p class="text-center"><img src="" alt="" /></p>
</div>
</div>
</div>
</div>

### 2.2 - Intégration des nouvelles ressources

Pour profiter de cette bibliothèque de pictogrammes, il faut dorénavant les intégrer dans le manager de maqueur. Pour ce faire, nous allons commencer par générer les marqueurs Leaflet que nous allons intégrer par la suite dans le gestionnaire de marqueur.

<h4 class="simple">2.2.1 - Générer les instances de <code>L.icon</code></h4>



<h4 class="simple">2.2.2 - Référencer les instances de <code>L.icon</code></h4>

<h4 class="simple">2.2.3 - Pour aller plus loin : généraliser la génération des instances de <code>L.icon</code></h4>

<script type="text/javascript">
$(function() { 
    Prism.fileHighlight(); 
    Prism.highlightAll();
});
</script>
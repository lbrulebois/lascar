<ol class="breadcrumb">
    <li><a href="#p=accueil">Documentation</a></li>
    <li><a href="#p=tutos/index">Tutoriels</a></li>
    <li class="active">Générer une carte personnalisée</li>
</ol>

# Générer une carte personnalisée

## Prérequis

### Les classiques
Pour utiliser rapidement LASCARjs, il vous faudra vous prémunir au préalable des librairies suivantes :
* **jQuery** (>= 3.1.0)
* **Leaflet** (>= 0.7.7)

Les versions indiquées entre partenthèses sont celles utilisées dans le cadre des démonstrateurs sur la page d'accueil,
mais rien ne vous empêche d'utiliser des versions supérieures ou inférieures. Sachez que nous n'avons pas encore testé 
ces autres versions et ne pouvons pas garantir de support.  

### Les objectifs

En repartant de la carte en mode <q>plein écran</q> du tutoriel [<span class="glyphicon glyphicon-link"></span> Utilisation rapide](#p=simple/index), nous allons :
* Intégrer deux cartes en simultané sur une même page en intervenant sur les identifiants.
* Voir comment ajouter, modifier et supprimer les layers utilisés.
* Modifier la position de la carte (via son centre et son niveau de zoom).
* Intervenir sur la génération des contrôles.

### Sources de base

<div id="exemple_simple">
</div>
<script type="text/javascript">
    $(function() {
        var exemple_simple = (new LascarCode({
			default: "html",
			codes: {
				"html": {
					type: "language-markup",
					name: "HTML",
					src: "data/exemples/simple/index.html"
				},
                "css": {
					type: "language-css",
					name: "CSS",
					src: "data/exemples/simple/style.css"
				},
                "js": {
					type: "language-javascript",
					name: "JavaScript",
					src: "data/exemples/simple/script.js"
				}
			}
		})).build();
        exemple_simple.appendTo($("#exemple_simple"));
    });
</script>

## Etape 1 : Intégrer deux cartes en simultané

Pour cette première étape, on se propose de découper la page web en deux, de manière à ce que la carte de gauche reste inchangée (pour le moment) et que celle de droite affiche une vue satellite. Nous utiliserons pour les photos aériennes celles de Google à l'aide de l'URL suivante :
<pre class="usage">
http://{s}.google.com/vt/lyrs=s&x={x}&y={y}&z={z}
</pre>

<div class="alert alert-warning">
    <h4 class="title">Attention !</h4>
    <p style="margin-bottom: 10px;">Le paramètre <code>{s}</code> prend pour valeur une parmis les suivantes : <strong>mt0</strong>, <strong>mt1</strong>, <strong>mt2</strong>, <strong>mt3</strong>. Cela nécessite donc la configuration du paramètre <code>subdomains</code> de l'objet<br/> <a href="http://leafletjs.com/reference-1.0.3.html#tilelayer"><span class="glyphicon glyphicon-link"></span> <code>L.tileLayer</code></a>. En fonction de la version de LASCARjs que vous utilisez, l'exploitation de ce paramètre diffère :</p>
<ul>
<li><p>Version <strong>0.2.0</strong> :<br/>Le paramètre n'est pas géré directement, il va falloir manuellement les intégrer.
Pour ce faire, je vous propose d'intégrer le paramètre comme suit :</p>
<ul type="1">
<li>Intégrer les sous-domaines dans le layer noté ci-après <code>layer</code>.</li>
<li>Redessiner le layer pour prendre en compte la modification.</li>
</ul>
<pre><code class="language-js line-numbers">layer.options.subdomains = ["mt0", "mt1", "mt2", "mt3"];
layer.redraw();
</code></pre>
</li>
<li><p>Version <strong>0.2.1</strong> :<br/>Le paramètre est géré directement à partir du constructeur de la classe <a href="#p=full/cl/layer/LayerDesc"><span class="glyphicon glyphicon-link"></span> <code>Lascar.cl.layer.LayerDesc</code></a>, permettant de référencer les layers à utiliser.</p>
</li>
</ul>
</div>

### 1.1 - Génération des conteneurs
Pour atteindre notre objectif, nous allons dans un premier temps générer les deux conteneurs pour les deux cartes avec un identifiant différent. En effet, pour avoir deux cartes dont la configuration diffère, il est logique que leurs identifiants soient différents. 

Dans le cadre de ce tutoriel, j'utiliserai les identifiants suivants :
* pour la carte de gauche : `l-left-map`.
* pour la carte de droite : `l-right-map`.

Le code HTML associé est donc :
<pre><code class="language-markup line-numbers">&lt;!-- CONTENEUR "lf-map" permettant la génération 
de la carte de gauche -->
&lt;div id="l-left-map">&lt;/div>

&lt;!-- CONTENEUR "lf-map" permettant la génération 
de la carte de droite -->
&lt;div id="l-right-map">&lt;/div></code></pre>

### 1.2 - Configuration des cartes (1/2)
Comme indiqué dans la documentation du constructeur de la classe [<span class="glyphicon glyphicon-link"></span> `Lascar.cl.Map`](#p=full/cl/Map), le premier paramètre : `id` permet de personnaliser l'identifiant du conteneur de la carte, tandis que le second : `options` permet de personnaliser la configuration de la carte (layers, zoom, centre, contrôles, ...). 

Afin que les cartes soient positionnées comme demandé ci-dessus, nous profitons de la possibilité de gérer le style de la carte depuis la configuration JavaScript.

Ci-dessous, voici comment j'ai configuré la carte de gauche. Pour rappel, nous souhaitions garder les mêmes propriétés (zoom, layers, ...), nous avons juste à intégrer le bon identifiant et le style approprié.

<pre><code class="language-js line-numbers">var 
    // Carte de gauche, que l'on positionne sur la première
    // moitié de l'écran, il s'agît là d'une carte "par défaut".
    leftMap = new Lascar.cl.Map("#l-left-map", {
        style: { width: "50%", float: "left" }
    })</code></pre>

### 1.3 - Configuration des cartes (2/2)
Dans la continuité de la configuration de la carte de gauche, nous allons traiter la carte de droite. La seule potentielle difficulté réside dans la gestion des layers de la carte de droite, sinon sa configuration se base grandement sur celle de gauche. Pour réaliser cette étape, je vous invite grandement à commencer par consulter la documentation de la classe [<span class="glyphicon glyphicon-link"></span> `Lascar.cl.layer.LayerDesc`](#p=full/cl/layer/LayerDesc). 

Je vous propose de vous montrer comme je configure le layer avec la vue satellite et vous donne d'autres chemins de layers fournis par Google. 

Comme indiqué dans la documentation, pour configurer un layer au travers de LASCARjs, vous allez avoir besoin des informations suivantes :
* un code d'identification du layer, qui doit être unique.
* un titre permettant d'aider vos utilisateurs à mieux identifier le fond de carte proposé.
* l'url permettant d'accéder au images composant le fond de carte.
* le texte permettant de citer la source à l'origine de ce fond de carte.
* le niveau de zoom maximum et minimum.

Pour rappel, les sous-domaines de l'URL ne sont pas ceux habituellement utilisés par les layers OpenStreetMap : `["mt0", "mt1", "mt2", "mt3"]`. Ci-dessous, voici comment j'ai configuré la carte de droite :

<pre><code class="language-js line-numbers">var 
    // ...
    // Carte de droite, que l'on positionne sur la seconde
    // moitié de l'écran. On en profite pour supprimer les
    // layers par défaut et intégrer ceux de Google.
    rightMap = new Lascar.cl.Map("#l-right-map", {
        style: { width: "50%", float: "right" },
		// Propriété permettant d'empêcher la génération du
		// contrôle de l'échelle.
        controls: { scale: { generate: false } },
        layers: {
			// Lorsque la définition d'un layer a pour valeur 
			// undefined, cela permet de le supprimer de la configuration
			// par défaut.
            "osm_basic": undefined,
            "osm_fr": undefined,
            "satellite": new Lascar.cl.layer.LayerDesc({
                code: "satellite",
                title: "Vue satellite (Google)",
                url: "http://{s}.google.com/vt/lyrs=s&x={x}&y={y}&z={z}",
                attribution: '© Contributeurs d\'<a href="http://openstreetmap.org">OpenStreetMap</a> - Hébergeur : <a href="http://google.com/">Google</a>',
                zoom: {
                    min: 0,
                    max: 20
                },
                subdomains: ["mt0", "mt1", "mt2", "mt3"]
            }),
            // ... ajoutez ici les autres layers que vous souhaitez configurer.
        },
		// On précise ici le nouveau layer par défaut, celui
		// de la vue satellite.
        default_layer: "satellite"
    })</code></pre>

Comme indiqué plus haut, vous trouvez ci-après d'autres adresses de layers fournis par Google et pouvant vous aider à manipuler l'ajout de fond de cartes :
<div class="row">
<div class="col-sm-4">
<div class="thumbnail">
<div class="caption">
<h3>Vue terrain</h3>
<p>Url :<br/>
<code>http://{s}.google.com/vt/lyrs=p&x={x}&y={y}&z={z}</code><br/>
Exemple :</p>
<p class="text-center"><img src="http://mt0.google.com/vt/lyrs=p&x=32&y=21&z=6" alt="" /></p>
</div>
</div>
</div>

<div class="col-sm-4">
<div class="thumbnail">
<div class="caption">
<h3>Vue hybride</h3>
<p>Url :<br/>
<code>http://{s}.google.com/vt/lyrs=s,h&x={x}&y={y}&z={z}</code><br/>
Exemple :</p>
<p class="text-center"><img src="http://mt0.google.com/vt/lyrs=s,h&x=32&y=21&z=6" alt="" /></p>
</div>
</div>
</div>

<div class="col-sm-4">
<div class="thumbnail">
<div class="caption">
<h3>Vue routière</h3>
<p>Url :<br/>
<code>http://{s}.google.com/vt/lyrs=m&x={x}&y={y}&z={z}</code><br/>
Exemple :</p>
<p class="text-center"><img src="http://mt0.google.com/vt/lyrs=m&x=32&y=21&z=6" alt="" /></p>
</div>
</div>
</div>
</div>

## Etape 2 : Gérer la position de la carte
Pour cette seconde étape, on se propose d'impacter la configuration de la position de la carte. Il est possible d'agir sur cette propriété à deux moments : lors de l'initialisation de la carte via son constructeur ou pendant son utilisation via la méthode `apply()`. Pour rappel, la position d'une carte est caractérisée par deux composantes :
* le niveau de de zoom
* les coordonnées du centre de la carte

### 2.1 - Position à l'initialisation
Comme vu dans l'étape précédente, il est possible d'agît sur les propriétés de base d'une carte gérée par LASCARjs. Il vous faudra ainsi personnaliser les paramètres `options.center` et `options.zoom` pour positionner la carte aux coordonnées et au niveau de zomm indiqué.

Dans l'exemple ci-dessous, je souhaite positionner ma cartede droite de la manière suivante :
* **Niveau de zoom** : `9`
* **Centre de la carte** : latitude = `47`, longitude = `0`

<pre><code class="language-js line-numbers">// ...
rightMap = new Lascar.cl.Map("#l-right-map", {
    // ... Autres paramètres
    center: [47, 0],
    zoom: 9
    // ...
});
</code></pre>

### 2.2 - Position durant l'utilisation
Avant toute chose, il est essentiel de parler du <q><em>comment récupérer la position actuelle de la carte ?</em></q> et la réponse est simple : en exploitant la méthode `getConfig()` de la carte qui retourne une instance de [<span class="glyphicon glyphicon-link"></span> `Lascar.cl.component.UrlDataMap`](#p=full/cl/component/UrlDataMap). Cette instance dispose des informations recherchées au travers des paramètres suivants (qui représentent l'état actuel de la carte) :
* `x` : la longitude
* `y` : la latitude
* `zoom` : le niveau de zoom

<div class="alert alert-warning">
<h4 class="title">Attention !</h4>
<p>
Dans le cas de l'événement <code>baselayerchange</code> le code du nouveau layer n'est pas encore intégré dans la configuration, il nous faut donc l'identifier manuellement. Un exemple illustré est disponible dans la classe <a href="#p=full/cl/component/Url"><span class="glyphicon glyphicon-link"></span> <code>Lascar.cl.component.Url</code></a> pour la méthode <code>addTo()</code>.
</p>
</div>

Une fois que vous avez compris comment récupérer la position actuelle d'une carte, la modification n'est pas plus compliquée. Il s'agît d'exploiter la méthode `apply()` et l'une de ses définitions. Dans l'exemple ci-dessous, j'utiliserai la définition permettant de modifier uniquement les coordonnées du centre et le niveau de zoom. Libre à vous d'utiliser la définition qui vous convient le mieux.

<pre><code class="language-js line-numbers">// Dans cet exemple, je souhaite positionner ma carte
// de la manière suivante : 
//    Latitude  : 43
//    Longitude : -1
//    Zoom      : 7
rightMap.apply({
    x: -1, 
    y: 43, 
    zoom: 7
});
</code></pre>

## Etape 3 : Pour aller plus loin - synchroniser les deux cartes
Dans cette dernière étape, je vous propose d'exploiter l'ensemble des connaissances acquises dans l'objectif de faire en sorte de synchroniser les deux cartes : c'est-à-dire que la modification de position (latitude, longitude et zoom) de l'une se répercute sur l'autre.

Pour ce faire, vous allez devoir surveiller les événements de type `moveend` de chacune des cartes. Vous serez ainsi notifié des changements de positions ou de zoom. Il ne reste plus qu'à répercuter les changements sur l'autre carte.  

Attention il vous faudra procéder à une vérification supplémentaire car en l'état, vous venez de générer une belle boucle infinie ... Je vous propose donc de ne récupérer que les notifications liées à l'événement `moveend` lorsque le mouvement n'est pas issu d'un mouvement programmé (qui est dû à un utilisateur).

### Rendu

<div class="row">
<div class="col-sm-12">
    <iframe src="/data/exemples/med__custommap/index.html" width="100%" height="300">
        <p>Votre navigateur ne supporte pas les iframes.</p>
    </iframe>
</div>  
</div>

<a target="_blank" href="/data/exemples/med__custommap/index.html" class="btn btn-sm btn-primary">
    <span class="glyphicon glyphicon-eye-open"></span> Consulter le rendu
</a>

### Sources

Ci-dessous, vous trouverez le code source permettant d'obtenir le rendu ci-dessus. 
Les sources ci-dessous se découpent en 3 parties nécessaires pour générer le résultat final :
* Le code **HTML** : permettant de fournir la structure de la page.
* Le code **CSS** : permettant de personnaliser le contenu de la page.
* Le code **JavaScript** : permettant de générer les cartes.

<div id="exemple_med__custommap">
</div>
<script type="text/javascript">
    $(function() {
        var exemple_med__custommap = (new LascarCode({
			default: "html",
			codes: {
				"html": {
					type: "language-markup",
					name: "HTML",
					src: "data/exemples/med__custommap/index.html"
				},
                "css": {
					type: "language-css",
					name: "CSS",
					src: "data/exemples/med__custommap/style.css"
				},
                "js": {
					type: "language-javascript",
					name: "JavaScript",
					src: "data/exemples/med__custommap/script.js"
				}
			}
		})).build();
        exemple_med__custommap.appendTo($("#exemple_med__custommap"));
    });
</script>

<script type="text/javascript">
$(function() { 
    Prism.fileHighlight(); 
    Prism.highlightAll();
});
</script>
<ol class="breadcrumb">
    <li><a href="#p=accueil">Documentation</a></li>
    <li><a href="#p=tutos/index">Tutoriels</a></li>
    <li class="active">Partager une carte au travers de l'URL</li>
</ol>

# Partager une carte au travers de l'URL

## Prérequis

### Les classiques
Pour utiliser rapidement LASCARjs, il vous faudra vous prémunir au préalable des librairies suivantes :
* **jQuery** (>= 3.1.0)
* **Leaflet** (>= 0.7.7)

Les versions indiquées entre partenthèses sont celles utilisées dans le cadre des démonstrateurs sur la page d'accueil,
mais rien ne vous empêche d'utiliser des versions supérieures ou inférieures. Sachez que nous n'avons pas encore testé 
ces autres versions et ne pouvons pas garantir de support.  

### Les objectifs

En repartant de la carte en mode <q>plein écran</q> disposant de marqueurs du tutoriel [<span class="glyphicon glyphicon-link"></span> Intégrer des marqueurs sur la carte](#p=tutos/medium/markersmap), nous allons :
* Configurer le partage de paramètres de la carte dans l'URL.
* Configurer la gestion d'un marqueur depuis l'URL.

### Sources de base

<div id="exemple_simple">
</div>
<script type="text/javascript">
    $(function() {
        var exemple_simple = (new LascarCode({
			default: "html",
			codes: {
				"html": {
					type: "language-markup",
					name: "HTML",
					src: "data/exemples/simple/index.html"
				},
                "css": {
					type: "language-css",
					name: "CSS",
					src: "data/exemples/simple/style.css"
				},
                "js": {
					type: "language-javascript",
					name: "JavaScript",
					src: "data/exemples/simple/script.js"
				}
			}
		})).build();
        exemple_simple.appendTo($("#exemple_simple"));
    });
</script>

## Etape 1 : Partager la structure d'une carte

### 1.1 - Génération du gestionnaire de paramètres

### 1.2 - Interconnexions des composants

## Etape 2 : Pour aller plus loin - partager une source de données depuis l'URL
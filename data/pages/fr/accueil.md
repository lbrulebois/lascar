<div class="alert alert-warning alert-dismissable">
    <h4 class="title">Attention !</h4>
    <p>La documentation présentée ci-dessous est en cours de rédaction. Certaines sections sont certainement absentes, incomplètes ou peuvent être encore améliorées. Si vous vous en sentez capable n'hésitez par à apporter votre contribution. Nous vous remercions de votre compréhension et de votre soutien !</p>
    <div class="progress progress-striped active">
        <div class="progress-bar" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" style="width: 85%;">85%</div></div>
    <p>
        <a class="btn btn-warning" href="https://framagit.org/lbrulebois/lascar"><span class="glyphicon glyphicon-send"></span> Contribuer</a>
        <a class="btn btn-link" href="#" data-dismiss="alert">ou Ignorer</a>
    </p>
</div>

# Documentation

Bienvenue sur la documentation de la librairie LASCARjs. Vous y trouverez toutes les informations
concernant les fonctionnalités fournies par celle-ci ainsi que des exemples pour vous accompagner
dans la maîtrise de cet utilitaire. Cette documentation se decompose en trois grandes sections :
* Comment utiliser rapidement LASCARjs ?
* Quelle est l'intégralité des fonctionnalités disponibles ?
* Apprendre à utiliser LASCARjs de manière ludique.

## Utilisation rapide 

Vous souhaitez mettre-en-place une carte rapidement en place dans votre application ? Alors cette rubrique est faite pour vous !  
Ainsi il vous sera possible de constater que générer une carte Leaflet est véritablement à la portée de tous. Cliquez sur le
bouton ci-dessous pour en savoir plus : 
  
<a class="btn btn-primary" href="#p=simple/index">Utiliser rapidement LASCARjs</a>

## Documentation détaillée

Vous souhaitez avoir un usage plus avancé des fonctionnalités apportées par la librairie LASCARjs, vous souhaitez personnaliser le fonctionnement de la carte générée : alors cette section est faite pour vous. Vous trouverez le descriptif complet des outils mis-à-disposition et des exemples d'utilisation.  
Cliquez sur le bouton ci-dessous pour en savoir plus :  

<a class="btn btn-primary" href="#p=full/index">Tout savoir sur LASCARjs</a>

Vous souhaitez obtenir plus d'informations sur les évolutions, changements et corrections apportées à la librairie en fonction de la version que vous utilisez : alors cette rubrique est faite pour vous. Cliquez sur le bouton ci-dessous pour en savoir plus :

<a class="btn btn-primary" href="#p=notes/index">Notes de versions</a>

## Tutoriels

Vous souhaitez découvrir les fonctionnalités de notre librairie LASCARjs de manière ludique, vous avez soiffe d'apprendre et de découvrire les méandres de la librairie : alors cette rubrique est faite pour vous. Cliquez sur le bouton ci-dessous pour en savoir plus :

<a class="btn btn-primary" href="#p=tutos/index">Apprendre à utiliser LASCARjs</a>
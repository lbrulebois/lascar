<?php
# ws.php
# Web Services permettant de répondre aux besoins de 
# la documentation en terme de language et de disponibilités
# des pages de la documentation.

# METHODE "sendJson()"
# permettant d'envoyer la réponse JSON auprès de l'utilisateur.
function sendJson($answer) {
    # Si la réponse ne dispose pas de 'statut' (S)
    # on intègre "S => INFO" !
    if(!isset($answer["S"])) {
        $answer["S"] = "INFO";
    }
    
    # enfin on définit l'en-tête du type de contenu ...
    header("Content-Type: application/json; charset=UTF-8");
    # Et on envoie enfin la réponse !
    echo json_encode($answer);
    return true;
}

# METHODE "listLang()"
# permettant de lister les langues disponibles pour la 
# documentation.
function listLang() {
    # 1. On récupère le contenu du dossier actuel
    $items = scandir("./");

    $result = array();
    # 2. On récupère uniquement le nom des dossiers du dossier
    # actuel (et on ignore les résultats '.' et '..').
    foreach($items as $item) {
        if($item != "." && $item != ".." && is_dir($item)) {
            $result[] = $item;
        }
    }

    # 3. On retourne le résultat !
    return $result;
}

# Si aucune action n'est définie, on génère une erreur !
if(!isset($_GET["a"])) {
    $answer = array("S" => "ERROR", "M" => "No action code found.");
    return sendJson($answer);
}

# Sinon, en fonction du service demandé, le traitement
# est différent !
switch($_GET["a"]) {
    # Service permettant de retourner les langues actuellement
    # disponibles pour la documentation. Cela revient à lister
    # le nom des répertoires disponible dans cet espace !
    case "lang":
        $answer = array("lang" => listLang());
    break;

    # Service permettant de vérifier pour quelles langues
    # la page demandée existe
    case "up":
        # On vérifie que l'on dispose d'une requête !
        if(!isset($_GET["q"])) {
            $answer = array("S" => "ERROR", "M" => "No query found.");
            return sendJson($answer);
        }

        $langAvailable = array();
        # Sinon, pour chaque langue disponible, on cherche
        # à vérifier si la page demandée existe !
        foreach(listLang() as $lang) {
            $path = $lang . "/" . $_GET["q"] . ".md";
            if(file_exists($path) && is_file($path)) {
                $langAvailable[] = $lang;
            }
        }

        # On compose enfin la réponse ...
        $answer = array("query" => $_GET["q"], "lang" => $langAvailable);
    break;

    # Par défaut, on indique une erreur !
    default: 
        $answer = array("S" => "ERROR", "M" => "Action code not found.");
    break;
}

# Enfin on retourne la réponse au format JSON
return sendJson($answer);
?>
/* 
LASCAR (LibrAirie Socle CARtographie)
Version     : 0.2.0
Auteur      : Loïc BRULEBOIS
Description :
Librairie permettant de générer facilement une carte 
à partir des tuiles d'OpenStreetMap. Cette Librairie
permet aussi de personnaliser la carte en y intégrant
des données GeoJSON ou OverPass.
*/
(function (global, factory) {
    "use strict";
    // Pour les environnements de type CommonJS ou NodeJS
    // on intègre la librairie dans les modules 
    if (typeof module == "object" && typeof module.exports == "object") {
        module.exports = global.document ?
            factory(global, true) :
            function (g) {
                if (typeof g.document != "object") {
                    throw new Error("Lascar requires a global with a document.");
                }
                return factory(g);
            };
    }
    // Sinon on se contente d'instancier la librairie ...
    else {
        factory(global);
    }
})(typeof window != "undefined" ? window : this, function (global, noGlobal) {
    "use strict";

    // On vérifie que la librairie JQuery est
    // initialisée sans quoi on retourne une erreur !
    if (typeof $ == "undefined") {
        throw {
            'name': "InitError",
            'message': "The jQuery library is required."
        };
    }

    // On vérifie que la librairie Leaflet est
    // initialisée sans quoi on retourne une erreur !
    if (typeof L == "undefined") {
        throw {
            'name': "InitError",
            'message': "The Leaflet library is required."
        };
    }

    var // Version de la librairie
        _version = "0.2.0",
        // Constructeur d'une instance de la librairie
        __Lascar__ = function () {
            // On référence les cartes générées par la
            // librairie pour mieux les identifier par
            // la suite !
            this._maps = {};
        };

    // On ajoute la méthode 'format()' permettant
    // de générer une chaîne de caractères et la 
    // complétant à l'aide de paramètres.
    if (typeof String.prototype.format != "function") {
        /* METHODE "format()"
        permettant de générer une chaîne de caractères
        en replaçant l'indice par l'argument correspondant.
        @param [0..n] : Les arguments à utiliser pour 
        le remplacement.
        @return : La chaîne de caractères. */
        String.prototype.format = function () {
            var _args = arguments;
            return this.replace(/{(\d+)}/g, function (match, value) {
                return typeof _args[value] != "undefined" ? _args[value] : match;
            });
        };
    }

    // On ajoute la méthode 'keys()' permettant de 
    // lister l'ensemble des propriétés d'un objet.
    if (typeof Object.keys != "function") {
        Object.keys = function (o) {
            // Si le paramètre n'est pas un objet, 
            // on retourne une erreur ! 
            if (typeof o != "object" || o == null) {
                throw new Error("Object.keys called on non-object");
            }

            var keys = new Array();
            // Pour chaque élément dans l'objet on 
            // intègre dans le tampon uniquement les
            // propriétés portées par l'objet.
            for (var key in o) {
                if (o.hasOwnProperty(key)) {
                    keys.push(key);
                }
            }
            return keys;
        };
    }

    // On ajoute la méthode 'count()' permettant de
    // compter le nombre d'éléments correspondant
    // au critère fourni dans un tableau.
    if(typeof Array.prototype.count != "function") {
        /* METHODE "count()"
        permettant de compter le nombre d'élements en fonction
        du tri a effectuer.
        @param [0] : la fonction permettant de filtrer sur le 
        type de donnée recherchée.
        @return le nombre de données correspondantes en fonction
        du critère de tri. */
        Array.prototype.count = function(classifier) {
            return this.reduce(function(counter, item) {
                var p = (classifier || String)(item);
                counter[p] = counter.hasOwnProperty(p) ? counter[p] + 1 : 1;
                return counter;
            }, {});
        };
    }

    // On encadre l'utilisation de la méthode 
    // 'window.onhashchange()' à l'aide du composant
    // ci-dessous.
    if (typeof global.HashChange == "undefined") {
        global.HashChange = new ((function () {
            function HashChange() {
                // On génère un tableau des fonctions 
                // à appliquer lorsque l'événement 'hashchange'
                // est appelé.
                this.funcs = new Array();

                // On génère la fonction d'encapsulation
                // associée à 'onhashchange()' ...
                global.onhashchange = function (e) {
                    // S'il s'agît d'un changement d'URL 
                    // programmé, on ne répercute pas 
                    // l'événement.
                    if (global.noApplyHashChange === true) {
                        delete global.noApplyHashChange;
                        return;
                    }

                    // _this.settedUrl = false;
                    // On appelle chacune des fonctions
                    // référencées dans le composant 'HashChange'
                    $.each(global.HashChange.funcs, function (i, f) {
                        f.call();
                    });
                };
            }

            /* METHODE "goto()"
            permettant de changer d'URL tout en évitant 
            de provoquer les fonctions liées à l'événement
            'onhashchange'.
            @param [0] : l'URL sur laquelle renvoyer le navigateur. */
            HashChange.prototype.goto = function (u) {
                // Si l'adresse est identique que celle actuelle
                // on ne provoque pas de redirection !
                if (window.location.href != u) {
                    global.noApplyHashChange = true;
                    global.location.href = u;
                }
            };

            return HashChange;
        })());
    }

    /* CLASSE "DescItem"
    permettant de générer une description 
    servant à décrire un paramètre d'une 
    structure d'un objet. */
    var DescItem = (function () {
        /* CONSTRUCTEUR : permettant d'instancer une
        nouvelle instance d'une description d'un paramètre
        d'un objet.
        @param [0] : Le nom du paramètre.
        @param [1] : Le type du paramètre. */
        function DescItem(l, t) {
            this.label = l;
            this.type = t;
        };

        return DescItem;
    })();

    /* CLASSE "Desc"
    permettant de générer une description
    servant à décrire la structure attendue
    d'un objet. */
    var Desc = (function () {
        /* CONSTRUCTEUR : permettant d'instancier une
        nouvelle instance d'une description d'un objet. 
        @param [0] : Le constructeur auquel rattacher
        la description. */
        function Desc(c) {
            // On initialise la liste des items 
            // décrivant l'objet
            this.items = new Array();
            // Si 'c' est bien un constructeur ...
            if (typeof c == "function") {
                // On attache la description actuelle
                // au prototype de l'objet décrit ...
                c.prototype.description = this;
                // ... et on génère une méthode 'validate()'
                // basée sur la méthode 'check()' permettant
                // de valider la structure de l'objet !
                c.prototype.validate = function () {
                    Lascar.util.check(this, this.description);
                };
            }
        };

        return Desc;
    })();

    /* CLASSE "Observable"
    permettant de générer un objet observable 
    par d'autres objets et capable de les notifier
    d'un changement. */
    var Observable = (function () {
        /* CONSTRUCTEUR : permettant de générer une
        nouvelle instance d'un observable. */
        function Observable() {
            this.changed = false;
            this.observers = new Array();
        };

        /* METHODE "notify()"
        permettant de notifier les observateurs d'un
        changement en leur fournissant une donnée 'd'.
        @param [0] : La donnée à transmettre.
        @return : 'false' -  si aucune notification
        n'a été envoyée, 'true' - sinon. */
        Observable.prototype.notify = function (d) {
            // Si l'objet actuel n'a pas subi de
            // changement, inutile de notifier les 
            // observateurs !
            if (!this.changed) { return false; }
            var _this = this;
            // Pour chaque observateur ...
            $.each(this.observers, function (i, o) {
                // Si l'observateur possède une méthode 
                // 'update(obs, d)', alors on le notifie !
                if (typeof o == "object" && o != null && typeof o.update == "function") {
                    o.update.call(o, _this, d);
                }
            });
            // Une fois l'ensemble des observateurs notifiés,
            // on rebascule l'indicateur de changement à false.
            this.changed = false;

            return true;
        };

        return Observable;
    })();

    // On intègre des fonctions et informations de base
    // à la racine de la librairie ...
    __Lascar__.prototype = {
        // Version de la librairie ...
        version: _version,
        // Méthodes et classes utilitaires
        util: {
            /* CLASSE "DescItem"
            permettant de générer une description d'un
            paramètre nécessaire à un objet. */
            DescItem: DescItem,
            /* CLASSE "Desc"
            permettant de générer une description d'un
            objet nécessaire pour la fonction 'check()'
            ci-dessous. */
            Desc: Desc,
            /* CLASSE "Observable"
            permettant de générer un objet en capacité d'être
            observé et de notifier ses observateurs. */
            Observable: Observable,
            /* METHODE "check()"
            permettant de vérifier l'intégrité d'un objet
            vis-à-vis d'une description.
            @param [0] : L'objet à vérifier
            @param [1] : La description de la structure
            @throws : L'exception retournée si l'objet n'est
            pas conforme à la structure. */
            check: function (o, s) {
                // On commence par vérifier que le premier
                // paramètre est bien un objet ...
                if (typeof o != "object") {
                    throw new Error("The first parameter isn't an object.");
                }
                // ... puis que le deuxième paramètre est bien
                // une instance de 'Desc' ...
                if (!(s instanceof Lascar.util.Desc)) {
                    throw new Error("The second parameter must be a 'Desc' instance.");
                }

                // On s'assure ensuite que la description de
                // l'objet est la même que celle fournie ...
                /*if(!(o.description instanceof Desc) || o.description != s) {
                    throw new Error("The object's description is invalid.");
                }*/

                var propsO = $.map(o, function (property, key) { return key; });
                // On peut enfin s'intéresser à la structure de
                // l'objet fourni en paramètre !
                $.each(s.items, function (i, d) {
                    // Si la propriété n'est pas dans l'objet 
                    // on retourne une erreur !
                    if ($.inArray(d.label, propsO) == -1) {
                        throw new Error("The property '{0}' is waited in the object.".format(d.label));
                    }

                    // En fonction du type décrivant le type 
                    // du paramètre attendu, on ne réagit pas 
                    // de la même manière ...
                    switch (typeof d.type) {
                        // S'il s'agît d'une chaîne de caractères, on
                        // vérifie le type à l'aide de 'typeof' ...
                        case "string":
                            if (typeof o[d.label] != d.type) {
                                throw new Error("The property '{0}' must be a '{1}' (typeof).".format(d.label, d.type));
                            }
                            break;

                        // S'il s'agît d'une fonction de construction,
                        // on vérifie le type à l'aide de 'instanceof' 
                        // ou si le type possède une descripton, on l'exploite ...
                        case "function":
                            // Si la classe possède une description, on 
                            // l'exploite pour validation !
                            if (d.type.description instanceof Desc) {
                                Lascar.util.check(o[d.label], d.type.description);
                            }
                            // Sinon on se contente d'un 'instanceof' ...
                            else if (!(o[d.label] instanceof d.type)) {
                                throw new Error("The property '{0}' must be a '{1}' (instanceof).".format(d.label, d.type.name));
                            }
                            break;

                        // S'il s'agît d'un objet et tout particulièrement
                        // d'une description, on l'exploite ...
                        case "object":
                            if (d.type instanceof Lascar.util.Desc) {
                                Lascar.util.check(o[d.label], d.type);
                            } else {
                                throw new Error("The type described by '{0}' is unknown.".format(d.type));
                            }
                            break;

                        // Sinon on retourne une erreur !
                        default:
                            throw new Error("The type described by '{0}' is unknown.".format(d.type));
                            break;
                    }
                });
            },
            /* METHODE "correctLp()"
            permettant de corriger le chemin des icônes 
            de Leaflet lorsque celui-ci n'a pas été détecté
            automatiquement.
            @param [0] : Le chemin ou le pattern permettant
            d'identifier le chemin de base de leaflet. */
            correctLp: function (p) {
                // S'il s'agît d'un RegExp, on cherche parmis tous
                // les chemins CSS, celui qui correspond au pattern 
                if (p instanceof RegExp) {
                    // Pour chaque feuille de style dans la page,
                    // on cherche celle qui correspond au pattern
                    // pour récupérer le chemin et y ajouter 'images/'
                    $("link[rel='stylesheet']").each(function (i, s) {
                        if (s.href.match(p)) {
                            L.Icon.Default.imagePath = s.href.split(p)[0] + "images/";
                            return true;
                        }
                    });
                }
                // S'il s'agît d'une chaîne de caractères, cela 
                // signifie que l'utilisateur nous a fourni directement
                // le chemin à utiliser 
                else if (typeof p == "string") {
                    L.Icon.Default.imagePath = p;
                }
                // Sinon on retourne une erreur signifiant que l'on
                // ne sait pas traiter l'élément fourni par l'utilisateur.
                else {
                    throw new Error("The parameter must be a 'RegExp' or a 'string'");
                }
            },
            /* METHODE "dropUndefined()"
            permettant de supprimer d'une liste source
            tous les éléments 'undefined' d'une liste
            allant servir d'extension.
            @param [0] : la liste source.
            @param [1] : la liste d'intégration. */
            dropUndefined: function (oS, oI) {
                // Pour chaque propriétés de l'objet,
                // on supprime toutes celles 'undefined'.
                $.each(Object.keys(oI), function (i, k) {
                    if (typeof oI[k] == "undefined") {
                        delete oS[k];
                    }
                    // S'il s'agît d'un objet, on descend
                    // récursivement dedans ...
                    else if (typeof oI[k] == "object" && oI[k] != null) {
                        Lascar.util.dropUndefined(oS[k], oI[k]);
                    }
                });
            },
            /* METHODE "applyLayerOnMap()"
            permettant d'intégrer un layer dans une carte
            existante dans le périmètre de l'objet associé.
            @param [0] : le nouveau layer ou null.
            @param [1] : l'identifiant de la carte. */
            applyLayerOnMap: function (l, i) {
                var m = this.maps[i],
                    // On récupère l'indicateur de mise-à-jour ou non
                    // des informations dans le gestionnaire des layers.
                    updateCtrlLayer = typeof arguments[2] == "boolean" ? arguments[2] : false,
                    // Ainsi que le nom à donner au layer
                    layerName = typeof arguments[3] == "string" ? arguments[3] : "unknown";
                // On commence par récupérer les informations
                // associées à la carte et si elle n'existe pas
                // on retourne une erreur !
                if (typeof m == "undefined") {
                    throw new Error("The map #{0} isn't using this layer.".format(i));
                }

                // Sinon on commence par supprimer le layer
                // actuellement en place  ...
                if (m.layer instanceof L.FeatureGroup) {
                    m.map._leaflet.map.removeLayer(m.layer);
                    // S'il faut mettre-à-jour le gestion des
                    // layers, on s'exécute !
                    if (updateCtrlLayer) {
                        m.map._leaflet.ctrlLayers.removeLayer(m.layer);
                    }
                }

                // On référence le nouveau layer s'il existe ...
                if (l instanceof L.FeatureGroup) {
                    m.layer = $.extend(true, new L.FeatureGroup(), l);
                    m.map._leaflet.map.addLayer(m.layer);
                    // S'il faut mettre-à-jour le gestion des
                    // layers, on s'exécute !
                    if (updateCtrlLayer) {
                        m.map._leaflet.ctrlLayers.addOverlay(m.layer, layerName);
                    }
                }
                // ... et on masque le contrôle "Notifier" ...
                if(m.map._leaflet.ctrlNotifier instanceof Lascar.cl.component.Notifier) {
                    m.map._leaflet.ctrlNotifier.hide();
                }
            },
            /* METHODE "unfold()"
            permettant de 'déplier' un objet de manière
            à retourner pour chaque propriété (quel que soit
            le niveau) avec sa définition complète.
            @param [0] : l'objet à déplier.
            @param [1] : la définition progressivement construite
            de la propriété.
            @param [2] : le tableau des résultats. 
            @return la description des propriétés 'dépliées'. */
            unfold: function (o) {
                // Tampon des propriétés de l'objet.
                var result = arguments[2] || {},
                    // Arguments fournis à la fonction.
                    _args = arguments;
                // Pour chaque propriété de l'objet ...
                $.each(Object.keys(o), function (i, _p) {
                    // ... on commence par récupérer la valeur associée
                    var value = o[_p];
                    // ... on construit l'identifiant de la propriété
                    var id = "{0}{1}".format(typeof _args[1] == "string" ? "{0}.".format(_args[1]) : "", _p);
                    // ... s'il s'agît d'un objet, on le dépile 
                    if (typeof value == "object" && value != null) {
                        Lascar.util.unfold(value, id, result);
                    }
                    // ... sinon onintègre la propriété 
                    else {
                        result[id] = value;
                    }
                });
                // Enfin on retourne 
                return result;
            }
        }
    };

    // On génère les classes (fonctionnalités) apportées 
    // par la librairie ...
    __Lascar__.prototype.cl = {
        /* DOMAINE "component"
        permettant de contenir l'ensemble des 
        classes des composants que l'on peut attacher
        à une carte. */
        component: {},
        /* DOMAINE "layer"
        permettant de contenir l'ensemble des 
        classes des layers que l'on peut attacher 
        à une carte. */
        layer: {}
    };

    /* CLASSE "layer.LayerDesc" 
    permettant de décrire un layer à exploiter 
    par une Carte et ainsi le convertir en une 
    couche exploitable par Leaflet. */
    __Lascar__.prototype.cl.layer.LayerDesc = (function () {
        /* CONSTRUCTEUR : permettant d'instancier une
        nouvelle description d'une couche de layer. 
        @param [0] : les potentielles données qu'il
        est possible d'intégrer à l'initialisation 
        de l'instance. */
        function LayerDesc(d) {
            var isD = typeof d == "object";
            // Si 'd' est un objet, on tente d'intégrer
            // les potentielles valeurs qui nous intéressent.
            this.code = isD && typeof d.code == "string" ? d.code : null;
            this.title = isD && typeof d.title == "string" ? d.title : null;
            this.url = isD && typeof d.url == "string" ? d.url : null;
            this.attribution = isD && typeof d.attribution == "string" ? d.attribution : null;
            this["zoom.min"] = isD && typeof d.zoom == "object" && typeof d.zoom.min == "number" ? d.zoom.min : null;
            this["zoom.max"] = isD && typeof d.zoom == "object" && typeof d.zoom.max == "number" ? d.zoom.max : null;
        };

        /* METHODE "toLeaflet()"
        permettant de générer l'objet Leaflet 
        à partir des informations de cet objet. */
        LayerDesc.prototype.toLeaflet = function () {
            // On commence par vérifier la validité de
            // l'objet actuel ...
            if (typeof this.validate == "function") {
                this.validate();
            }
            // Enfin on peut générer l'objet Leaflet ...
            return new L.tileLayer(this.url, {
                minZoom: this["zoom.min"],
                maxZoom: this["zoom.max"],
                attribution: this.attribution ||
                '&copy; contributeurs d\'<a href="http://openstreetmap.org">OpenStreetMap</a>'
            });
        };

        return LayerDesc;
    })();

    /* CLASSE "layer.UrlBased"
    permettant de générer un layer exploitant 
    des ressources distantes ou un web service. */
    __Lascar__.prototype.cl.layer.UrlBased = (function () {
        /* CONSTRUCTEUR : permettant de générer une nouvelle 
        instance de layer exploitant des ressources distantes.
        @param [0] : les paramètres permettant de récupérer
        les données pour la génération du layer. */
        function UrlBased(p) {
            // On génère le requêteur ...
            this.req = new Lascar.cl.component.Requester(p);
            // ... et on attache l'objet actuel comme 
            // observateur du requêteur.
            this.req.observers.push(this);
            // On génère le layer de base qui sera répercuté
            // dans chacune des 'Map' référencées.
            this.layer = null;
            // On génère aussi la liste des 'Map'
            // exploitant les ressources apportées 
            // par ce composant.
            // <id_carte> => { map: <Map>, layer: <null|Layer> }
            this.maps = {};
            // ainsi qu'un conteneur pour la/les fonctions de
            // parsage des réponses du requêteur.
            this.parseFuncs = new Array();
            // On initialise le conteneur des options.
            this.options = {};

            var _this = this;
            // On intègre les paramètres par défaut
            // dans l'objet actuel (pour commencer) ...
            $.each(Object.keys(this.default), function (i, k) {
                _this.options[k] = typeof _this.default[k] == "object" ?
                    jQuery.extend(true, {}, _this.default[k]) :
                    _this.default[k];
            });
            var options = arguments[1];
            // Et on tient compte des potentielles personnalisations
            // souhaitées par l'utilisateur.
            if (typeof options == "object" && options != null) {
                // On commence par supprimer toutes
                // les nouvelles options définies à
                // undefined !
                Lascar.util.dropUndefined(this.options, options);
                this.options = jQuery.extend(true, this.options, options);
            }
        };

        /* DOMAINE "default"
        permettant de décrire les valeurs par défaut
        des propriétés permettant de générer les
        objets . */
        UrlBased.prototype.default = {
            // On définit les styles par défaut des 
            // types d'objets (point, ligne, forme).
            style: {
                point: {
                    opacity: 1.0,
                    weight: 2,
                    color: "#0088CE",
                    radius: 5
                },
                ligne: {
                    opacity: 1.0,
                    weight: 2,
                    color: "#0088CE"
                },
                forme: {
                    opacity: 1.0,
                    weight: 2,
                    color: "#0088CE"
                }
            }
        };

        /* METHODE "update()"
        permettant de recevoir les notifications des 
        éléments observés et réagir en conséquence.
        @param [0] : l'émetteur de la notification.
        @param [1] : la potentielle information liée. */
        UrlBased.prototype.update = function (e, o) {
            // S'il s'agît d'une notification du requêteur,
            // on doit mettre-à-jour le/les layers !
            if (e == this.req) {
                try {
                    this.parse(o);
                }
                // ... et en cas d'erreur on met-à-jour la
                // notification pour une erreur.
                catch(e) {
                    $.each(this.maps, function (i, m) {
                        m.map._leaflet.ctrlNotifier.show("remove-circle", "Erreur dans le chargement des données ...");
                    });
                }
            }
        };

        /* METHODE "parse()"
        permettant de traiter le parsage de la réponse 
        du requêteur pour intégrer cela sous forme de 
        layer pour chacune des cartes référencées. La/
        les fonctions ont pour objectif de générer/compléter
        le layer qui sera répercuté sur l'ensemble des 
        'Map' référencées.
        @param [0] : la réponse reçue. */
        UrlBased.prototype.parse = function (o) {
            // En cas d'erreur, dans la réponse, on retourne 
            // une erreur !
            if (typeof o.error != "undefined") {
                throw new Error("An error occured during the parsing of the answer.");
            }

            var _this = this;
            // Pour chacune des fonctions de parsage,
            // on l'appelle avec le paramètre fourni.
            $.each(this.parseFuncs, function (i, f) {
                f.call(_this, o);
            });

            var _this = this;
            // Une fois le parsage réalisé, on met-à-jour
            // le layer sur l'ensemble des cartes référencées.
            // -----
            // Le nom de la couche dépend de la présence ou non
            // d'un nom dans les paramètres et du nom du service
            // requêté.
            var layerName = typeof this.options.name == "string" ? 
                this.options.name : this.req.url.substr(this.req.url.lastIndexOf("/") + 1);
            $.each(this.maps, function (i, m) {
                Lascar.util.applyLayerOnMap.call(_this, _this.layer, m.map.id.substr(1), true, layerName);
            });
        };

        /* METHODE "addTo()"
        permettant d'ajouter une copie du layer à la
        carte ou aux cartes 'Map' fournies en paramètre.
        @param [0] : la/les cartes sur lesquelles appliquer
        une copie du layer parsé. */
        UrlBased.prototype.addTo = function (m) {
            // S'il s'agît d'un objet 'Map', on l'intègre
            // si ce n'est pas déjà fait !
            if (m instanceof Lascar.cl.Map && $.inArray(m.id, Object.keys(this.maps)) == -1) {
                // On commence par référencer la carte ...
                this.maps[m.id.substr(1)] = { map: m, layer: null };
                // ... si la carte ne dispose pas encore du composant
                // 'Notifier', on en génère un !
                if(!(m._leaflet.ctrlNotifier instanceof Lascar.cl.component.Notifier)) {
                    m._leaflet.ctrlNotifier = new Lascar.cl.component.Notifier();
                    m._leaflet.ctrlNotifier.addTo(m._leaflet.map);
                }
                // Enfin on applique le layer si nécessaire !
                if(typeof this.layer == "object" && this.layer != null) {
                    Lascar.util.applyLayerOnMap.call(this, this.layer, m.id.substr(1));
                }
            }
            // S'il s'agît d'un tableau, on n'intègre que
            // les instances de 'Map' non encore référencées
            else if (m instanceof Array) {
                var _this = this;
                $.each(m, function (i, _m) {
                    _this.addTo(_m);
                });
            }
            // On retourne enfin le composant 
            return this;
        };

        /* METHODE "apply()"
        permettant de provoquer l'appel du requêteur
        et ainsi la génération des layers. */
        UrlBased.prototype.apply = function () {
            // Avant d'envoyer la requête, on active le
            // loader de chacune des cartes !
            $.each(this.maps, function (i, m) {
                m.map._leaflet.ctrlNotifier.show("refresh", "Chargement des données ...");
            });

            // On envoie la requête ...
            this.req.send(this);
        };

        /* METHODE "setProperties()"
        permettant de générer la popup associé à un objet
        Leaflet à partir des propriétés fournies en paramètre.
        @param [0] : l'objet Leaflet.
        @param [1] : les propriétés attachées à l'objet. */
        UrlBased.prototype.setProperties = function (l, p) {
            // Si l'objet Leaflet n'existe pas ou qu'il
            // n'y a pas de propriétés, on retourne une
            // erreur !
            if (typeof l != "object" || l == null || typeof p != "object" || p == null) {
                throw new Error("setProperties() needs a Leaflet graphical object and a classical object as parameters.");
            }

            // On génère le contenu de la popup de la manière suivante :
            // 1. un conteneur évitant les débordements
            // 2. un tableau tel que pour chaque ligne soit
            //    associée une clé et sa valeur
            var properties = Lascar.util.unfold(p);
            var content =
                '<div class="lascar-ct-popup">' +
                '   <table class="table">' +
                '       <thead>' +
                '           <tr>' +
                '               <th>Clé</th>' +
                '               <th>Valeur</th>' +
                '           </tr>' +
                '       </thead>' +
                '       <tbody>';

            $.each(properties, function (i, v) {
                content +=
                    '           <tr>' +
                    '               <td><code>{0}</code></td>'.format(i) +
                    '               <td>{0}</td>'.format(v) +
                    '           </tr>';
            });

            content +=
                '       </tbody>' +
                '   </table>' +
                '</div>';

            // Enfin on peut attacher le contenu à l'objet Lascar
            l.bindPopup(content);
        };

        return UrlBased;
    })();

    /* CLASSE "layer.GeoJSON"
    permettant de générer un layer exploitant 
    des ressources distantes ou un web service
    retournant des données au format GeoJSON. */
    __Lascar__.prototype.cl.layer.GeoJSON = (function () {
        function GeoJSON() {
            // On appelle le constructeur de 'UrlBased'
            // pour initialiser l'objet.
            __Lascar__.prototype.cl.layer.UrlBased.apply(this, arguments);
            // On initialise les fonctions de parsage
            // nécessaires au bon chargement/rechargement
            // du layer.
            addParseFuncs.call(this);
        };

        // Héritage : class 'UrlBased'
        GeoJSON.prototype = Object.create(__Lascar__.prototype.cl.layer.UrlBased.prototype);
        GeoJSON.prototype.constructor = GeoJSON;

        // -----
        /* DOMAINE "parsing"
        permettant de fournir les différentes méthodes 
        de parsage des différents types d'objets GeoJSON
        manipulables. */
        GeoJSON.prototype.parsing = {};

        /* METHODE "coordinates()"
        permettant de parser les coordonnées des informations
        GeoJSON afin qu'elles soient exploitables par Leaflet.
        @param [0] : la/les coordonnées à traiter.
        @return les coordonnées traitées. */
        GeoJSON.prototype.parsing["coordinates"] = function (d) {
            // S'il s'agît d'autre chose que d'un tableau,
            // on retourne une erreur !
            if (!(d instanceof Array)) {
                throw new Error("coordinates() waits an 'Array' as parameter.");
            }

            // Si nous sommes arrivés à un tableau composés 
            // de coordonnées (nombres) on le traite en tant 
            // que tel !
            if (typeof d[0] == "number") {
                return [d[1], d[0]];
            }
            // Sinon, s'il s'agît d'un tableau composés
            // de tableaux, on les traite récursivement ...
            else if (d[0] instanceof Array) {
                var result = new Array(),
                    _this = this;
                $.each(d, function (i, _d) {
                    result.push(_this.parsing["coordinates"].call(_this, _d));
                });
                return result;
            }
            // S'il ne s'agît pas d'un type traité, on
            // retourne une erreur !
            else {
                throw new Error("Unknown type for coordinates.");
            }
        };

        /* METHODE "Point()"
        permettant de générer l'objet Leaflet associé
        au type 'Point' (GeoJSON).
        @param [0] : les données associées.
        @return le L.Marker associé aux informations fournies. */
        GeoJSON.prototype.parsing["Point"] = function (d) {
            // Si des propriétés de style sont définies, on 
            // les intègre, sinon on utilise celles par défaut.
            var style = typeof arguments[1] == "object" && arguments[1] != null ? arguments[1] : this.options.style.point;
            return new L.circleMarker(this.parsing["coordinates"].call(this, d.coordinates), style);
        };

        /* METHODE "LineString()"
        permettant de générer l'objet Leaflet associé
        au type 'LineString' (GeoJSON).
        @param [0] : les données associées.
        @return le L.polyline associé aux informations fournies. */
        GeoJSON.prototype.parsing["LineString"] = function (d) {
            // Si des propriétés de style sont définies, on 
            // les intègre, sinon on utilise celles par défaut.
            var style = typeof arguments[1] == "object" && arguments[1] != null ? arguments[1] : this.options.style.ligne;
            return new L.polyline(this.parsing["coordinates"].call(this, d.coordinates)).setStyle(style);
        };

        /* METHODE "Polygon()"
        permettant de générer l'objet Leaflet associé
        au type 'Polygon' (GeoJSON).
        @param [0] : les données associées. 
        @return le L.polygon associé aux informations fournies. */
        GeoJSON.prototype.parsing["Polygon"] = function (d) {
            // Si des propriétés de style sont définies, on 
            // les intègre, sinon on utilise celles par défaut.
            var style = typeof arguments[1] == "object" && arguments[1] != null ? arguments[1] : this.options.style.forme;
            return new L.polygon(this.parsing["coordinates"].call(this, d.coordinates)).setStyle(style);
        };

        /* METHODE "Multi()"
        permettant de parser un multi-éléments à partir
        du type des éléments et de la donnée associée.
        @param [0] : le type des éléments.
        @param [1] : la donnée associée.
        @return le layer contenant l'ensemble des éléments. */
        GeoJSON.prototype.parsing["Multi"] = function (t, d) {
            // On génère un layer contenant l'ensemble 
            // des éléments du "multi-éléments".
            var container = new L.FeatureGroup(),
                _this = this,
                style = arguments[2];
            $.each(d.coordinates, function (i, _c) {
                _this.parsing[t].call(_this, { coordinates: _c }, style)
                    .addTo(container);
            });
            // Une fois chaque élément traité, on retourne
            // le layer.
            return container;
        };

        /* METHODE "MultiPoint"
        permettant de générer l'objet Leaflet associé
        au type 'MultiPoint' (GeoJSON).
        @param [0] : les données associées.
        @return le layer contenant l'ensemble des éléments. */
        GeoJSON.prototype.parsing["MultiPoint"] = function (d) {
            return this.parsing["Multi"].call(this, "Point", d, arguments[1]);
        };

        /* METHODE "MultiLineString"
        permettant de générer l'objet Leaflet associé
        au type 'MultiLineString' (GeoJSON).
        @param [0] : les données associées. 
        @return le layer contenant l'ensemble des éléments. */
        GeoJSON.prototype.parsing["MultiLineString"] = function (d) {
            return this.parsing["Multi"].call(this, "LineString", d, arguments[1]);
        };

        /* METHODE "MultiPolygon"
        permettant de générer l'objet Leaflet associé
        au type 'MultiPolygon' (GeoJSON).
        @param [0] : les données associées. 
        @return le layer contenant l'ensemble des éléments. */
        GeoJSON.prototype.parsing["MultiPolygon"] = function (d) {
            return this.parsing["Multi"].call(this, "Polygon", d, arguments[1]);
        };

        /* METHODE "GeometryCollection"
        permettant de générer l'objet Leaflet associé
        au type 'GeometryCollection' (GeoJSON).
        @param [0] : les données associées. 
        @return le layer contenant l'ensemble des géométries. */
        GeoJSON.prototype.parsing["GeometryCollection"] = function (d) {
            // On génère un layer contenant l'ensemble
            // des objets géométriques listés dans 
            // la collection.
            var container = new L.FeatureGroup(),
                _this = this;
            $.each(d.geometries, function (i, _g) {
                // On récupère le fruit de la génération de
                // l'objet géométrique pour l'associer au 
                // conteneur ...
                _this.parsing[_g.type].call(_this, _g)
                    .addTo(container);
            });
            // Une fois chaque élément traité, on retourne
            // le layer.
            return container;
        };

        /* METHODE "Feature"
        permettant de générer l'objet Leaflet associé
        au type 'Feature' (GeoJSON).
        @param [0] : les données associées. 
        @return l'objet Leaflet associé. */
        GeoJSON.prototype.parsing["Feature"] = function (d) {
            // On tente de récupérer les potentielles propriétés 
            // de style pour les appliquer sur le/les objets.
            var style = typeof d.properties == "object" && d.properties != null &&
                typeof d.properties.style == "object" && d.properties.style != null ?
                d.properties.style : null;
            // On commence par générer l'objet géométrique
            // indiqué dans l'objet 'Feature'.
            var geometry = this.parsing[d.geometry.type].call(this, d.geometry, style);
            // Si l'objet 'Feature' possède des propriétés
            // permettant de décrire l'objet on les intègres
            // dans une popup.
            if (typeof d.properties == "object" && d.properties != null) {
                this.setProperties(geometry, d.properties);
            }
            // On encapsule la géométrie dans un layer ...
            var container = new L.FeatureGroup();
            geometry.addTo(container);
            // que l'on retourne enfin ...
            return container;
        };

        /* METHODE "FeatureCollection"
        permettant de générer l'objet Leaflet associé
        au type 'FeatureCollection' (GeoJSON).
        @param [0] : les données associées.
        @return le conteneur de Leaflet. */
        GeoJSON.prototype.parsing["FeatureCollection"] = function (d) {
            // On génère le conteneur des 'Feature'
            var container = new L.FeatureGroup(),
                _this = this;
            $.each(d.features, function (i, f) {
                _this.parsing["Feature"].call(_this, f)
                    .addTo(container);
            });
            // On retourne le conteneur 
            return container;
        };

        // -----

        // On ajoute la fonction de parsage des données
        // GeoJSON pour les intégrer dans les cartes associées.
        var addParseFuncs = function () {
            this.parseFuncs.push(function (o) {
                // On génère l'objet Leaflet ...
                // et dans le cas d'un Point, LineString ou Polygon, 
                // on intègre le fruit de la génération dans un layer !
                var geometry = this.parsing[o.data.type].call(this, o.data);
                if (o.data.type.match(/(Point|LineString|Polygon)/)) {
                    this.layer = new L.FeatureGroup();
                    geometry.addTo(this.layer);
                }
                // Sinon on intègre le fruit de la génération directement !
                else {
                    this.layer = geometry;
                }
            });
        };

        return GeoJSON;
    })();

    /* CLASSE "layer.OverPass"
    permettant de générer un layer exploitant 
    des ressources distantes ou un web service
    retournant des données au format OverPass. */
    __Lascar__.prototype.cl.layer.OverPass = (function () {
        function OverPass() {
            // On appelle le constructeur de 'UrlBased'
            // pour initialiser l'objet.
            __Lascar__.prototype.cl.layer.UrlBased.apply(this, arguments);
            // On initialise les fonctions de parsage
            // nécessaires au bon chargement/rechargement
            // du layer.
            addParseFuncs.call(this);
        };

        // Héritage : class 'UrlBased'
        OverPass.prototype = Object.create(__Lascar__.prototype.cl.layer.UrlBased.prototype);
        OverPass.prototype.constructor = OverPass;

        // -----
        /* DOMAINE "parsing"
        permettant de fournir les différentes méthodes 
        de parsage des différents types d'objets OverPass
        manipulables. */
        OverPass.prototype.parsing = {};

        /* METHODE "node()"
        permettant de parser les informations OverPass
        concernant un noeud afin qu'elles soient exploitables 
        par Leaflet.
        @param [0] : les informations sur le noeud.
        @return l'objet Leaflet généré. */
        OverPass.prototype.parsing["node"] = function (d) {
            // On récupère le style défini dans les options
            var style = this.options.style.point;
            // et on génère le marqueur associé !
            return new L.circleMarker([d.lat, d.lon], style);
        };

        /* METHODE "way()"
        permettant de parser les informations OverPass
        concernant un chemin/ligne afin qu'elles soient  
        exploitables par Leaflet.
        @param [0] : les informations sur le chemin/ligne.
        @return l'objet Leaflet généré. */
        OverPass.prototype.parsing["way"] = function (d) {
            // Un chemin est considéré comme un polygone
            // lorsqu'au moins un de ses points est traversé 
            // plus d'une fois.
            var isPolygon = $.map(d.nodes.count(), function(v,i) { if(v > 1) { return i; } }).length > 0;
            // On récupère le style défini dans les options
            // en fonction de s'il s'agît d'un polygone ou
            // d'une ligne.
            var style = isPolygon ? this.options.style.forme : this.options.style.ligne;
            // on compile les coordonnées de l'ensemble des
            // points composant le chemin ...
            var coordinates = new Array(),
                _this = this;
            $.each(d.nodes, function(i,_n) {
                var node = _this._tmpelems.node[_n];
                coordinates.push([node.lat, node.lon]);
            });
            // et on génère la ligne ou le polygone associée !
            return new (isPolygon ? L.polygon : L.polyline)(coordinates).setStyle(style);
        };

        /* METHODE "relation()"
        permettant de parser les informations OverPass
        concernant une relation/forme afin qu'elles soient  
        exploitables par Leaflet.
        @param [0] : les informations sur la relation/forme.
        @return l'objet Leaflet généré. */
        OverPass.prototype.parsing["relation"] = function (d) {
            // Peut nettement être amélioré en liant les informations
            // et rôles aux objets concernés,voir regrouper les objets
            // de la relation dans un même layer.
            return new L.FeatureGroup();
        };

        // -----

        // On ajoute la fonction de parsage des données
        // OverPass pour les intégrer dans les cartes associées.
        var addParseFuncs = function () {
            // Cette première fonction a pour vocation de
            // récupérer les différents objets et les référencer.
            this.parseFuncs.push(function (o) {
                // On référence le dictionnaire des données 
                this._tmpelems = {
                    node:     {}, // Objets 'node'
                    way:      {}, // Objets 'way'
                    relation: {}  // Objets 'relation'
                };
                // puis pour chaque élément contenu dans le flux
                // on le génère et on le référence dans un "dictionnaire"
                // pour les éléments composés.
                if(o.data.elements instanceof Array) {
                    var _this = this;
                    $.each(o.data.elements, function(i,_e) {
                        _this._tmpelems[_e.type][_e.id] = _e;
                    });
                }
            });

            // Cette seconde fonction a pour vocation de
            // traiter la génération des différents objets.
            this.parseFuncs.push(function (o) {
                // On génère un layer vide ...
                this.layer = new L.FeatureGroup();
                // Si on dispose d'informations, on les traite !
                if(o.data.elements instanceof Array) {
                    var _this = this;
                    $.each(o.data.elements, function(i,_e) {
                        // On commence par générer l'objet Leaflet associé ...
                        var geometry = _this.parsing[_e.type].call(_this, _e);
                        // Si le parsage n'a rien produit, on passe
                        // à la suite, sinon on l'intègre !
                        if(typeof geometry != "undefined") {
                            // Si la ressource générée possède des tags,
                            // on attache une popup !
                            if(typeof _e.tags == "object" && _e.tags != null) {
                                _this.setProperties(geometry, _e.tags);
                            }
                            // Enfin on attache l'élément générer au layer
                            geometry.addTo(_this.layer);
                        }
                    });
                }
                // Une fois le chargement terminé, on
                // supprime les objets temporaires ...
                delete this._tmpelems;
            });
        };

        /* METHODE "apply()" (surchargée)
        permettant d'appliquer la génération du layer 
        OverPass sur une section géographique définie 
        par une BBOX.
        @param [0] : la BBOX à utiliser. */
        OverPass.prototype.apply = function(bounds) {
            // Si l'utilisateur n'a pas défini de requête on
            // retourne une erreur !
            if(typeof this.req.overpassQuery != "string" || $.trim(this.req.overpassQuery) == "") {
                throw new Error("The OverPass layer needs a valid non empty query.");
            }

            // Si l'utilisateur n'a pas défini des coordonnées
            // pour la BBOX, on retourne une erreur !
            if(!(bounds instanceof Array) || bounds.length != 4 || typeof bounds[0] != "number" || 
                typeof bounds[1] != "number" || typeof bounds[2] != "number" || typeof bounds[3] != "number") {
                throw new Error("apply() needs a valid bounds definition.");
            }

            // Avant d'envoyer la requête, on active le
            // loader de chacune des cartes !
            $.each(this.maps, function (i, m) {
                m.map._leaflet.ctrlNotifier.show("refresh", "Chargement des données OverPass ...");
            });

            // On modifie la requête à la volée car elle peut
            // nécessiter d'utiliser des paramètres comme la BBOX.
            this.req.send(this, {data: { data: this.req.overpassQuery.replace(/{{bbox}}/g, bounds.join(",")) } });
        };

        return OverPass;
    })();

    /* CLASSE "component.UrlDataMap"
    permettant de générer un conteneur pour les
    paramètres d'une carte gérée par le gestionnaire
    d'URL. */
    __Lascar__.prototype.cl.component.UrlDataMap = (function () {
        /* CONSTRUCTEUR : permettant d'instancier une
        nouvelle description d'informations pour une carte. 
        @param [0] : les potentielles données qu'il
        est possible d'intégrer à l'initialisation 
        de l'instance. */
        function UrlDataMap(d) {
            var isD = typeof d == "object";
            // Si 'd' est un objet, on tente d'intégrer
            // les potentielles valeurs qui nous intéressent.
            this.x = isD && typeof d.x == "number" ? d.x : null;
            this.y = isD && typeof d.y == "number" ? d.y : null;
            this.zoom = isD && typeof d.zoom == "number" ? d.zoom : null;
            this.layer = isD && typeof d.layer == "string" ? d.layer : null;
        };

        return UrlDataMap;
    })();

    /* CLASSE "component.UrlDataMarker"
    permettant de générer un conteneur pour les
    paramètres d'un marqueur géré par le gestionnaire
    d'URL. */
    __Lascar__.prototype.cl.component.UrlDataMarker = (function () {
        /* CONSTRUCTEUR : permettant d'instancier une
        nouvelle description d'informations pour un marqueur. 
        @param [0] : les potentielles données qu'il
        est possible d'intégrer à l'initialisation 
        de l'instance. */
        function UrlDataMarker(d) {
            var isD = typeof d == "object";
            // Si 'd' est un objet, on tente d'intégrer
            // les potentielles valeurs qui nous intéressent.
            this.x = isD && typeof d.x == "number" ? d.x : null;
            this.y = isD && typeof d.y == "number" ? d.y : null;
            this.marker = isD && typeof d.marker == "string" ? d.marker : null;
        };

        return UrlDataMarker;
    })();

    /* CLASSE "component.Url"
    permettant de générer un gestionnaire de paramètres
    de configuration (layer, x, y et zoom) des cartes 
    gérées par le composant. */
    __Lascar__.prototype.cl.component.Url = (function () {
        /* CONSTRUCTEUR : permettant de générer une nouvelle
        instance du gestionnaire des paramètres dans l'URL. */
        function Url() {
            // On appelle le constructeur de 'Observable'
            // pour initialiser l'objet.
            __Lascar__.prototype.util.Observable.call(this);
            // On référence une fonction lorsque l'événement
            // 'onhashchange' est appelé
            var _this = this;
            global.HashChange.funcs.push(function () {
                _this.apply();
            });
        };

        // Héritage : class 'Observable'
        Url.prototype = Object.create(__Lascar__.prototype.util.Observable.prototype);
        Url.prototype.constructor = Url;

        /* METHODE "addTo()"
        permettant d'associer le composant à une 
        carte afin de l'observer et la notifier 
        des changements de l'URL.
        @param [0] : la/les cartes. */
        Url.prototype.addTo = function (m) {
            // Si le paramètre est un objet de type 'Map'
            // on la référence !
            if (m instanceof Lascar.cl.Map) {
                // On ajoute la carte comme observatrice 
                // et le composant comme observateur de 
                // la carte ...
                this.observers.push(m);
                m.observers.push(this);
                // On en profite provoquer une notification
                // lors d'un changement (zoom ou layer) sur
                // la carte ...
                m._leaflet.map.on("moveend baselayerchange", function (event) {
                    var config = m.getConfig();
                    // Dans le cas de l'événement 'baselayerchange'
                    // le nouveau layer n'est pas encore arrivé
                    // jusqu'à la configuration, il nous faut donc
                    // l'identifier manuellement ...
                    if (typeof event.layer == "object" && event.layer != null) {
                        $.each(m._leaflet.layers, function (i, l) {
                            if (l == event.layer) {
                                config.layer = i;
                            }
                        });
                    }
                    // On notifier le/les composants observateurs ...
                    m.changed = true;
                    m.notify(config);
                });
            }
            // S'il s'agît d'un tableau, pour chaque 
            // carte qu'il contient on la référence !
            else if (m instanceof Array) {
                var _this = this;
                $.each(m, function (i, _m) {
                    _this.addTo(_m);
                });
            }

            // On retourne enfin le composant
            return this;
        };

        /* METHODE "update()"
        permettant de traiter les informations des
        objets observés par le composant.
        @param [0] : l'émetteur de la notification.
        @param [1] : le contenu potentiel associé  
        à la notification. */
        Url.prototype.update = function (e, o) {
            // S'il s'agît d'un composant de type 'Map', 
            // on récupère les potentiels changements 
            // pour cette carte pour les intégrer dans l'URL ...
            if (e instanceof Lascar.cl.Map) {
                // On génère l'encapsulation ...
                var data = {};
                data[e.id.substr(1)] = o;
                // ... permettant la sérialisation.
                this.serialize(data);
            }
        };

        /* METHODE "apply()"
        permettant d'appliquer les paramètres de 
        l'URL en notifiant les cartes observatrices. */
        Url.prototype.apply = function () {
            // On récupère les paramètres dans l'URL
            var params = this.unserialize();
            // et on notifie l'ensemble des observateurs
            // d'un changement ...
            this.changed = true;
            this.notify(params);
        };

        /* METHODE "unserialize()"
        permettant d'extraire les paramètres intégrés
        dans l'URL et permettant de configurer la/les
        cartes observatrices de cet objet. 
        @return : le fruit de la désérialisation.*/
        Url.prototype.unserialize = function () {
            // On commence par récupérer l'URL ...
            var href = window.location.href;
            // L'URL qui nous intéresse doit être de ce format :
            // Dtc = (Z)/(X)/(Y),layer=(L)
            // Dtm = (Lat)/(Long)/(id_marker)
            // Dc = (id)=[Dtc | Dtm]
            // Url = Dc(&Dc)*
            // Objet tampon pour le résultat du parsage
            var results = {},
                posStartData = -1;
            // S'il n'existe pas de données, on retourne
            // l'objet vide !
            if ((posStartData = href.indexOf("#")) == -1) { return results; }
            // Sinon on la traite !
            $.each(href.substr(posStartData + 1).split("&"), function (i, d) {
                // Si la donnée est de la forme (id)=(...) 
                // on peut envisager de la traiter, sinon
                // on part du principe que la valeur associée
                // vaut 'null' ...
                if (d.match(/([a-zA-Z0-9_-]+)=(.*)/)) {
                    // Pour traiter correctement la donnée
                    // associée à la clé, il nous faut identifier
                    // s'il s'agît de données cartographiques ou
                    // de marqueurs.
                    var dataKey = d.substr(0, d.indexOf("=")),
                        dataValue = d.substr(d.indexOf("=") + 1),
                        dataValueMap = undefined;
                    // S'il s'agît de données cartographiques on
                    // les traite en tant que tel !
                    if ((dataValueMap = dataValue.match(/(-?[0-9]*\.?[0-9]*)\/(-?[0-9]*\.?[0-9]*)\/(-?[0-9]*\.?[0-9]*),layer=(.+)/))) {
                        results[dataKey] = new __Lascar__.prototype.cl.component.UrlDataMap({
                            zoom: parseInt(dataValueMap[1]),
                            x: parseFloat(dataValueMap[3]),
                            y: parseFloat(dataValueMap[2]),
                            layer: dataValueMap[4]
                        });
                    }
                    // S'il s'agît de données de marqueurs on
                    // les traite en tant que tel !
                    else if ((dataValueMap = dataValue.match(/(-?[0-9]*\.?[0-9]*)\/(-?[0-9]*\.?[0-9]*)\/(.*)/))) {
                        results[dataKey] = new __Lascar__.prototype.cl.component.UrlDataMarker({
                            x: parseFloat(dataValueMap[2]),
                            y: parseFloat(dataValueMap[1]),
                            marker: dataValueMap[3]
                        });
                    }
                    // Sinon on ne traite pas la donnée ...
                } else {
                    results[d] = null;
                }
            });

            // On retourne enfin le résultat du parsage
            return results;
        };

        /* METHODE "serialize()"
        permettant d'intégrer les paramètres de la/les
        cartes observatrices dans l'URL pour une utilisation
        ultérieure ou pour partager la/les cartes. 
        @param [0] : la potentielle configuration à fusionner. */
        Url.prototype.serialize = function () {
            // On commence par récupérer les paramètres
            // actuellement en place dans l'URL ...
            var params = this.unserialize(),
                additionnal = arguments[0] || {};
            // Si le premier argument est un objet non null,
            // on le fusionne avec les paramètres identifiés
            jQuery.extend(true, params, additionnal);
            // Pour toutes les cartes observées, on
            // récupère leurs configurations et on
            // les intègre dans 'params' sauf celle
            // reçue en paramètre ! ...
            $.each(this.observers, function (i, o) {
                var mapId = undefined;
                if (o instanceof Lascar.cl.Map && typeof additionnal[(mapId = o.id.substr(1))] == "undefined") {
                    params[mapId] = o.getConfig();
                }
            });

            // On peut enfin concatener l'ensemble des
            // éléments dans l'URL ...
            var url = new Array(),
                indice = 0;
            // Pour chaque paramètres ...
            $.each(params, function (i, p) {
                // S'il s'agît de données de carte ...
                if (p instanceof Lascar.cl.component.UrlDataMap) {
                    url.push("{0}={1}/{2}/{3},layer={4}".format(i, p.zoom, p.y, p.x, p.layer));
                }
                // S'il s'agît de données de marqueurs ...
                else if (p instanceof Lascar.cl.component.UrlDataMarker) {
                    url.push("{0}={1}/{2}/{3}".format(i, p.y, p.x, p.marker));
                }
            });

            // On intègre enfin les nouveaux paramères dans l'URL
            global.HashChange.goto((window.location.href.split("#"))[0].concat("#{0}".format(url.join("&"))));
        };

        return Url;
    })();

    /* CLASSE "component.Requester"
    permettant de générer un gestionnaire de requêtes
    AJAX à partir d'une configuration fournie. */
    __Lascar__.prototype.cl.component.Requester = (function () {
        /* CONSTRUCTEUR : permettant de générer une nouvelle
        instance d'un requêteur AJAX.
        @param [0] : les potentielles données qu'il
        est possible d'intégrer à l'initialisation 
        de l'instance. */
        function Requester(d) {
            // On appelle le constructeur de 'Observable'
            // pour initialiser l'objet.
            __Lascar__.prototype.util.Observable.call(this);
            var _this = this;
            // On intègre les paramètres par défaut
            // dans l'objet actuel (pour commencer) ...
            $.each(Object.keys(this.default), function (i, k) {
                _this[k] = _this.default[k];
            });

            // On commence par supprimer toutes
            // les nouvelles options définies à
            // undefined !
            Lascar.util.dropUndefined(this, d || {});
            // Si 'd' est un objet, on tente d'intégrer
            // les potentielles valeurs qui nous intéressent.
            jQuery.extend(true, this, d || {});
        };

        // Héritage : class 'Observable'
        Requester.prototype = Object.create(__Lascar__.prototype.util.Observable.prototype);
        Requester.prototype.constructor = Requester;

        /* DOMAINE "default"
        permettant de décrire les valeurs par défaut
        des propriétés permettant de générer le requêteur. */
        Requester.prototype.default = {
            async: true,
            cache: false,
            crossDomain: false,
            contentType: "application/json",
            dataType: "json",
            headers: {}
        };

        /* METHODE "send()"
        permettant d'envoyer la requête et retourner
        si 'async' == false le résultat de la requête,
        sinon celui-ci sera notifié aux observateurs.
        @param [0] : le nouvel observateur à ajouter si
        celui-ci n'est pas encore référencé en tant que tel.
        @param [1] : les potentiels nouveaux paramètres
        à prendre en compte avant l'envoi. */
        Requester.prototype.send = function (o, p) {
            // ETAPE n°1 : on intègre les potentiels
            // nouveaux paramètres !
            // -----
            // On commence par supprimer toutes
            // les nouvelles options définies à
            // undefined !
            Lascar.util.dropUndefined(this, p || {});
            // Si 'p' est un objet, on tente d'intégrer
            // les potentielles valeurs qui nous intéressent.
            jQuery.extend(true, this, p || {});

            // ETAPE n°2 : on référence si besoin le
            // nouvel observateur.
            if ($.inArray(o, this.observers) == -1) {
                this.observers.push(o);
            }

            // ETAPE n°3 : on procède à l'envoi de la
            // requête mais avant on s'assure que les
            // paramètres minimums sont présents ...
            this.validate();
            var _this = this,
                result = null;
            // ... si tout est conforme, on evoie la requête.
            $.ajax({
                type: this.type,
                url: this.url,
                async: typeof this.async == "boolean" ? this.async : true,
                cache: this.cache || false,
                crossDomain: this.crossDomain || false,
                contentType: this.contentType || "application/json",
                dataType: this.dataType || "json",
                data: this.data || undefined,
                beforeSend: function (xhr) {
                    // S'il y a des en-têtes à intégrer, 
                    // on les intègre ... exemple : "X-CSRF-Token"
                    if (typeof _this.headers == "object" && _this.headers != null) {
                        $.each(Object.keys(_this.headers), function (i, k) {
                            xhr.setRequestHeader(k, _this.headers[k]);
                        });
                    }
                }
            }).done(function (data, status, jqXHR) {
                // On notifie les observateurs de la réponse !
                _this.changed = true;
                _this.notify((result = { data: data, status: status, jqXHR: jqXHR }));
            }).fail(function (jqXHR, status, error) {
                // On notifie les observateurs de l'erreur' !
                _this.changed = true;
                _this.notify((result = { error: error, status: status, jqXHR: jqXHR }));
            });

            // S'il s'agît d'un appel synchrone, on
            // retourne le résultat de la requête.
            if (!(typeof this.async == "boolean" ? this.async : true)) {
                return result;
            }
        };

        return Requester;
    })();

    /* CLASSE "component.MarkerManager"
    permettant de générer une gestionnaire de marqueurs
    à partir des paramètres fournis par un 'component.Url'. */
    __Lascar__.prototype.cl.component.MarkerManager = (function () {
        /* CONSTRUCTEUR : permettant de générer une nouvelle
        instance de gestionnaire de marqueurs pouvant être 
        disposés sur une ou plusieurs cartes. */
        function MarkerManager(k) {
            // On référence les marqueurs utilisable par
            // le manager et étant intégrables sur la/les
            // cartes ...
            this.markers = {};
            // on prépare aussi le conteneur du marqueur ...
            this.layer = null;
            // et on génère aussi la liste des 'Map'
            // exploitant les ressources apportées 
            // par ce composant.
            // <id_carte> => { map: <Map>, layer: <null|Layer> }
            this.maps = {};
            // Si une clé d'identification du paramètre
            // à utiliser dans l'URL est défini, on l'utilise 
            // sans quoi on positionne celle par défaut.
            this.key = typeof k == "string" && $.trim(k) != "" ? k : "marker";
        };

        /* METHODE "link()"
        permettant d'associer le composant actuel au 
        gestionnaire de paramètres dans l'URL et ainsi 
        mettre-en-place le/les marqueurs.
        @param [0] : le gestionnaire des paramètres
        dans l'URL. */
        MarkerManager.prototype.link = function (u) {
            // Si le gestionnaire n'est pas du type 
            // attendu, on retourne une erreur !
            if (!(u instanceof Lascar.cl.component.Url)) {
                throw new Error("link() waits a Lascar.cl.component.Url instance.");
            }
            // Sinon on associe l'instance actuelle au
            // gestionnaire fourni en paramètre.
            u.observers.push(this);
        };

        /* METHODE "update()"
        permettant de recevoir les notifications des
        objets observés par l'instance.
        @param [0] : l'émetteur de la notification.
        @param [1] : la potentielle donnée associée. */
        MarkerManager.prototype.update = function (e, o) {
            // S'il s'agît d'un gestionnaire de paramètre
            // dans l'URL, on récupère les informations
            // associées à la clé de lecture des marqueurs.
            if (e instanceof Lascar.cl.component.Url && $.inArray(this.key, Object.keys(o)) != -1) {
                // On récupère les propriétés du marqueur
                // que l'on valide avant de les exploiter ...
                this.build(o[this.key]);
            }
        };

        /* METHODE "clear()"
        permettant de réinitialiser les layers de marqueurs
        sur la/les cartes associées à ce composant. */
        MarkerManager.prototype.clear = function () {
            // Si aucune génération n'a été faite, 
            // inutile de purger les layers ...
            if (this.layer == null) {
                return false;
            }

            var _this = this;
            // Sinon, pour chaque carte référencée, on
            // supprime les layers associés ...
            $.each(this.maps, function (i, m) {
                if (m.layer instanceof L.FeatureGroup) {
                    m.map._leaflet.map.removeLayer();
                    m.layer = null;
                }
            });
            // ... avant de supprimer le layer dans le 
            // gestionnaire.
            this.layer = null;
        };

        /* METHODE "build()"
        permettant de générer ou regénérer le layer du
        marqueur à partir des données fournies.
        @param [0] : la donnée à exploiter. */
        MarkerManager.prototype.build = function (d) {
            // On s'assure que la donnée fournie soit 
            // nulle ou du type 'UrlDataMarker'.
            if (!(d instanceof Lascar.cl.component.UrlDataMarker) && d != null) {
                throw new Error("build() needs a valid UrlDataMarker or a null parameter.");
            }

            // avant de générer le nouveau si la donnée existe ...
            if (d != null) {
                // On commence par s'assurer que la donnée est
                // valide ...
                d.validate();
                // ... avant de générer ou regénérer le marqueur.
                this.layer = new L.FeatureGroup();
                // Si l'icône du marqueur n'existe pas dans le
                // dictionnaire des marqueurs à disposition, on 
                // utilise le marqueur par défaut de Leaflet.
                var marker = undefined,
                    _this = this;
                if (typeof this.markers[d.marker] != "undefined") {
                    marker = new L.Marker([d.y, d.x], this.markers[d.marker]);
                } else {
                    marker = new L.Marker([d.y, d.x]);
                }
                marker.addTo(this.layer);
                // Une fois le nouveau layer généré, on le répercute
                // dans chacune des cartes référencées !
                $.each(this.maps, function (i, m) {
                    Lascar.util.applyLayerOnMap.call(_this, _this.layer, m.map.id.substr(1));
                });
            }
            // Si la donnée est nulle, on se contente de purger 
            // les layers !
            else {
                this.clear();
            }
        }

        /* METHODE "addTo()"
        permettant d'ajouter une copie du layer à la
        carte ou aux cartes 'Map' fournies en paramètre.
        @param [0] : la/les cartes sur lesquelles appliquer
        une copie du layer parsé. */
        MarkerManager.prototype.addTo = function (m) {
            // S'il s'agît d'un objet 'Map', on l'intègre
            // si ce n'est pas déjà fait !
            if (m instanceof Lascar.cl.Map && $.inArray(m.id, Object.keys(this.maps)) == -1) {
                // On commence par référencer la carte ...
                this.maps[m.id.substr(1)] = { map: m, layer: null };
                Lascar.util.applyLayerOnMap.call(this, this.layer, m.id.substr(1));
            }
            // S'il s'agît d'un tableau, on n'intègre que
            // les instances de 'Map' non encore référencées
            else if (m instanceof Array) {
                var _this = this;
                $.each(m, function (i, _m) {
                    _this.addTo(_m);
                });
            }
            // On retourne enfin le composant 
            return this;
        };

        return MarkerManager;
    })();

    /* CLASSE "Notifier" (composant Leaflet)
    permettant de générer une notification associée
    à une carte Leaflet (chargement, erreur, ...). */
    __Lascar__.prototype.cl.component.Notifier = L.Control.extend({
        options: { position: "topleft" },
        /* METHODE "onAdd()"
        permettant de traiter la génération du composant
        suite à son intégration dans une carte Leaflet.
        @param [0] : la carte Leaflet.
        @return le composant DOM généré. */
        onAdd: function(m) {
            // On génère le conteneur du "loader"
			this.container = L.DomUtil.create("div","lascar-leaflet-loader");
            // On ajoute l'animation ...
            var animation = L.DomUtil.create("p","loader-icon", this.container),
                icon = L.DomUtil.create("span", "glyphicon glyphicon-refresh", animation);
            // ... on le masque par défaut 
            this.hide();
            // ... et on le retourne enfin.
			return this.container;
        },
        /* METHODE "show()"
        permettant d'afficher le composant de chargement
        avec un potentiel message si nécessaire.
        @param [0] : (optionnel) l'icône à associer ou l'icône
        de chargement par défaut.
        @param [1] : (optionnel) le message à intégrer.
        @return le composant. */
        show: function() {
            // Si l'utilisateur souhaite intégrer un
            // message, on en tient compte !
            var $container = $(this.container),
                icone = typeof arguments[0] == "string" ? arguments[0] : "refresh",
                message = typeof arguments[1] == "string" && $.trim(arguments[1]) != "" ? 
                arguments[1] : undefined;
            // Pour commencer, on supprime le message précédent ...
            $container.find(".message").remove();
            // ... et s'il y a un nouveau message, on 
            // le génère !
            if(typeof message == "string") {
                var _ctMessage = L.DomUtil.create("p","message", 
                    this.container);
                $(_ctMessage).css({ 'margin': "0px" })
				.html(message);
            }
            // On actualise l'icône pour ce faire on 
            // annule l'icône précédente ...
            var $icone = $container.find(".glyphicon"),
                oldClass = $icone.attr("class").match(/glyphicon-([a-z-]+)/)[0],
                newClass = "glyphicon-{0}".format(icone);
            // On remplace l'icône ...
            $icone.removeClass(oldClass).addClass(newClass);
            // S'il ne s'agît pas de l'icône "refresh", on 
            // supprime l'animation, sinon on l'ajoute !
            if(newClass == "glyphicon-refresh") { $icone.parent().addClass("loader-icon"); }
            else { $icone.parent().removeClass("loader-icon"); }
            // ... on affiche le composant ...
            $container.show();
            // et enfin on retourne le composant.
            return this;
        },
        /* METHODE "hide()"
        permettant de masquer le composant de chargement.
        @return le composant. */
        hide: function() {
            // On masque le conteneur ...
            $(this.container).hide();
            // et enfin on retourne le composant.
            return this;
        }
    });

    /* CLASSE "Map"
    permettant de générer une carte dîte 'classique'
    à laquelle il est possible d'attacher des composants. */
    __Lascar__.prototype.cl.Map = (function () {
        /* CONSTRUCTEUR : permettant de générer une nouvelle
        instance de carte 'classique'. L'utilisation du constructeur
        est variable : void / string / object / string + object */
        function Map() {
            // On appelle le constructeur de 'Observable'
            // pour initialiser l'objet.
            __Lascar__.prototype.util.Observable.call(this);
            var _this = this;
            // On intègre les paramètres par défaut
            // dans l'objet actuel (pour commencer) ...
            $.each(Object.keys(this.default), function (i, k) {
                _this[k] = typeof _this.default[k] == "object" ?
                    jQuery.extend(true, {}, _this.default[k]) :
                    _this.default[k];
            });

            // On traite ensuite le/les potentiels
            // paramètres permettant de personnaliser
            // la configuration de la carte.

            // CAS : typeof [0] == "string"
            // personnalisation de l'ID de la carte.
            if (typeof arguments[0] == "string") {
                var id = arguments[0];
                // Si la chaîne de caractères ne correspond
                // pas à un ID d'un objet DOM existant, on
                // retourne une erreur !
                if (!id.match(/#([a-zA-Z0-9_-]+)/) || $(id).length == 0) {
                    throw new Error("A map needs a correct DOM Id definition.");
                }
                // Sinon on intègre l'information 
                this.id = id;
            }

            // CAS : typeof [0] == "object" OR 
            // typeof [1] == "object"
            if (typeof arguments[0] == "object" || typeof arguments[1] == "object") {
                var options = typeof arguments[0] == "object" ? arguments[0] : arguments[1];
                // Si les options ne sont pas nulles, on 
                // intègre toutes les propriétés de l'objet
                // dans l'objet actuel.
                if (options != null) {
                    // On commence par supprimer toutes
                    // les nouvelles options définies à
                    // undefined !
                    Lascar.util.dropUndefined(this.options, options);
                    this.options = jQuery.extend(true, this.options, options);
                }
            }

            // On provoque enfin la construction de la
            // carte à l'aide de la fonction 'build()'.
            if (typeof this.build == "function") {
                this.build();
            }
        };

        // Héritage : class 'Observable'
        Map.prototype = Object.create(__Lascar__.prototype.util.Observable.prototype);
        Map.prototype.constructor = Map;

        /* DOMAINE "default"
        permettant de décrire les valeurs par défaut
        des propriétés permettant de générer la carte. */
        Map.prototype.default = {
            // Id par défaut du conteneur de la carte
            'id': "#lf-map",
            // Options par défaut associées à la carte
            'options': {
                // Style spécifique par défaut 
                'style': {
                    'width': "100%",
                    'height': "100%"
                },
                // Point central de la carte (au démarrage)
                'center': [46.53972, 2.43028],
                // Niveau de zoom (au démarrage)
                'zoom': 6,
                // Configuration des contrôles associables à la Carte
                'controls': {
                    // Indicateur de génération du contrôle de zoom
                    'zoom': {
                        'generate': true,
                        'position': "topright"
                    },
                    // Indicateur de génération du contrôle d'échelle
                    'scale': {
                        'generate': true,
                        'position': "bottomright"
                    }
                },
                // Layer par défaut
                'default_layer': "osm_basic",
                // Layers utilisés par la carte 
                'layers': {
                    'osm_basic': new __Lascar__.prototype.cl.layer.LayerDesc({
                        code: "osm_basic",
                        title: "OpenStreetMap",
                        url: "http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
                        attribution: '© Contributeurs d\'<a href="http://openstreetmap.org">OpenStreetMap</a> - Hébergeur : <a href="http://openstreetmap.org/">OpenStreetMap</a>',
                        zoom: {
                            min: 0,
                            max: 19
                        }
                    }),
                    'osm_fr': new __Lascar__.prototype.cl.layer.LayerDesc({
                        code: "osm_fr",
                        title: "OpenStreetMap (FR)",
                        url: "http://{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png",
                        attribution: '© Contributeurs d\'<a href="http://openstreetmap.org">OpenStreetMap</a> - Hébergeur : <a href="http://tile.openstreetmap.fr/">OpenStreetMap (FR)</a>',
                        zoom: {
                            min: 0,
                            max: 18
                        }
                    })
                }
            }
        };

        /* METHODE "build()"
        permettant de construire la carte à partir
        des paramètres fournis ('default' ou 'custom'). */
        Map.prototype.build = function () {
            // ETAPE n°1 : On valide les paramètres avant 
            // de construire la carte.
            var $ctMap = $(this.id);
            // On s'assure que l'identifiant DOM est valide.
            if (typeof this.id != "string" || $ctMap.length == 0) {
                throw new Error("The DOM Id is invalid (string).");
            }

            // On s'assure que les options soient correctes.
            if (typeof this.options != "object" || this.options == null) {
                throw new Error("The options aren't defined (not null, object).");
            }
            // On s'assure que les options disposent de layers
            if (typeof this.options.layers != "object" || this.options.layers == null) {
                throw new Error("A 'Map' object needs layers.");
            }
            var idsLayers = $.map(this.options.layers, function (l, i) { return i; });
            // On s'assure que le layer par défaut est bien
            // défini parmis les layers.
            if ($.inArray(this.options.default_layer, idsLayers) == -1) {
                throw new Error("The 'default_layer' is invalid.");
            }

            // ETAPE n°2 : On peut procéder à la génération
            // de la carte Leaflet.
            var lascarMap = Lascar._maps[this.id.substr(1)];
            // Si la carte Leaflet est déjà générée, on la 
            // supprime pour mieux la regénérer.
            if (typeof lascarMap != "undefined") {
                lascarMap._leaflet.map.remove();
                delete Lascar._maps[this.id.substr(1)];
            }

            // On définit le style du conteneur de la carte
            $ctMap.css(this.options.style);

            this._leaflet = {};
            // On génère enfin la carte Leaflet ...
            this._leaflet.map = new L.map(this.id.substr(1), {
                'center': this.options.center,
                'zoom': this.options.zoom,
                'zoomControl': typeof this.options.controls.zoom != "object" ? true : false
            });
            // Une fois la carte générée, on la 
            // référence dans Lascar !
            Lascar._maps[this.id.substr(1)] = this;

            var _this = this,
                defaultLayer = undefined,
                layersByName = {};
            this._leaflet.layers = {};
            // ... puis on s'intéresse aux layers.
            $.each(Object.keys(this.options.layers), function (i, k) {
                var layer = _this.options.layers[k];
                // On référence le layer Leaflet 
                _this._leaflet.layers[k] = layer.toLeaflet();
                layersByName[layer.title] = _this._leaflet.layers[k];
                // Si la clé est celle du layer par défaut,
                // on le référence.
                if (k == _this.options.default_layer) {
                    defaultLayer = _this._leaflet.layers[k];
                }
            });

            // On référence le gestionnaire des layers
            (this._leaflet.ctrlLayers =
                new L.control.layers(layersByName)
            ).addTo(this._leaflet.map);
            // et on attache le layer par défaut à la carte.
            this._leaflet.map.addLayer(defaultLayer);

            // S'il faut générer le contrôle de zoom, on 
            // l'instancie !
            if (typeof this.options.controls.zoom == "object" && this.options.controls.zoom.generate) {
                (this._leaflet.ctrlZoom = new L.Control.Zoom({
                    'position': this.options.controls.zoom.position
                })).addTo(this._leaflet.map);
            }

            // S'il faut générer le contrôle d'échelle, on
            // l'instancie !
            if (typeof this.options.controls.scale == "object" && this.options.controls.scale.generate) {
                (this._leaflet.ctrlScale = new L.control.scale({
                    'position': this.options.controls.scale.position
                })).addTo(this._leaflet.map);
            }
        };

        /* METHODE "getConfig()"
        permettant de récupérer la configuration
        actuelle de la carte pour une exploitation
        par le composant 'Url' par exemple. */
        Map.prototype.getConfig = function () {
            // On identifie le code du layer actuellement
            // utilisé à partir du layer Leaflet ...
            var actualLayer = null,
                _this = this;
            $.each(this._leaflet.layers, function (i, l) {
                if (l == _this.getActiveBaseLayer().layer) {
                    actualLayer = i;
                }
            });

            // On peut enfin retourner la configuration
            // de la carte !
            return new Lascar.cl.component.UrlDataMap({
                x: this._leaflet.map.getCenter().lng,
                y: this._leaflet.map.getCenter().lat,
                zoom: this._leaflet.map.getZoom(),
                layer: actualLayer
            });
        };

        /* METHODE "getActiveBaseLayer()"
        permettant de récupérer le layer Leaflet
        actuellement affiché sur la carte. */
        Map.prototype.getActiveBaseLayer = function () {
            // Tampon du layer actif définit à null par défaut.
            var activeLayer = null,
                _this = this;
            $.each(this._leaflet.ctrlLayers._layers, function (i, l) {
                // S'il s'agît du layer actuellement utilisé 
                // par la carte, n'étant pas en 'overlay', 
                // alors on a trouvé le layer actif.
                if (_this._leaflet.map.hasLayer(l.layer) && !l.overlay) {
                    activeLayer = l;
                }
            });
            // On retourne enfin le layer Leaflet.
            return activeLayer;
        }

        /* METHODE "update()"
        permettant de traiter les informations des
        objets observés par la carte.
        @param [0] : l'émetteur de la notification.
        @param [1] : le contenu potentiel associé  
        à la notification. */
        Map.prototype.update = function (e, o) {
            // S'il s'agît d'un composant de gestion 
            // des paramètres dans l'URL, on récupère
            // les potentiels changements graphiques
            // à intégrer pour cette carte ...
            if (e instanceof Lascar.cl.component.Url) {
                var params = o[this.id.substr(1)];
                // Les paramètres envoyés concernent-ils
                // la carte actuelle ? Si oui, on applique 
                // les paramètres, sinon on les ignore.
                if (params instanceof Lascar.cl.component.UrlDataMap) {
                    this.apply(params);
                }
            }
        };

        /* METHODE "apply()"
        permettant de modifier l'affichage de la carte
        soit en restaurant son affichage à celui par
        défaut, soit en utilisant la configuration
        fournie en paramètre !.
        UTILISATION :
        [0] apply()
        [0] apply(null)
        [1] apply({layer: ""})
        [2] apply({x: -1, y: -1, zoom: -1})
        [3] apply({x: -1, y: -1, zoom: -1, layer: ""}) */
        Map.prototype.apply = function () {
            // On ne tolère pour cette fonction zéro
            // ou un paramètre, si tout est conforme
            // on détermine le mode d'application !
            // 0 = par défaut
            // 1 = layer
            // 2 = coordonnées
            // 3 = layer + coordonnées
            var applyMod = arguments.length != 0 && arguments.length != 1 ? -1 : 0,
                params = arguments[0] || {};

            // CAS [1] | [3] : On dispose de l'information 'layer'
            try {
                Lascar.util.check(params, Lascar.dc.D_UrlDataMap_Layer);
                applyMod = 1;
            } catch (e) { }

            // CAS [2] | [3] : On dispose des informations coordonnées
            try {
                Lascar.util.check(params, Lascar.dc.D_UrlDataMap_Coords);
                applyMod += 2;
            } catch (e) { }

            // Si le mode ne correspond à aucun mode
            // actuellement configuré on retourne une
            // erreur !
            if (applyMod == -1) {
                throw new Error("The parameters are not sufficient to update the map.");
            }

            // On initialise les paramètres aux valeurs 
            // par défaut avant d'y intégrer les arguments
            // fournis !
            var center = this.options.center,
                zoom = this.options.zoom,
                layerCode = this.options.default_layer;

            // CAS [2] | [3] : le centre et le zoom sont
            // définis dans les paramètres.
            if (applyMod >= 2) {
                center = [params.y, params.x];
                zoom = params.zoom;
            }

            // CAS [1] | [3] : le code du layer est défini,
            // on s'assure toutefois qu'il existe.
            if (applyMod == 1 || applyMod == 3) {
                if (!(this.options.layers[params.layer] instanceof Lascar.cl.layer.LayerDesc)) {
                    throw new Error("The layer {0} isn't configured yet.".format(params.layer));
                }
                layerCode = params.layer;
            }

            // On applique enfin la configuration !
            this._leaflet.map.setView(center, zoom);
            // En ce qui concerne les layers, on commence par
            // supprimer le layer actuel ...
            this._leaflet.map.removeLayer(this._leaflet.layers[this.getConfig().layer]);
            // ... avant d'intégrer le nouveau !
            this._leaflet.map.addLayer(this._leaflet.layers[layerCode]);

            return this;
        };

        return Map;
    })();

    // On génère les descriptions des constructeurs
    // apportés par la librairie ...
    __Lascar__.prototype.dc = {};

    /* DESCRIPTION "D_LayerDesc"
    permettant d'indiquer quels sont les champs
    requis au bon fonctionnement de la classe
    'layer.LayerDesc'. */
    var D_LayerDesc = new __Lascar__.prototype.util.Desc(__Lascar__.prototype.cl.layer.LayerDesc);
    D_LayerDesc.items.push(new __Lascar__.prototype.util.DescItem("code", "string"));
    D_LayerDesc.items.push(new __Lascar__.prototype.util.DescItem("title", "string"));
    D_LayerDesc.items.push(new __Lascar__.prototype.util.DescItem("url", "string"));
    D_LayerDesc.items.push(new __Lascar__.prototype.util.DescItem("attribution", "string"));
    D_LayerDesc.items.push(new __Lascar__.prototype.util.DescItem("zoom.min", "number"));
    D_LayerDesc.items.push(new __Lascar__.prototype.util.DescItem("zoom.max", "number"));
    __Lascar__.prototype.dc.D_LayerDesc = D_LayerDesc;

    /* DESCRIPTION "D_UrlDataMap"
    permettant d'indiquer quels sont les champs
    requis au bon fonctionnement de la classe
    'component.UrlDataMap'. */
    var D_UrlDataMap = new __Lascar__.prototype.util.Desc(__Lascar__.prototype.cl.component.UrlDataMap);
    D_UrlDataMap.items.push(new __Lascar__.prototype.util.DescItem("x", "number"));
    D_UrlDataMap.items.push(new __Lascar__.prototype.util.DescItem("y", "number"));
    D_UrlDataMap.items.push(new __Lascar__.prototype.util.DescItem("zoom", "number"));
    D_UrlDataMap.items.push(new __Lascar__.prototype.util.DescItem("layer", "string"));
    __Lascar__.prototype.dc.D_UrlDataMap = D_UrlDataMap;

    /* DESCRIPTION "D_UrlDataMap_Layer"
    permettant d'indiquer quels sont les champs
    requis pour appliquer une configuration 
    minimale avec un layer. */
    var D_UrlDataMap_Layer = new __Lascar__.prototype.util.Desc();
    D_UrlDataMap_Layer.items.push(new __Lascar__.prototype.util.DescItem("layer", "string"));
    __Lascar__.prototype.dc.D_UrlDataMap_Layer = D_UrlDataMap_Layer;

    /* DESCRIPTION "D_UrlDataMap_Coords"
    permettant d'indiquer quels sont les champs
    requis pour appliquer une configuration 
    minimale avec une latitude, une longitude
    et un niveau de zoom. */
    var D_UrlDataMap_Coords = new __Lascar__.prototype.util.Desc();
    D_UrlDataMap_Coords.items.push(new __Lascar__.prototype.util.DescItem("x", "number"));
    D_UrlDataMap_Coords.items.push(new __Lascar__.prototype.util.DescItem("y", "number"));
    D_UrlDataMap_Coords.items.push(new __Lascar__.prototype.util.DescItem("zoom", "number"));
    __Lascar__.prototype.dc.D_UrlDataMap_Coords = D_UrlDataMap_Coords;

    /* DESCRIPTION "D_UrlDataMarker"
    permettant d'indiquer quels sont les champs
    requis au bon fonctionnement de la classe
    'component.UrlDataMarker'. */
    var D_UrlDataMarker = new __Lascar__.prototype.util.Desc(__Lascar__.prototype.cl.component.UrlDataMarker);
    D_UrlDataMarker.items.push(new __Lascar__.prototype.util.DescItem("x", "number"));
    D_UrlDataMarker.items.push(new __Lascar__.prototype.util.DescItem("y", "number"));
    D_UrlDataMarker.items.push(new __Lascar__.prototype.util.DescItem("marker", "string"));
    __Lascar__.prototype.dc.D_UrlDataMarker = D_UrlDataMarker;

    /* DESCRIPTION "D_Requester"
    permettant d'indiquer quels sont les champs
    requis au bon fonctionnement de la classe
    'component.Requester'. */
    var D_Requester = new __Lascar__.prototype.util.Desc(__Lascar__.prototype.cl.component.Requester);
    D_Requester.items.push(new __Lascar__.prototype.util.DescItem("url", "string"));
    D_Requester.items.push(new __Lascar__.prototype.util.DescItem("type", "string"));
    __Lascar__.prototype.dc.D_Requester = D_Requester;

    // On génère l'instance de la librairie ...
    global.Lascar = new __Lascar__();
});
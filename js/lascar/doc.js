/* 
Documentation pour LASCARjs
Version     : 0.2.0
Auteur      : Loïc BRULEBOIS
Description :
Script permettant d'initialiser le comportement
de l'espace documentaire de la librairie.
*/

/* Lascar.Cookies 
Gestionnaire des cookies de l'application web
associée à la librairie LASCARjs (ajout, modification
ou suppression).
*/
Lascar.Cookies = new ((function () {
    /* CONSTRUCTEUR permettant de générer l'instance 
    du gestionnaire des cookies. */
    function Cookies() {
        // Liste référençant l'ensemble des cookies
        // de la session courante tel que chaque 
        // paramètre corresponde à un cookie :
        // 'clé' => { options + valeur }
        var cookies = (function () {
            var listCookies = {},
                actualCookies = document.cookie.split("; ");
            $.each(actualCookies, function (i, cookie) {
                var _dCookie = cookie.split("=");
                listCookies[_dCookie[0]] = { value: _dCookie[1] };
            });
            return listCookies;
        })();

        /* METHODE "integrate()"
        permettant d'ajouter, modifier ou supprimer
        un cookie référencé dans la liste ci-dessus.
        @param k : la clé du cookie.
        @param v : la potentielle valeur à intégrer
        ou undefined pour supprimer le cookie. */
        this.integrate = function (k, v) {
            // On vérifie la clé fournie !
            Cookies.validate.key(k);

            // Si la valeur à intégrer est "undefined", 
            // cela signifie que l'on souhaite supprimer 
            // cette valeur !
            if (typeof v == "undefined") {
                // On le déréférence de la liste des cookies
                delete cookies[k];
                // et on le supprime !
                document.cookie = "{0}=; expires=Thu, 01 Jan 1970 00:00:00 UTC;".format(k);
            }
            // Sinon il s'agît d'un ajout ou d'une 
            // mise-à-jour !
            else {
                // On vérifie la clé valeur fournie !
                Cookies.validate.val(v);
                // On le référence dans la liste des cookies
                cookies[k] = v;
                // et on l'ajoute ou le met-à-jour !
                document.cookie = "{0}={1}{2}".format(
                    k, v.value,
                    // On concactène enfin le reste des paramètres
                    // dans une chaîne de caractères !
                    (function (params) {
                        var result = "",
                            cleanedParams = jQuery.extend(true, {}, params);
                        // On supprime 'value' pour ne pas le réintégrer à nouveau !
                        delete cleanedParams["value"];

                        // Enfin, pour chaque autre paramètre, ...
                        $.each(Object.keys(params), function (i, _p) {
                            // On l'intègre !
                            result = "{0}; {1}={2}".format(result, _p, params[_p]);
                        });
                        return result;
                    })(v)
                );
            }
        };

        /* METHODE "get()"
        permettant de récupérer les informations 
        associées à un cookie.
        @param k : la clé du cookie. 
        @return les informations du cookie. */
        this.get = function (k) {
            // On vérifie la clé fournie !
            Cookies.validate.key(k);
            // Sinon on retourne les potentielles informations
            // associées à la clé !
            return cookies[k];
        };

        this.getAll = function () {
            return jQuery.extend(true, {}, cookies);
        };
    };

    /* DOMAINE "validate"
    permettant de fournir l'ensemble des fonctions
    de validation liées aux Cookies. */
    Cookies.validate = {
        /* METHODE "validate.key()"
        permettant de valider la clé d'un cookie.
        @param k : la clé à valider. */
        key: function (k) {
            // Si la clé n'est pas une chaîne de caractères
            // on retourne une erreur !
            if (typeof k != "string" || !k.match(/^([a-zA-Z0-9._-]+)$/)) {
                throw new Error("Cookies.validate/key() needs a valid string key.");
            }
        },
        /* METHODE "validate.val()"
        permettant de valider la description d'un cookie.
        @param v : la valeur à valider. */
        val: function (v) {
            // Si la description n'est pas un objet ou est nulle,
            // on retourne une erreur !
            if (typeof v != "object" || v == null) {
                throw new Error("Cookies.validate/val() needs a valid not null object value.");
            }

            // On valide la structure à l'aide de la description
            try { Lascar.util.check(v, Cookies.validate.desc.value); }
            catch (e) { throw new Error("Cookies.validate/val() needs at least a 'value' parameter."); }
        },
        /* DOMAINE "desc"
        permettant de gérer les descriptions nécessaire 
        pour la validation d'objets. */
        desc: {
            value: (function () {
                var d = new Lascar.util.Desc();
                d.items.push(new Lascar.util.DescItem("value", "string"));
                return d;
            })()
        }
    };

    return Cookies;
})());

/* METHODE "getBrowserLanguage()"
permettant de récupérer le code court de la langue
du navigateur.
@return le code court de la langue du navigateur
(ou "fr" par défaut). */
Lascar.util.getBrowserLanguage = function () {
    return (window.navigator.userLanguage || window.navigator.language || "fr").match(/^([a-z]+)/)[0];
};

/* METHODE "getUrlParams()"
permettant de récupérer l'ensemble des paramètres
de l'URL sous forme d'un objet les décrivant.
@return : les paramètres de l'URL. */
Lascar.util.getUrlParams = function () {
    var vars = {};
    // On récupère l'ensemble des paramètres 
    // qu'ils disposent ou non de valeurs.
    $.each(window.location.href.match(/[?&#]+(\w+)(=[\w\/\.]+)?/gi), function (i, m) {
        // On se débarasse des symboles '&' et '?' ...
        m = m.replace(/[&?#]/, "");
        // On extrait les informations intéressantes ...
        var data = m.match(/(\w+)(=)?([\w\/\.]+)?/);
        // et on les assemble.
        vars[data[1]] = data[3];
    });
    return vars;
};

/* Lascar.Documentation
Gestionnaire en charge de l'affichage des pages de
la documentation de la librairie LASCARjs.
*/
Lascar.Documentation = new ((function () {
    /* CONSTRUCTEUR permettant de générer l'instance 
    en charge de la gestion de la documentation. */
    function Documentation() {
        // Gestionnaire en charge de la conversion Markdown > HTML
        this.converter = new showdown.Converter(Documentation.default["mdOptions"]);
        // Conteneur de la page de la documentation
        this.$container = $(Documentation.default["container"]);
        // Requêteur permettant de récupérer le contenu d'une page
        this.r_pageReader = new Lascar.cl.component.Requester({
            type: "GET",
            dataType: "html",
            contentType: "text/markdown; charset=UTF-8"
        });
        // Requêteur vers le webservice de récupération des 
        // langues disponibles dans la documentation
        this.r_langAvailable = new Lascar.cl.component.Requester({
            type: "GET",
            dataType: "json",
            url: "/data/pages/ws.php?a=lang",
            async: false
        });
        // Requêteur vers le webservice de récupération des 
        // traductions disponibles pour une page de la documentation.
        this.r_pageUp = new Lascar.cl.component.Requester({
            type: "GET",
            dataType: "json",
            url: "/data/pages/ws.php?a=up",
            async: false
        });
        // On initialise la réaction lors d'un changement de langue
        $(Documentation.default["select"]).on("changed.bs.select", function(e) {
            // On définit la nouvelle valeur du cookie
            // en charge de la langue ...
            Lascar.Cookies.integrate(Documentation.default["cookieLang"], { 
                value: $(this).selectpicker("val")
            });
            // ... et on rafraichit l'affichage !
            Lascar.Documentation.load();
        });
    };

    /* DOMAINE "default"
    permettant de gérer la configuration par défaut de
    la documentation. */
    Documentation.default = {
        "cookieLang": "lascar.lang",
        "lang": "fr",
        "container": "#documentation .page.container",
        "alert": "#documentation .container .alert-lang",
        "select": "#documentation .container .select-lang",
        "mdOptions": {
            "tables": true
        },
        "error.loading": {
            "*": "An error occured when loading the documentation. Retry later.",
            "fr": "Une erreur est survenue pendant le chargement de la documentation. Merci de réessayer plus tard."
        },
        "error.notrans": {
            "*": "Sorry, no translation is available for this page in your favorite language.",
            "fr": "Désolé, aucune traduction en Français n'est disponible pour cette page."
        },
        "select.lang.label": {
            "*": "Available languages",
            "fr": "Langues disponibles"
        },
        "select.actlang.label": {
            "*": "yours",
            "fr": "votre langue"
        },
        "select.langs": {
            "*": "... (?)",
            "fr": "Français (fr)",
            "en": "English (en)"
        }
    };

    /* METHODE "getLang()"
    permettant de récupérer le code la langue 
    soit souhaité par l'utilisateur, soit de son
    navigateur.
    @return le code de la langue à appliquer. */
    Documentation.prototype.getLang = function () {
        // On commence par récupérer la potentielle 
        // langue définie dans le cookie et celle du navigateur.
        var cookieLang = Lascar.Cookies.get(Documentation.default["cookieLang"]),
            browserLang = Lascar.util.getBrowserLanguage();
        // Si la langue est définie dans le cookie et qu'elle 
        // est valide, on l'exploite, sinon on exploite la langue
        // du navigateur par défaut.
        return typeof cookieLang == "undefined" ? browserLang : cookieLang.value;
    };

    /* METHODE (privée) "checkAndLoadPage()"
    permettant de tenter de charger une page de la langue
    de l'utilisateur ou dans la langue par défaut si non 
    disponible. Cette méthode se charge de vérifier aussi
    l'existence de la page dans la documentation.
    @param pagePath : le chemin de la page de documentation
    à charger ! */
    var checkAndLoadPage = function (pagePath) {
        // On commence par vérifier que la page est disponible
        // dans la langue souhaitée par l'utilisateur !
        var ans_pageUp = this.r_pageUp.send(null, { data: { q: pagePath.replace(".md", "") } }),
            $alertLang = $(Documentation.default["alert"]);

        // S'il n'est pas possible d'obtenir une réponse 
        // du web service, on retourne une erreur en fonction
        // de la langue de l'utilisateur !
        if (typeof ans_pageUp.error != "undefined") {
            throw new Error(Documentation.default["error.loading"][this.getLang()]
                || Documentation.default["error.loading"]["*"]);
        }

        // Sinon on traite la réponse et si elle est négative, on
        // retourne le message associé dans la langue de l'utilisateur !
        if (ans_pageUp.data.S != "INFO") {
            throw new Error("{0}\n{1}".format(
                Documentation.default["error.loading"][this.getLang()]
                || Documentation.default["error.loading"]["*"],
                ans_pageUp.data.M));
        }

        /* METHODE (privée) : permettant de provoquer le 
        chargement de la page désignée par l'URL fournie en 
        paramètre.
        @param urlPage : le chemin de la page à charger. */
        var loadPage = function (urlPage, lang, langs) {
            var $selectLang = $(Documentation.default["select"]),
                _this = this;

            // On met-à-jour le selecteur de langues à partir des
            // informations fournies !
            $selectLang.attr("title", 
                Documentation.default["select.lang.label"][this.getLang()]
                    || Documentation.default["select.lang.label"]["*"]);
            // On vide la liste des langues disponibles ...
            $selectLang.find(".selectpicker.select-lang option").remove();

            // Et on intègre les nouvelles langues !
            $.each(langs, function(i, _lang) {
                var $option = undefined;
                // On génère l'option !
                ($option = $("<option>", {
                    value: _lang,
                    html: Documentation.default["select.langs"][_lang] || Documentation.default["select.langs"]["*"]
                })).appendTo($selectLang.find(".selectpicker.select-lang"));
                // et s'il s'agît de la langue de l'utilisateur, on
                // le précise !
                if(_lang == _this.getLang()) {
                    $option.attr("data-subtext", " - {0}".format(
                        Documentation.default["select.actlang.label"][_this.getLang()]
                            || Documentation.default["select.actlang.label"]["*"]));
                }
            });
            // On actualise enfin le rendu de la liste
            $selectLang.selectpicker("refresh").selectpicker("render");
            // S'il s'agît de la langue de l'utilisateur, on 
            // la sélectionne !
            if(lang == this.getLang()) { $selectLang.selectpicker("val", lang); }
            // Sinon on ne sélectionne aucune langue 
            // pour permettre le choix de l'utilisateur !
            else { $selectLang.selectpicker("val", ""); }
            
            this.r_pageReader.send(this, { url: "data/pages/{0}".format(urlPage) });
        };

        // Sinon on analyse dans quelles langues est disponible la page ...
        // S'il n'y a aucune langue, cela signifie que la page
        // n'existe pas !
        if (ans_pageUp.data.lang.length == 0) {
            // On masque le message d'avertissement ...
            $alertLang.hide();
            checkAndLoadPage.call(this, "erreurs/404.md");
        }
        // Sinon on vérifie si la langue de l'utilisateur est proposée
        // pour la page demandée, si elle ne l'est pas, on charge un message
        // et on affiche la page dans la langue par défaut si possible !
        else if ($.inArray(this.getLang(), ans_pageUp.data.lang) == -1) {
            // On intègre le message d'avertissement ...
            $alertLang.show().find(".message").html(
                Documentation.default["error.notrans"][this.getLang()] || Documentation.default["error.notrans"]["*"]
            );
            var urlPage = pagePath,
                lang = ans_pageUp.data.lang[0];
            // On vérifie si la page est disponible dans la langue 
            // par défaut de la documentation ... si elle ne l'est pas,
            // on utilise la première langue disponible !
            if ($.inArray(Documentation.default["lang"], ans_pageUp.data.lang) == -1) {
                urlPage = "{0}/{1}".format(lang, urlPage);
            }
            // Sinon on utilise la langue par défaut !
            else {
                lang = Documentation.default["lang"];
                urlPage = "{0}/{1}".format(lang, urlPage);
            }

            loadPage.call(this, urlPage, lang, ans_pageUp.data.lang);
        }
        // Sinon on charge la page !
        else {
            var lang = this.getLang();
            // On masque le message d'avertissement ...
            $alertLang.hide();
            loadPage.call(this, "{0}/{1}".format(lang, pagePath), lang, ans_pageUp.data.lang);
        }
    };

    /* METHODE "load()"
    permettant de charger la page contenue dans
    le paramètre de l'URL 'p'. */
    Documentation.prototype.load = function () {
        var page = ["accueil"],
            // On récupère les paramètres dans l'URL
            // pour déterminer la page à afficher.
            params = Lascar.util.getUrlParams();
        if (typeof params["p"] == "string" && $.trim(params["p"]) != "") {
            page = params["p"].split("/");
        }

        var pagePath = (function (p) {
            var path = "";
            // Pour chaque éléments dans le tableau, 
            // on l'intègre dans le chemin de la page ...
            $.each(p, function (i, _p) {
                path = path.concat(_p);
                // S'il s'agît du dernier élément du 
                // tableau, c'est le fichier.
                if (i == p.length - 1) {
                    path = path.concat(".md");
                } else {
                    path = path.concat("/");
                }
            });
            return path;
        })(page);

        // On vérifie l'accessibilité de le page et on 
        // la charge potentiellement !
        checkAndLoadPage.call(this, pagePath);
    };

    /* METHODE "update()"
    permettant de recevoir les notifications des 
    éléments observés par le gestionnaire de Documentation.
    @param e : l'émetteur de la notification.
    @param o : l'objet optionnel fourni. */
    Documentation.prototype.update = function (e, o) {
        // S'il s'agît d'une réponse de l'intégrateur de
        // pages ...
        if (e == this.r_pageReader) {
            // En cas d'erreur, on la traite !
            if (typeof o.error != "undefined") {
                // S'il s'agît d'une erreur HTTP on la traite 
                if (o.jqXHR.status > 0) {
                    checkAndLoadPage.call(this, "erreurs/{0}".format(o.jqXHR.status));
                }
                // Sinon on retourne une erreur ...
                else {
                    throw new Error(Documentation.default["error.loading"][this.getLang()]
                        || Documentation.default["error.loading"]["*"]);
                }
            } else {
                this.$container.html(this.converter.makeHtml(o.data));
                window.scrollTo(0, 0);
            }
        }
    };

    return Documentation;
})());

// A chaque changement dans l'URL, on récupère
// les paramètres pour charger la page de la
// documentation associée.
var loadPage = function () {
    Lascar.Documentation.load();
};
window.HashChange.funcs.push(loadPage);
$(window).on("load", loadPage);
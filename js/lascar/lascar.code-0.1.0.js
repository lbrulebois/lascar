/**
 * LASCAR / LibrAirie Socle CARtographie
 * Module pour la documentation de la librairie Lascar 
 * permettant de générer un conteneur pour montrer et 
 * colorer du code à partir des informations fournies.
 * Version : 0.1.0 (à mettre au propre !)
 * Auteur  : Loïc BRULEBOIS
 */

try {
	// ----- INITIALISATION -----
	// On vérifie aussi que la librairie JQuery est
	// initialisée sans quoi on retourne aussi une erreur !
	if(typeof $ == "undefined") {
		throw {
			'name': "InitError",
			'message': "The jQuery library is required."
		};
	}

	// On vérifie aussi que la librairie Prism est
	// initialisée sans quoi on retourne aussi une erreur !
	if(typeof Prism == "undefined") {
		throw {
			'name': "InitError",
			'message': "The Prism library is required."
		};
	}

	// CLASSE "LascarCode"
	// Classe permettant de générer des conteneurs de codes
	// avec une coloration synthaxique.
	var LascarCode = (function() {
		// ----- CONSTRUCTEUR -----
		function __constructor__() {
			// On vérifie qu'il y a bien un paramètre de fourni
			// (les options et les codes sources à afficher) ...
			if(arguments.length != 1 || typeof arguments[0] != "object") {
				throw {
					'name': "InitConfig",
					'message': "The LascarCode class needs an object as first (and only) parameter."
				}
			}

			var options = arguments[0];
			// ... et que ces options sont bien formattées
			var waited = [
				{'name': "default", 'type': "string"},
				{'name': "codes", 'type': "object"}
			];
			LascarCode.prototype.utils.check(options,waited);

			var existDefault = false;
			// On vérifie que pour chaque code source fourni, 
			// les objets soient bien structurés et que le code
			// par défaut existe !
			for(var cleCode in options.codes) {
				var code = options.codes[cleCode];
				waited = [
					{'name': "type", 'type': "string"},
					{'name': "name", 'type': "string"},
					{'name': "src", 'type': "string"}
				];
				LascarCode.prototype.utils.check(code,waited);

				if(cleCode == options.default) {
					existDefault = true;
				}
			}

			if(!existDefault) {
				throw {
					'name': "InitConfig",
					'message': "The LascarCode class needs an 'default' code that exists in 'codes'."
				}
			}

			// Si tout est conforme, on peut générer l'objet !
			this.version = "0.1.0";  // Version de la librairie
			this.$ = {};             // Conteneur à variables
			this._options = options; // Options associées à l'objet
		};

		// METHODE "build()" : permettant de générer le conteneur pour
		// afficher du/des codes sources pour l'associer par la suite 
		// à son composant de destination.
		__constructor__.prototype.build = function() {
			// On commence par récupérer le dernier ID fourni
			// à un conteneur de code 
			var liCodes = $("." + LascarCode._defaults.class);
			var lastId = 0;

			// S'il y a au moins un élément, on récupère le 
			// dernier afin d'extraire son ID
			if(liCodes.length > 0) {
				lastId = parseInt($(liCodes[liCodes.length-1])
					.attr("id").replace(/ct-code-/,""));
			}

			// On génère le conteneur de code ...
			var conteneur = $("<div>", {
				'id': "ct-code-" + (lastId+1),
				'class': LascarCode._defaults.class
			});

			var _this = this;
			var onglets = '<ul class="nav nav-tabs" role="tablist">';
			var corps = '<div class="tab-content">';
			// On génère ensuite les différents composants
			// dans un premier temps, les onglets pour chaque 
			// code !
			$.each(this._options.codes, function(indice, code) {
				var idCode = 'ct_' + (lastId+1) + '_' + indice;
				onglets += 
					'<li role="presentation"' + (indice == _this._options.default ? ' class="active"' : '') + '>' +
					'	<a href="#' + idCode + '" role="tab" data-toggle="tab">' +
					'	' + code.name + 
					'	</a>' +
					'</li>';

				corps += 
					'<div role="tabpanel" class="tab-pane' + (indice == _this._options.default ? ' active' : '') + '" id="' + idCode + '">' +
					'	<pre class="' + code.type + ' line-numbers" data-src="' + code.src + '"></pre>' +
					'</div>';
			});

			onglets += '</ul>';
			corps += '</div>';

			$(onglets).appendTo(conteneur);
			$(corps).appendTo(conteneur);

			return conteneur;
		};

		// ----- MODULE "utils" -----
		// Contenant un lot de fonctions utilisées par le reste
		// des classes de la librairie et pouvant être utiles
		// aux développeurs.
		__constructor__.prototype.utils = {
			// ----- PARAMETRES -----
			'version': "0.3.1",
			// ----- METHODES -----
			// METHODE "clone()" : permettant de copier tous les
			// paramètres d'un objet dans un nouvel objet retourné.
			// @param [0] : l'objet dont les paramètres sont à cloner
			// @return : l'objet cloné
			// @throws BadConfig : le paramètre fourni n'est pas
			// conforme (n'est pas un objet).
			'clone': function() {
				var source = arguments[0];

				// Si l'objet source n'est pas un objet, on retourne
				// une erreur !
				if(typeof source != "object") {
					throw {
						'name': "BadConfig",
						'message': "The waited parameter must be an 'object'."
					};
				}

				// Si l'objet source vaut 'null', on retourne 'null'
				// directement !
				if(source == null) {
					return null;
				}

				// Sinon, on génère un nouvel objet et on copie ses
				// paramètres !
				var nouveau = new source.constructor();
				$.each(source, function(key, item) {
					nouveau[key] = item;
				});
				// Enfin on retourne le clone généré
				return nouveau;
			},
			// METHODE "group()" : permettant d'assembler les
			// propriétés de deux objets de manière à ce que
			// par défaut, toutes les propriétés du premier
			// soient présentes, et que pour toutes celles aussi
			// présentes dans le second, elles soient remplacées.
			// @param [0] : le premier objet
			// @param [1] : le deuxième objet (si null, les propriétés
			// du premier objet seront retournées telle quelle).
			// @return l'objet issu de la fusion des deux objets
			// @throw BadConfig : la configuration fournie ne
			// permet pas le bon déroulement de la fonction.
			'group': function() {
				var base = Lascar.prototype.utils.clone(arguments[0]);
				var complement = arguments[1];

				// On vérifie que les paramètres fournis sont corrects
				// sans quoi on retourne une erreur
				if(typeof base != "object" && typeof complement != "object" && typeof complement != "undefined") {
					throw {
						'name': "BadConfig",
						'message': "The first parameter must be an object and the second one can be an object (or null)."
					};
				}

				// Si le deuxième argument est null ou undefined, on 
				// retourne directement le premier objet ...
				if(typeof complement == "undefined" || complement == null) {
					return base;
				}

				// Sinon, il faut remplacer les propriétés du premier
				// objet par celles proposées dans le second ...
				var compilation = base;
				$.each(complement, function(key,item) { 
					// S'il ne s'agît pas d'un objet, on fusionne
					if(typeof item != "object") {
						if(typeof item != "undefined") {
							compilation[key] = item; 
						} else {
							delete compilation[key];
						}
					} 
					// Sinon, on applique la méthode "group()" 
					// sur cet objet !
					else {
						var compiledObject = item;
						if(typeof compilation[key] != "undefined") {
							compiledObject = Lascar.prototype.utils.group(compilation[key],item);
						}
						compilation[key] = compiledObject;
					}
				});
				return compilation;
			},
			// METHODE "blink()" : permettant de provoquer une réaction de 
			// clignotement sur un objet jQuery à une cadence paramétrable.
			// @param [0] : l'objet jQuery
			// @param [1] : les potentielles options à intégrer
			'blink': function() {
				var item = arguments[0];
				var options = arguments[1];
				
				// L'item doit être un objet jQuery !
				if(typeof item != "string") {
					throw {
						'name': "BadType",
						'message': "The 'item' parameter must be a string value."
					}
				}
				
				// Par défaut, le timeout est de 1s
				var timeout = 1000;
				
				// S'il y a des options on les en prend compte
				// (la seule option que l'on attend c'est la durée)
				if(typeof options == "number") {
					timeout = options;
				} else if(typeof options == "object" && typeof options.timeout == "number") {
					timeout = options.timeout;
				}
				
				// On applique l'effet de style sur l'item
				setInterval(function() {
					$(item).fadeOut(timeout/2);
					$(item).fadeIn(timeout/2); 
				}, timeout);
			},
			// METHODE "check()" : permettant de vérifier que les paramètres
			// de premier niveau d'un objet sont conformes à la description
			// qui en a été faite. Dans le cas contraire, une erreur est
			// retournée permettant d'identifier la source de l'anormalie.
			// @param [0] : l'objet/les données à contrôler
			// @param [1] : les propriétés attendues décrites de la manière
			// suivante : Array[ { 'name': "", 'type': ... } ]
			// @throws "BadConfig" : la configuration fournie ne permet pas
			// d'exécuter la méthode 'check()' correctement.
			// @throws "InvalidObject" : l'exception levée lorsqu'un paramètre
			// est non conforme à la description qui en a été faite
			'check': function() {
				var data = arguments[0];
				var waited = arguments[1];

				// On vérifie que les paramètres soient bien des objets
				// sans quoi on retourne une erreur !
				if(typeof data != "object" || typeof waited != "object") {
					throw {
						'name': "BadConfig",
						'message': "check() function needs two objects as parameters."
					};
				}

				// On récupère les propriétés de l'objet pour
				// effectuer la comparaison
				var propsO = $.map(data, function(item, indice){return indice;});
				// Pour chaque propriété attendue ...
				$(waited).each(function(indice,prop) {
					// Si elle n'est pas dans l'objet on retourne une erreur !
					if($.inArray(prop.name,propsO) == -1) {
						throw {
							'name': "InvalidObject",
							'message': "The property '" + prop.name + "' is waited in the object."
						};
					}
					
					// S'il s'agît d'un type primitif décrit par une 
					// chaîne de caractères, on le compare via "typeof"
					if(typeof prop.type == "string") {
						if(typeof data[prop.name] != prop.type) {
							throw {
								'name': "InvalidObject",
								'message': "The property '" + prop.name + "' must be a '" + prop.type + "' (typeof)."
							};
						}
					} 
					// S'il s'agît d'une classe décrite par une fonction
					// on le compare via "instanceof"
					else if(typeof prop.type == "function") {
						if(!(data[prop.name] instanceof prop.type)) {
							throw {
								'name': "InvalidObject",
								'message': "The property '" + prop.name + "' must be a '" + prop.type.name + "' (instanceof)."
							};
						}
					}
					// Sinon on ne sait pas comparer ...
					else {
						throw {
							'name': "InvalidObject",
							'message': "The type described by '" + prop.type + "' is unknown."
						};
					}
				});				
			}
		};

		return __constructor__;
	})();

	// OPTIONS par défaut du conteneur de code
	LascarCode._defaults = {
		'class': "code-tester"
	};

} 
// Dans le cas d'une exception lancée pendant l'initialisation
// ou l'utilisation de LASCAR, on la retourne à l'utilisateur 
// sous la forme la plus simple possible !
catch(e) {
	var contenuErreur = e;
	// En fonction du type de l'erreur, le message est soit 
	// retourné directement, soit il est contenu dans un message.
	if(typeof e == "object") {
		contenuErreur = "Type : " + e.name + "\n" +
			"Message : " + e.message;
	}
	
	alert(
		"ERREUR CRITIQUE !\n" +
		"Une erreur est survenue pendant le chargement de 'Lascar.code'.\n" +
		contenuErreur
	);
}
/* 
Démonstrateurs pour LASCAR
Version     : 0.2.0
Auteur      : Loïc BRULEBOIS
Description :
Script permettant d'initialiser les différents
démonstrateurs pour LASCAR.
*/
try {
    // -----
    // DEMONSTRATEUR n°1
    // Carte par défaut.
	var map1 = new Lascar.cl.Map(),
    // -----
    // -----
    // DEMONSTRATEUR n°2
    // Carte avec un conteneur personnalisé.
        map2 = new Lascar.cl.Map("#map-id-perso"),
    // -----
    // -----
    // DEMONSTRATEUR n°3
    // Gestionnaire des paramètres dans l'URL
        um = new Lascar.cl.component.Url(),
	// Gestionnaire des marqueurs depuis les
	// paramètres de l'URL
		mm = new Lascar.cl.component.MarkerManager(),
    // Carte avec un conteneur et une configuration
    // personnalisés.
        map3 = new Lascar.cl.Map("#map-all-perso", {
		    default_layer: "thunderforest_out",
			layers: {
				osm_basic: undefined,
				osm_fr: undefined,
			    wikimedia: new Lascar.cl.layer.LayerDesc({
					code: "wikimedia",
					title: "Wikimédia",
					url: "https://maps.wikimedia.org/osm-intl/{z}/{x}/{y}.png",
					attribution: '© Contributeurs d\'<a href="http://openstreetmap.org">OpenStreetMap</a> - Hébergeur : <a href="https://www.mediawiki.org/wiki/Maps">Wikimedia Maps</a>',
					zoom: {
                        min   : 0,
                        max   : 18
					}
                }),
				thunderforest_out: new Lascar.cl.layer.LayerDesc({
					code: "thunderforest_out",
					title: "Thunderforest Outdoors",
					url: "http://{s}.tile.thunderforest.com/outdoors/{z}/{x}/{y}.png",
					attribution: '© Contributeurs d\'<a href="http://openstreetmap.org">OpenStreetMap</a> - Hébergeur : <a href="http://www.thunderforest.com/outdoors/">Thunderforest</a>',
					zoom: {
                        min   : 0,
                        max   : 18
					}
                })
			},
			controls: {
				scale: {
					generate: false
				}
			}
		});
    // On attache le composant de gestion des paramètres
    // dans l'URL à notre 3ème démonstrateur ainsi qu'au 
	// gestionnaire des marqueurs.
    um.addTo(map3);
	mm.link(um);
	mm.addTo(map3);
	// On en profite pour corriger le chemin des images
	// par défaut de Leaflet car je n'ai pas respecté 
	// la structure des répertoires de Leaflet, rendant
	// inaccessibles les chemins par défaut.
	Lascar.util.correctLp(/leaflet\.css/);
    // On souhaite ausi que les potentiels paramètres fournis
    // lors du chargement de la page soient pris en compte.
	$(window).on("load", function(event) {
		um.apply();
	});
	// -----
    // -----
    // DEMONSTRATEUR n°4
	// On génère la carte ...
	var map4 = new Lascar.cl.Map("#map-lf-geojson");
	// NDLA : Il est possible de réaliser le même travail
	// que ci-dessous en utilisant directement l'objet $.ajax
	// par exemple.
	// *****
	// ... puis le service en charge du requêtage
	// de la donnée GeoJSON ...
	map4.geojsonSrv = new Lascar.cl.component.Requester({
		type: "GET", 
		url: "data/departements.geojson.json"
	});
	// ... on ajoute une réaction à la carte lorsqu'elle
	// reçoit la réponse du service GeoJSON.
	map4.update = function(e,o) {
		// On ne traite que les notification du 
		// service  GeoJSON ...
		if(e == this.geojsonSrv) {
			// S'il y a eu une erreur, on en informe
			// l'utilisateur.
			if(typeof o.error != "undefined") {
				alert("{0} : une erreur est survenue pendant le chargement des données GeoJSON.".format(map4.id));
			}
			// Sinon on charge les données à l'aide d'un 
			// layer GeoJSON fourni par Leaflet !
			else {
				(this._leaflet.ctrlGeojson = new L.geoJson(o.data, {
					// Pour chaque 'Feature' à attacher, on
					// lui associe une fenêtre de dialogue
					// présentant les données associées.
					'onEachFeature': function(f, l) {
						if (f.properties) {
							var contenu = "<ul style=\"list-style-type: none; padding: 0px;\">";
							$.each(f.properties, function(k, v) {
								contenu += "<li><strong>{0}</strong> = {1}</li>".format(k,v);
							});
							contenu += "</ul>";
							l.bindPopup(contenu);
						}
					}
				})).addTo(this._leaflet.map);
			}
		}
	};
	// On envoie enfin la requête !
	map4.geojsonSrv.send(map4);
	// -----
	// -----
	// DEMONSTRATEUR n°5
	// On génère la carte ...
	var map5 = new Lascar.cl.Map("#map-l-geojson");
	// ... on référence le layer Lascar GeoJSON utilisant 
	// les mêmes données que précédemment.
	map5._leaflet.geojsonLayer = new Lascar.cl.layer.GeoJSON({
		type: "GET", 
		url: "data/departements.geojson.json"
	}, {
		name: "Départements (FR)",
		style: {
			forme: {
				fillOpacity: 0.4
			}
		}
	});
	map5._leaflet.geojsonLayer.addTo(map5);
	map5._leaflet.geojsonLayer.apply();
	// -----
	// DEMONSTRATEUR n°5
	// On génère la carte ...
	var map6 = new Lascar.cl.Map("#map-l-overpass", {
		center: [45.76227259876609,4.857062101364136], 
		// Autre exemple pour le RER E : [48.91708480384719, 2.484283447265625],
		zoom: 19
	});
	// On référence le layer Lascar OverPass ...
	map6._leaflet.overpassLayer = new Lascar.cl.layer.OverPass({
		type: "POST", 
		url: "https://overpass-api.de/api/interpreter",
		contentType: "application/x-www-form-urlencoded; charset=UTF-8",
		overpassQuery: '[out:json]; way[name="Tour Oxygène"]({{bbox}}); (._;>;); out;' 
		// Autre exemple pour le RER E : '[out:json]; rel[network="RER"][ref="E"]({{bbox}}); (._;>;); out;'
	}, {
		name: "TO²",
		// Autre exemples pour le RER E : "RER E",
		style: {
			point: {
				radius: 7,
				color: "blue",
				fillColor: "yellow"
			},
			ligne: {
				color: "#6E1E78"
			},
			forme: {
				color: "orange",
				fillOpacity: 0.4
			}
		}
	});
	map6._leaflet.overpassLayer.addTo(map6);
	// On récupère la zone d'affichage de la carte
	var bounds = map6._leaflet.map.getBounds();
	bounds = [
		parseFloat(bounds.getSouthWest().lat.toFixed(6)),
		parseFloat(bounds.getSouthWest().lng.toFixed(6)),
		parseFloat(bounds.getNorthEast().lat.toFixed(6)),
		parseFloat(bounds.getNorthEast().lng.toFixed(6))
	];
	map6._leaflet.overpassLayer.apply(bounds);
	// -----
} catch(e) {
    console.log(e);
}